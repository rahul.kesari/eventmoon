package com.music_event_app.domain.usecase

import com.music_event_app.MainApplication
import com.music_event_app.data.repository.RemoteRepository
import com.music_event_app.data.source.remote.ApiErrorHandle
import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.util.exceptions.ServerException
import com.music_event_app.util.helper.NetworkHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

/**
 * An interactor that calls the actual implementation of [LoginViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class HomeFragmentUseCase(
    private val remoteRepository: RemoteRepository,
    private val apiErrorHandle: ApiErrorHandle?
) {

    @ExperimentalCoroutinesApi
    fun loadCategories(
        scope: CoroutineScope,
        params: CommonRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if (NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.loadCategories(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if (it.Status!!) {
                            // 404 is for no data found from server
                            onResult.onSuccess(it)
                        } else {
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        } else {
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }


    @ExperimentalCoroutinesApi
    fun loadEventLists(
        scope: CoroutineScope,
        params: CommonRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if (NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.loadEventList(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if (it.Status!!) {

                            onResult.onSuccess(it)
                        } else {
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        } else {
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }

    @ExperimentalCoroutinesApi
    fun loadStories(
        scope: CoroutineScope,
        params: CommonRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if (NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.loadStories(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if (it.Status!!) {
                            onResult.onSuccess(it)
                        } else {
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        } else {
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }

    @ExperimentalCoroutinesApi
    fun hitReminder(
        scope: CoroutineScope,
        params: CommonRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if (NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.hitReminder(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if (it.Status!! || it.StatusCode == 404) {
                            // 404 is for no data found from server
                            onResult.onSuccess(it)
                        } else {
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        } else {
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }
}