package com.music_event_app.domain.usecase.base

import com.music_event_app.data.source.remote.ErrorModel

interface UseCaseResponse<Type> {

    fun onSuccess(result: Type)

    fun onError(errorModel: ErrorModel?)
}
