package com.music_event_app.domain.usecase

import com.music_event_app.data.repository.RemoteRepository
import com.music_event_app.data.source.remote.ApiErrorHandle
import com.music_event_app.domain.model.TermsConditionResponse
import com.music_event_app.domain.usecase.base.SingleUseCase

/**
 * An interactor that calls the actual implementation of [LoginViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class TermsConditionUseCase(private val remoteRepository: RemoteRepository, apiErrorHandle: ApiErrorHandle) : SingleUseCase<TermsConditionResponse, Any?>(apiErrorHandle) {

    override suspend fun run(params: Any?): TermsConditionResponse {
        return remoteRepository.getTermsAndConditionDetail()
    }
}