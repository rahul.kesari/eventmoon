package com.music_event_app.domain.usecase

import com.music_event_app.MainApplication
import com.music_event_app.data.repository.RemoteRepository
import com.music_event_app.data.source.remote.ApiErrorHandle
import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.util.exceptions.ServerException
import com.music_event_app.util.helper.NetworkHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

/**
 * An interactor that calls the actual implementation of [LoginViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class LoginSignupUseCase(private val remoteRepository: RemoteRepository,private val apiErrorHandle: ApiErrorHandle?)  {

    @ExperimentalCoroutinesApi
    fun loginWithGoogle(
        scope: CoroutineScope,
        params: LoginRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if(NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.loginWithGoogle(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if(it.Status!!) {
                            onResult.onSuccess(it)
                        }else{
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        }else{
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }


    @ExperimentalCoroutinesApi
    fun loginWithFacebook(
        scope: CoroutineScope,
        params: LoginRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if(NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.loginWithFacebook(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if(it.Status!!) {
                            onResult.onSuccess(it)
                        }else{
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        }else{
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }


    @ExperimentalCoroutinesApi
    fun registerWithEmail(
        scope: CoroutineScope,
        params: LoginRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if(NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.registerWithEmail(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if(it.Status!!) {
                            onResult.onSuccess(it)
                        }else{
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        }else{
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }

    @ExperimentalCoroutinesApi
    fun loginWithEmail(
        scope: CoroutineScope,
        params: LoginRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if(NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.loginWithEmail(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if(it.Status!!) {
                            onResult.onSuccess(it)
                        }else{
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        }else{
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }

    @ExperimentalCoroutinesApi
    fun forgetPassword(
        scope: CoroutineScope,
        params: LoginRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if(NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.forgetPassword(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if(it.Status!!) {
                            onResult.onSuccess(it)
                        }else{
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        }else{
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }


    @ExperimentalCoroutinesApi
    fun resetPassword(
        scope: CoroutineScope,
        params: LoginRequest?,
        onResult: (UseCaseResponse<BaseResponse>)
    ) {
        if(NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async {
                remoteRepository.resetPassword(params!!)
            }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        //if status code from server response is not true
                        if(it.Status!!) {
                            onResult.onSuccess(it)
                        }else{
                            //through exception with the server response
                            onResult.onError(apiErrorHandle?.handleServerResponseCode(it.StatusCode,it.ErrorMessage))
                        }
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        }else{
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }
}