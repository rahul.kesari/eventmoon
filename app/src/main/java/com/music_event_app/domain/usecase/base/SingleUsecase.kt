package com.music_event_app.domain.usecase.base

import com.music_event_app.MainApplication
import com.music_event_app.data.source.remote.ApiErrorHandle
import com.music_event_app.di.module.NetworkModule
import com.music_event_app.util.helper.NetworkHelper
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.io.IOException

abstract class SingleUseCase<Type, in Params>(private val apiErrorHandle: ApiErrorHandle?) where Type : Any {

    val TAG = SingleUseCase::class.java.name
    abstract suspend fun run(params: Params? = null): Type

    @ExperimentalCoroutinesApi
    fun invoke(
        scope: CoroutineScope,
        params: Params?,
        onResult: (UseCaseResponse<Type>)
    ) {
        if(NetworkHelper.isConnected(MainApplication.context)) {
            val backgroundJob = scope.async { run(params) }
            scope.launch {
                backgroundJob.await().let {
                    try {
                        onResult.onSuccess(it)
                    } catch (e: HttpException) {
                        onResult.onError(apiErrorHandle?.traceErrorException(e))
                    }
                }
            }
        }else{
            onResult.onError(apiErrorHandle?.traceErrorException(IOException()))
        }
    }
}