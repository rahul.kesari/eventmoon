package com.music_event_app.domain.model.locationsearch

data class LocationDetailResponse(
    var html_attributions: List<Any>?,
    var result: Result?,
    var status: String?
)

data class Result(
    var address_components: List<AddressComponent>?,
    var adr_address: String?,
    var business_status: String?,
    var formatted_address: String?,
    var formatted_phone_number: String?,
    var geometry: Geometry?,
    var icon: String?,
    var international_phone_number: String?,
    var name: String?,
    var opening_hours: OpeningHours?,
    var place_id: String?,
    var plus_code: PlusCode?,
    var rating: Double?,
    var reference: String?,
    var reviews: List<Review>?,
    var types: List<String>?,
    var url: String?,
    var user_ratings_total: Int?,
    var utc_offset: Int?,
    var vicinity: String?,
    var website: String?
)

data class AddressComponent(
    var long_name: String?,
    var short_name: String?,
    var types: List<String>?
)

data class Geometry(
    var location: Location?,
    var viewport: Viewport?
)

data class OpeningHours(
    var open_now: Boolean?,
    var periods: List<Period>?,
    var weekday_text: List<String>?
)

data class PlusCode(
    var compound_code: String?,
    var global_code: String?
)

data class Review(
    var author_name: String?,
    var author_url: String?,
    var language: String?,
    var profile_photo_url: String?,
    var rating: Int?,
    var relative_time_description: String?,
    var text: String?,
    var time: Int?
)

data class Location(
    var lat: Double?,
    var lng: Double?
)

data class Viewport(
    var northeast: Northeast?,
    var southwest: Southwest?
)

data class Northeast(
    var lat: Double?,
    var lng: Double?
)

data class Southwest(
    var lat: Double?,
    var lng: Double?
)

data class Period(
    var close: Close?,
    var open: Open?
)

data class Close(
    var day: Int?,
    var time: String?
)

data class Open(
    var day: Int?,
    var time: String?
)