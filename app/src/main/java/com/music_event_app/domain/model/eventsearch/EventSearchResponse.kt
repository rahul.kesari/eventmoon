package com.music_event_app.domain.model.eventsearch

import com.music_event_app.domain.model.events.EventItem

data class EventSearchResponse(
    var Day: List<Day?>
)

data class Day(
    var data: List<EventItem?>,
    var day: String?
)
