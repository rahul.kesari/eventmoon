package com.music_event_app.domain.model


import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*


data class BaseResponse(
    val ErrorMessage: String?,
    val Group: Group?,
    @SerializedName("Results")
    val Results: Any?=null,
    val Status: Boolean?,
    val StatusCode: Int?,
    val totalPageCount: Int?
)


class Group(
)


data class Results(
    val Code: String?=null,
    val EarnedPoints: Int?,
    val GuestPoint: Int?,
    val InvitePoint: Int?,
    val IsAlreadyRewarded: Boolean?,
    val TotalRewards: String?,
    val accessToken: String?,
    val applyEnterprice: Boolean?,
    val businessLocation: String?,
    val businesslongitude: String?,
    val bussinesslatitude: String?,
    val countryCode: String?,
    val deviceToken: String?,
    val deviceType: String?,
    val emailId: String?,
    val fbId: String?,
    val gId: String?,
    val groupPeople: String?,
    val isAlreadyRegisteredWithFB: Boolean?,
    val isAlreadyRegisteredWithGoogle: Boolean?,
    val isBusinessUser: Boolean?,
    val isCouple: Boolean?,
    val isEnterprice: Boolean?,
    val isFlySolo: Boolean?,
    val isGroup: Boolean?,
    val isVerified: String?,
    val name: String?,
    val password: String?,
    val phone: String?,
    val profileType: String?,
    val userId: Int?,
    val userImagePath: String?,
    val userName: String?
)