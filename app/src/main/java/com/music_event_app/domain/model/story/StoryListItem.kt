package com.music_event_app.domain.model.story

data class StoryListItem(
    var duration: String?,
    var eventId: Int?,
    var image: String?,
    var isEvent: Boolean?,
    var isImage: Boolean?,
    var latitude: String?,
    var location: String?,
    var longitude: String?,
    var postedDate: String?,
    var thumbnailImage: String?,
    var user: User?,
    var video: Any?
)

data class User(
    var profilePic: String?,
    var userId: Int?,
    var userName: String?
)