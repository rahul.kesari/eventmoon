package com.music_event_app.domain.model.events


data class  EventItem(
    var EventLink: String?,
    var FullAddress: String?,
    var IsTrending: String?,
    var IsVirtual: String?,
    var Rewards: Rewards?,
    var Slots: Any?,
    var StartAndEndTime: String?,
    var Thumbnail: String?,
    var activityCount: Int?,
    var addToCalender: Boolean?,
    var alredyRating: Boolean?,
    var categoryId: Int?,
    var categoryName: String?,
    var day: String?,
    var dealtext: String?,
    var discount: String?,
    var endDate: String?,
    var endTime: String?,
    var eventDescription: String?,
    var eventId: Int?,
    var eventName: String?,
    var eventStory: EventStory?,
    var eventUser: List<Any>?,
    var imagepath: String?,
    var interestedCount: String?,
    var isCouple: String?,
    var isFlySolo: String?,
    var isGroup: String?,
    var isInterested: String?,
    var isOwner: Boolean?,
    var isTmaster: Boolean?,
    var isadvertisement: Boolean?,
    var latitude: String?,
    var location: String?,
    var longitude: String?,
    var nearMe: String?,
    var nearMeEvents: ArrayList<EventItem?>,
    var newstartTime: String?,
    var percentage: String?,
    var phoneNo: String?,
    var price: String?,
    var ratingCount: String?,
    var ratings: String?,
    var startDate: String?,
    var startTime: String?,
    var ticketCount: String?,
    var ticketSold: String?,
    var ticketUrl: String?,
    var ticketsLeft: String?,
    var timeId: String?,
    var NearMeDistabnce:String?
)

data class Rewards(
    var Description: String?,
    var Image: String?,
    var RewardId: String?,
    var Title: String?
)

data class EventStory(
    var duration: String?,
    var eventId: Int?,
    var image: String?,
    var isEvent: Boolean?,
    var isImage: Boolean?,
    var latitude: String?,
    var location: String?,
    var longitude: String?,
    var postedDate: String?,
    var thumbnailImage: String?,
    var user: User?,
    var video: String?
)

data class User(
    var profilePic: String?,
    var userId: Int?,
    var userName: String?
)