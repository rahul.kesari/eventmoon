package com.music_event_app.domain.model

data class TermsConditionResponse(
    val ErrorMessage: String,
    val Group: Group,
    val Results: String,
    val Status: Boolean,
    val StatusCode: Int,
    val totalPageCount: Int
)
