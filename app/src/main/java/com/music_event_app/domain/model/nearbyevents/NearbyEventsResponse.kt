package com.music_event_app.domain.model.nearbyevents

data class NearbyEventsResponse(
    var events: List<Event>?,
    var time_categories: List<TimeCategory>?
)

data class Event(
    var address: String?,
    var eventId: String?,
    var eventName: String?,
    var imagepath: String?,
    var interested: String?,
    var latitude: Double?,
    var longitude: Double?
)

data class TimeCategory(
    var category: String?,
    var categoryId: Int?,
    var time: String?
)