package com.music_event_app.domain.model.category

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@SuppressLint("ParcelCreator")
@Parcelize
data class CategoryItem(
    var CategoryName: String?,
    var Categoryid: Int?=0,
    var Discription: String?=""
):Parcelable