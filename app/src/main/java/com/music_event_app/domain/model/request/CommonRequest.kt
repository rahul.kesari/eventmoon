package com.music_event_app.domain.model.request


data class CommonRequest(
    var accessToken: String? = "",
    var userId: Int? = 0,
    var latitude: String? = "",
    var longitude: String? = "",
    var pageNumber: Any?,
    var dateTimeNow: String? = "",
    var isFromWeb: String? = "0",
    var categoryId: Int? = 0,
    var totalPageCount: Int? = 0,
    var SearchText: String? = "0",
    var eventName: String? = "",
    var eventId: Int? = 0,
    //for rattings
    var ratings: Float? = 0.0f,
    var reviewText: String? = "",
    //for eventreminder
    var startDate: String? = "",
    var image: String? = "",
    var deviceToken: String? = "",
    var deviceType: String? = "",
    var appName: String? = "",
    // for like,dislike
    var ratingsID: Int? = 0,
    var isLike: Boolean? = false,
    //for location filter
    var time: String? = "",
    var radius: Int? = 0
)

