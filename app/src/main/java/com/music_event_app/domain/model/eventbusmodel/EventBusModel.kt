package com.music_event_app.domain.model.eventbusmodel

data class  EventBusModel(
    var eventType: String?,
    var data: Any?)