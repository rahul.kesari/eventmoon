package com.music_event_app.domain.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class LoginRequest(
    var City: String?="",
    var Country: String?="",
    var State: String?="",
    var appname: String?="",
    var deviceToken: String?="",
    var deviceType: String?="",
    var emailId: String?="",
    var gid: String?="",
    var isTermsChecked: String?="",
    var name: String?="",
    var fbid: String?="",
    var filepath: String?="",
    var password: String?="",
    var refCode: String?="",
    var mobileContact: String?="",
    var VerificationCode: String?=""
):Parcelable