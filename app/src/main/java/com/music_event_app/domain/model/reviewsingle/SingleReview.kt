package com.music_event_app.domain.model.reviewsingle

data class SingleReview(
    var eventRatingDetailsModel: EventRatingDetailsModel?,
    var ratingByVotesModel: List<RatingByVotesModel>?,
    val reviewDescriptionWithModel: List<ReviewDescriptionWithModel?>?
)

data class EventRatingDetailsModel(
    var AverageRating: String?,
    var TotalVotes: String?,
    var eventID: Int?,
    var ratingPercentage: String?
)

data class RatingByVotesModel(
    var PercentageByVotes: String?="0",
    var Ratings: String?="0",
    var VotesByRating: String?="0"
)

data class ReviewDescriptionWithModel(
    var EventName: String?="",
    var Name: String?="",
    var PostedDate: String?="",
    var Ratings: String?="",
    var RatingsID: Int?=0,
    var ReviewText: String?="",
    var TotalDisLike: String?="",
    var TotalLike: String?="",
    var UserImagePath: String?=""
)