package com.music_event_app.data.source.remote

import android.util.Log
import com.music_event_app.util.exceptions.ServerException
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

/**
 * This class trace exceptions(api call or parse data or connection errors) &
 * depending on exception returns a [ErrorModel]
 *
 * */
class ApiErrorHandle {

    fun traceErrorException(throwable: Throwable?): ErrorModel {
        val errorModel: ErrorModel? = when (throwable) {

            // if throwable is an instance of HttpException
            // then attempt to parse error data from response body
            is HttpException -> {
                // handle UNAUTHORIZED situation (when token expired)
                if (throwable.code() == 401) {
                    ErrorModel(
                        throwable.message(),
                        throwable.code(),
                        ErrorModel.ErrorStatus.UNAUTHORIZED,
                        false
                    )
                } else {
                    getHttpError(throwable.response()?.errorBody())
                }
            }

            // handle api call timeout error
            is SocketTimeoutException -> {
                ErrorModel(
                    "Slow Internet Connection!",
                    ErrorModel.ErrorStatus.TIMEOUT
                )
            }

            // handle connection error
            is IOException -> {
                ErrorModel(
                    "No Internet Connection Found!",
                    ErrorModel.ErrorStatus.NO_CONNECTION
                )
            }

            // handle connection error
            is ServerException -> {
                ErrorModel(
                    throwable.message,
                    0,
                    ErrorModel.ErrorStatus.BAD_RESPONSE,
                    retryNeeded = false
                )
            }
            else -> null
        }
        return errorModel ?: ErrorModel(
            "No Defined Error!",
            0,
            ErrorModel.ErrorStatus.BAD_RESPONSE,
            false
        )
    }

    fun handleServerResponseCode(statusCode: Int?, errorMessage: String?): ErrorModel {

        val errorModel: ErrorModel? = when (statusCode) {
            400 ->
                ErrorModel(
                    errorMessage,
                    statusCode,
                    ErrorModel.ErrorStatus.BAD_RESPONSE,
                    retryNeeded = false
                )
            404 -> ErrorModel(
                errorMessage,
                statusCode,
                ErrorModel.ErrorStatus.EMPTY_RESPONSE,
                retryNeeded = false
            )

            else -> ErrorModel(
                errorMessage,
                statusCode,
                ErrorModel.ErrorStatus.NOT_DEFINED,
                retryNeeded = false
            )
        }
        return errorModel!!
    }

    /**
     * attempts to parse http response body and get error data from it
     *
     * @param body retrofit response body
     * @return returns an instance of [ErrorModel] with parsed data or NOT_DEFINED status
     */
    private fun getHttpError(body: ResponseBody?): ErrorModel {
        return try {
            // use response body to get error detail
            val result = body?.string()
            Log.d("getHttpError", "getErrorMessage() called with: errorBody = [$result]")
            ErrorModel(
                body.toString(),
                400,
                ErrorModel.ErrorStatus.BAD_RESPONSE,
                true
            )
        } catch (e: Throwable) {
            e.printStackTrace()
            ErrorModel(
                message = e.message,
                errorStatus = ErrorModel.ErrorStatus.NOT_DEFINED
            )
        }

    }
}