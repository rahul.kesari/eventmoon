package com.music_event_app.data.source.remote

import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.TermsConditionResponse
import com.music_event_app.domain.model.locationsearch.LocationDetailResponse
import com.music_event_app.domain.model.locationsearch.LocationSearchResponse
import com.music_event_app.domain.model.request.CommonRequest
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Url

interface APIService {


    @POST("LoginGoogle")
    suspend fun loginWithGoogle(@Body loginRequest: LoginRequest?): BaseResponse

    @POST("LoginFacebook")
    suspend fun loginWithFacebook(@Body loginRequest: LoginRequest): BaseResponse

    @POST("RegisterWithEmail")
    suspend fun registerWithEmail(@Body loginRequest: LoginRequest): BaseResponse

    @POST("LoginByEmail")
    suspend fun loginWithEmail(@Body loginRequest: LoginRequest): BaseResponse

    @POST("TermAndConditions")
    suspend fun termsAndCondition(): TermsConditionResponse

    @POST("ForgetPassword")
    suspend fun forgetPassword(@Body loginRequest: LoginRequest): BaseResponse

    @POST("ResetPassword")
    suspend fun resetPassword(@Body loginRequest: LoginRequest): BaseResponse

    @POST("Category")
    suspend fun loadCategories(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("Event")
    suspend fun loadEventList(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("StoryList")
    suspend fun loadStories(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("SearchEvent")
    suspend fun loadEventSearch(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("SearchAutoSuggestion")
    suspend fun loadEventSearchSuggestion(@Body commonRequest: CommonRequest?): BaseResponse

    @GET
    suspend fun searchLocationPlaces(@Url url:String ): LocationSearchResponse

    @GET
    suspend fun locationDetail(@Url url:String ): LocationDetailResponse

    @POST("EventDetail")
    suspend fun loadEventDetail(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("Ratings")
    suspend fun submitRatings(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("RatingReview")
    suspend fun getEventReview(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("EventReminderPushNotification")
    suspend fun hitReminder(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("LikeDislikeReview")
    suspend fun submitLikeDislike(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("UpdateDeviceToken")
    suspend fun updateDeviceToken(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("UpdateProfile")
    suspend fun submitProfileDetail(@Body request: MultipartBody?): BaseResponse

    @POST("CreateEvent")
    suspend fun createNewEvent(@Body request: MultipartBody?): BaseResponse

    @POST("MyEvent")
    suspend fun loadMyEventList(@Body commonRequest: CommonRequest?): BaseResponse

    @POST("nearbyEvents")
    suspend fun loadNearbyEvents(@Body commonRequest: CommonRequest?): BaseResponse

}