package com.music_event_app.data.repository

import com.music_event_app.data.source.remote.APIService
import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.TermsConditionResponse
import com.music_event_app.domain.model.locationsearch.LocationDetailResponse
import com.music_event_app.domain.model.locationsearch.LocationSearchResponse
import com.music_event_app.domain.model.request.CommonRequest
import okhttp3.MultipartBody

class RemoteRepository(val apiService: APIService) {



    suspend fun loginWithGoogle(loginRequest: LoginRequest?): BaseResponse {
        return apiService.loginWithGoogle(loginRequest)
    }

    suspend fun loginWithFacebook(loginRequest: LoginRequest): BaseResponse {
        return apiService.loginWithFacebook(loginRequest)
    }

    suspend fun registerWithEmail(loginRequest: LoginRequest): BaseResponse {
        return apiService.registerWithEmail(loginRequest)
    }

    suspend fun loginWithEmail(loginRequest: LoginRequest): BaseResponse {
        return apiService.loginWithEmail(loginRequest)
    }

    suspend fun forgetPassword(loginRequest: LoginRequest): BaseResponse {
        return apiService.forgetPassword(loginRequest)
    }

    suspend fun resetPassword(loginRequest: LoginRequest): BaseResponse {
        return apiService.resetPassword(loginRequest)
    }

    suspend fun getTermsAndConditionDetail(): TermsConditionResponse {
        return apiService.termsAndCondition()
    }


    suspend fun loadCategories(commonRequest: CommonRequest): BaseResponse {
        return apiService.loadCategories(commonRequest)
    }

    suspend fun loadEventList(commonRequest: CommonRequest): BaseResponse {
        return apiService.loadEventList(commonRequest)
    }

    suspend fun loadStories(commonRequest: CommonRequest): BaseResponse {
        return apiService.loadStories(commonRequest)
    }

    suspend fun loadEventSearch(commonRequest: CommonRequest): BaseResponse {
        return apiService.loadEventSearch(commonRequest)
    }

    suspend fun loadEventSearchSuggestion(commonRequest: CommonRequest): BaseResponse {
        return apiService.loadEventSearchSuggestion(commonRequest)
    }

    suspend fun searchLocation(url: String): LocationSearchResponse {
        return apiService.searchLocationPlaces(url)
    }

    suspend fun locationDetail(url: String): LocationDetailResponse {
        return apiService.locationDetail(url)
    }

    suspend fun loadEventDetail(commonRequest: CommonRequest): BaseResponse {
        return apiService.loadEventDetail(commonRequest)
    }


    suspend fun submitRatings(commonRequest: CommonRequest): BaseResponse {
        return apiService.submitRatings(commonRequest)
    }

    suspend fun getEventReview(commonRequest: CommonRequest): BaseResponse {
        return apiService.getEventReview(commonRequest)
    }

    suspend fun hitReminder(commonRequest: CommonRequest): BaseResponse {
        return apiService.hitReminder(commonRequest)
    }

    suspend fun submitLikeDislike(commonRequest: CommonRequest): BaseResponse {
        return apiService.submitLikeDislike(commonRequest)
    }

    suspend fun updateDeviceToken(commonRequest: CommonRequest): BaseResponse {
        return apiService.updateDeviceToken(commonRequest)
    }

    suspend fun submitProfileDetail(multipartBody: MultipartBody): BaseResponse {
        return apiService.submitProfileDetail(multipartBody)
    }

    suspend fun createNewEvent(multipartBody: MultipartBody): BaseResponse {
        return apiService.createNewEvent(multipartBody)
    }

    suspend fun loadMyEventList(commonRequest: CommonRequest): BaseResponse {
        return apiService.loadMyEventList(commonRequest)
    }

    suspend fun loadNearbyEvent(commonRequest: CommonRequest): BaseResponse {
        return apiService.loadNearbyEvents(commonRequest)
    }
}