package com.music_event_app.util.firebasenotification;

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.music_event_app.ui.eventdetail.EventDetailActivity
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.firebasenotification.NotificationUtils.Companion.isAppIsInBackground
import com.music_event_app.util.helper.Prefs


/**
 * Created by rahul .
 */

class MyFireBaseMessagingService : FirebaseMessagingService() {
    private var notificationUtils: NotificationUtils? = null

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        Prefs.putBoolean(AppConstants.PreferenceConstants.IS_TOKEN_REFRESHED, true)
        Prefs.putString(AppConstants.PreferenceConstants.FCM_TOKEN, token)

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
//        Log.e(TAG, "From: " + remoteMessage!!.from)

        if (remoteMessage == null) return

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.notification!!.body)
            //            handleNotification(remoteMessage.getNotification().getBody());
            //            handleDataMessage(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.e(TAG, "Data Payload: " + remoteMessage.data)
            try {
                // JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(
                    remoteMessage.data["title"],
                    remoteMessage.data["body"],
                    remoteMessage.data["image"],
                    remoteMessage.data["click_action"],
                    remoteMessage.data["eventId"]
                )
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }
        } else {
            Log.e(TAG, "onMessageReceived: else")
        }
    }

    private fun handleNotification(message: String) {
        if (!isAppIsInBackground(applicationContext)) {
            // app is in foreground, broadcast the push message
            val pushNotification =
                Intent(Config.PUSH_NOTIFICATION)
            pushNotification.putExtra("message", message)
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

            // play notification sound
            val notificationUtils = NotificationUtils(applicationContext)
            notificationUtils.playNotificationSound()
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private fun handleDataMessage(
        title: String?,
        message: String?,
        imageUrl: String?,
        clickAction: String?,
        eventId: String?
    ) {

        val resultIntent: Intent

        when (clickAction) {
            "ACTION_OPEN_PUSH" -> {
                resultIntent = Intent(applicationContext, EventDetailActivity::class.java)
                resultIntent.putExtra(EventDetailActivity.EVENT_ID, eventId?.toInt())
            }
            else -> {
                resultIntent = Intent(applicationContext, HomeActivity::class.java)
            }
        }


        // check for image attachment
        if (TextUtils.isEmpty(imageUrl)) {

            showNotificationMessage(
                applicationContext,
                title!!,
                message,
                System.currentTimeMillis().toString(),
                resultIntent
            )
        } else {
            // image is present, show notification with image
            showNotificationMessageWithBigImage(
                applicationContext,
                title!!,
                message,
                System.currentTimeMillis().toString(),
                resultIntent,
                imageUrl
            )
        }
    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(
        context: Context,
        title: String,
        message: String?,
        timeStamp: String,
        intent: Intent
    ) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent)
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(
        context: Context, title: String, message: String?,
        timeStamp: String,
        intent: Intent,
        imageUrl: String?
    ) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent, imageUrl)
    }

    companion object {
        private val TAG = MyFireBaseMessagingService::class.java.simpleName
    }
}
