package com.music_event_app.util.helper

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import androidx.annotation.Nullable
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar


class BottomFabBehavior(
    @Nullable context: Context?,
    @Nullable attrs: AttributeSet?
) :CoordinatorLayout.Behavior<ImageView?>(context, attrs) {


    fun layoutDependsOn(
        @Nullable parent: CoordinatorLayout?,
        child: ImageView,
        dependency: View
    ): Boolean {
        return dependency is Snackbar.SnackbarLayout
    }

   override fun onDependentViewRemoved(
        parent: CoordinatorLayout,
        child: ImageView,
        dependency: View
    ) {
        child.translationY = 0.0f
    }

   override fun onDependentViewChanged(
        parent: CoordinatorLayout,
        child: ImageView,
        dependency: View
    ): Boolean {
        return updateButton(child, dependency)
    }

    private fun updateButton(child: View, dependency: View): Boolean {
        return if (dependency is Snackbar.SnackbarLayout) {
            val oldTranslation: Float = child.getTranslationY()
            val height = dependency.getHeight() as Float
            val newTranslation: Float = dependency.getTranslationY() - height
            child.setTranslationY(newTranslation)
            oldTranslation != newTranslation
        } else {
            false
        }
    }


}