package com.music_event_app.util.helper

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.music_event_app.domain.model.Results
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.eventsearch.EventSearchResponse
import com.music_event_app.domain.model.nearbyevents.NearbyEventsResponse
import com.music_event_app.domain.model.reviewsingle.SingleReview
import com.music_event_app.domain.model.story.StoryListItem


object ObjectConverter {


    fun getLoginResponse(datas: Any?): Results? {
        val gson = GsonBuilder().create()
        return gson.fromJson(gson.toJson(datas), Results::class.java)
    }

    fun getCategoryResponse(datas: Any?): ArrayList<CategoryItem?> {
        val gson = GsonBuilder().create()
        val listType = object :
            TypeToken<ArrayList<CategoryItem?>?>() {}.type
        return gson.fromJson<java.util.ArrayList<CategoryItem?>>(
            gson.toJson(datas),
            listType
        )
    }

    fun getEventResponseList(datas: Any?): ArrayList<EventItem?> {
        val gson = GsonBuilder().create()
        val listType = object :
            TypeToken<ArrayList<EventItem?>?>() {}.type
        return gson.fromJson<java.util.ArrayList<EventItem?>>(
            gson.toJson(datas),
            listType
        )
    }

    fun getStoryListResponse(datas: Any?): ArrayList<StoryListItem?> {
        val gson = GsonBuilder().create()
        val listType = object :
            TypeToken<ArrayList<StoryListItem?>?>() {}.type
        return gson.fromJson<java.util.ArrayList<StoryListItem?>>(
            gson.toJson(datas),
            listType
        )
    }

    fun getEventSearchResponse(datas: Any?): EventSearchResponse? {
        val gson = GsonBuilder().create()
        return gson.fromJson(gson.toJson(datas), EventSearchResponse::class.java)
    }

    fun getSuggestionListResponse(datas: Any?): ArrayList<String?> {
        val gson = GsonBuilder().create()
        val listType = object :
            TypeToken<ArrayList<String?>?>() {}.type
        return gson.fromJson<java.util.ArrayList<String?>>(
            gson.toJson(datas),
            listType
        )
    }

    fun getEventDetailResponse(datas: Any?): EventItem? {
        val gson = GsonBuilder().create()
        return gson.fromJson(gson.toJson(datas), EventItem::class.java)
    }

    fun getEventSingleReviewResponse(datas: Any?): SingleReview? {
        val gson = GsonBuilder().create()
        return gson.fromJson(gson.toJson(datas), SingleReview::class.java)
    }

    fun getEventNearbyResponse(datas: Any?): NearbyEventsResponse? {
        val gson = GsonBuilder().create()
        return gson.fromJson(gson.toJson(datas), NearbyEventsResponse::class.java)
    }

}