package com.music_event_app.util.helper

import android.app.Activity
import android.graphics.Color
import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.androidadvance.topsnackbar.TSnackbar
import com.music_event_app.R
import de.mateware.snacky.Snacky

object SnackBarHelper {

    fun showSuccess(msg: String, context: Activity) {
        Snacky.builder()
            .setActivity(context)
            .setText(msg)
            .setDuration(Snacky.LENGTH_SHORT)
            .success()
            .show()
    }

    fun showInfo(msg: String, context: Activity) {
        Snacky.builder()
            .setActivity(context)
            .setText(msg)
            .centerText()
            .setDuration(Snacky.LENGTH_LONG)
            .info()
            .show()
    }

    fun showError(msg: String?, context: Activity) {
        Snacky.builder()
            .setText(msg)
            .setActivity(context)
            .setDuration(Snacky.LENGTH_SHORT)
//            .setActionText(R.string.retry_string)
            .error()
            .show()
    }

    fun showSuccessFromTop(msg: String, context: Activity,view:View) {
        val snackbar: TSnackbar = TSnackbar.make(
            view,
            msg!!,
            TSnackbar.LENGTH_LONG
        )
        snackbar.setActionTextColor(Color.WHITE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(Color.GREEN)
        val textView = snackbarView.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)
        snackbar.duration = TSnackbar.LENGTH_LONG
        snackbar.show()
    }

    fun showInfoFromTop(msg: String, context: Activity,view:View) {
        val snackbar: TSnackbar = TSnackbar.make(
            view,
            msg!!,
            TSnackbar.LENGTH_LONG
        )
        snackbar.setActionTextColor(Color.WHITE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.header_bg))
        val textView = snackbarView.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)
        textView.setTextSize(20.0f)
        snackbar.duration = TSnackbar.LENGTH_LONG
        snackbar.show()
    }

    fun showErrorFromTop(msg: String?, context: Activity,view:View){
        val snackbar: TSnackbar = TSnackbar.make(
            view,
            msg!!,
            TSnackbar.LENGTH_LONG
        )
        snackbar.setActionTextColor(Color.WHITE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(Color.RED)
        val textView = snackbarView.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)
        snackbar.duration = TSnackbar.LENGTH_LONG
        snackbar.show()
    }

    fun showRetry(msg: String?,context: Activity, actionObject:RetryActionClicked?){
        Snacky.builder()
            .setBackgroundColor(Color.parseColor("#ff0000"))
            .setTextSize(18.0f)
            .setTextColor(Color.parseColor("#FFFFFF"))
            .setTextTypefaceStyle(Typeface.ITALIC)
            .setText(msg)
            .setMaxLines(2)
            .setActionClickListener { actionObject?.onRetryClicked() }
            .setActionText(R.string.retry_string)
            .setActionTextColor(Color.parseColor("#D9000000"))
            .setActionTextSize(19.0f)
            .setActivity(context)
            .setDuration(Snacky.LENGTH_INDEFINITE)
            .build()
            .show()
    }

    interface RetryActionClicked{
        fun onRetryClicked()
    }
}