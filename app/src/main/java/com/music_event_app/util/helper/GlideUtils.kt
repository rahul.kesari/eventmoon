package com.music_event_app.util.helper;

import android.content.Context;
import android.widget.ImageView;
import androidx.core.net.toUri
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

object GlideUtils {
    fun loadImage(
        context: Context?,
        url: String?,
        viewId: ImageView?,
        placeHolder: Int
    ) {
        Glide.with(context!!).load(url?.trim()?.toUri()).apply(
            RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()
                .placeholder(placeHolder)
        ).into(viewId!!)
    }

    fun loadImageWithoutCache(
        context: Context?,
        url: String?,
        viewId: ImageView?,
        placeHolder: Int
    ) {
        Glide.with(context!!).load( url).apply(
            RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()
                .placeholder(placeHolder)
        ).into(viewId!!)
    }

    fun loadLocalImage(
        context: Context?,
        url: String?,
        viewId: ImageView?,
        placeHolder: Int
    ) {
        Glide.with(context!!).load(url).apply(
            RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()
                .placeholder(placeHolder)
        ).into(viewId!!)
    }

    fun loadImageCenterCrop(
        context: Context?,
        url: String?,
        viewId: ImageView?,
        placeHolder: Int
    ) {
        Glide.with(context!!).load( url).apply(
            RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().dontAnimate()
                .placeholder(placeHolder)
        ).into(viewId!!)
    }
}