package com.music_event_app.util.helper

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.Spanned
import androidx.appcompat.app.AlertDialog
import androidx.browser.customtabs.CustomTabsIntent
import com.music_event_app.R
import com.music_event_app.util.AppConstants
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList


object CommonHelper {

    fun fromHtml(html: String?): Spanned? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }


    fun getAddressToUse(): ArrayList<String?> {

        var list = ArrayList<String?>()


        //if changed addresss is blank then use user's current location.
        if (Prefs.getString(AppConstants.PreferenceConstants.SELECTED_LATITUDE, "")!!.isNotEmpty()) {

            list.add(
                Prefs.getString(
                    AppConstants.PreferenceConstants.SELECTED_LATITUDE,
                    "36.1251958"
                )
            )
            list.add(
                Prefs.getString(
                    AppConstants.PreferenceConstants.SELECTED_LONGITUDE,
                    "-115.3150848"
                )
            )
            list.add(
                Prefs.getString(
                    AppConstants.PreferenceConstants.SELECTED_ADDRESS,
                    "Las Vegas"
                )
            )

            if (Prefs.getString(AppConstants.PreferenceConstants.SELECTED_LATITUDE, "")!!
                    .isEmpty()
            ) {
                list.add(AppConstants.ADDRESS_TYPE.DL) //Default Las Vegas Location
            } else {
                list.add(AppConstants.ADDRESS_TYPE.SL) //Searched Location
            }

        } else if(Prefs.getString(AppConstants.PreferenceConstants.LATITUDE, "")!!.isNotEmpty()){
            //use user's current location address if not manually changed using location filter

            list.add(Prefs.getString(AppConstants.PreferenceConstants.LATITUDE, ""))
            list.add(Prefs.getString(AppConstants.PreferenceConstants.LONGITUDE, ""))
            list.add(Prefs.getString(AppConstants.PreferenceConstants.CITY, ""))
            list.add(AppConstants.ADDRESS_TYPE.GPSL) //current Location

        }else{
            list.add("36.1251958")
            list.add("-115.3150848")
            list.add("Las Vegas")
            list.add(AppConstants.ADDRESS_TYPE.DL)
        }
        return list
    }


    open fun navigateToMap(context: Context, latitude: String, longitude: String) {

        try {
            val uri: String =
                java.lang.String.format(
                    Locale.ENGLISH,
                    "geo:%f,%f",
                    latitude.toFloat(),
                    longitude.toFloat()
                )
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
            MessagesUtils.showToastError(context, "Geotag Format is not correct")
        }
    }

    open fun formatNumber(num: Float): String {
        val df = DecimalFormat("#.###")
        df.roundingMode = RoundingMode.CEILING
        return df.format(num)
    }

    open fun shareContent(
        context: Context,
        message: String?
    ) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, message)
        intent.putExtra(Intent.EXTRA_TEXT, message)
        context.startActivity(Intent.createChooser(intent, "Share"))
    }

    fun openCalendarDialog(
        context: Context?,
        dateListener: DateListener
    ) {
        val picker: DatePickerDialog
        val cldr = Calendar.getInstance(Locale.US)
        val day = cldr[Calendar.DAY_OF_MONTH]
        val month = cldr[Calendar.MONTH]
        val year = cldr[Calendar.YEAR]
        // date picker dialog
        picker = DatePickerDialog(
            context!!,
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                dateListener.onDateFetched(dayOfMonth.toString(), (monthOfYear+1).toString(),year.toString())
            }, year, month, day
        )
        // picker.getDatePicker().setMaxDate(System.currentTimeMillis());
        picker.show()
    }

    interface DateListener {
        fun onDateFetched(dayofMonth: String, monthOfyear: String, year: String)
    }

    open fun openUrlsInChromeCustomTabs(context: Context, url: String) {
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(context, Uri.parse(url))
    }

    open fun isUserLoggedIn(): Boolean {
        return Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN, "")!!.isNotEmpty()
    }

    fun showConfirmationDialog(
        context: Context?,
        message: String?,
        mListener: DialogCLick
    ) {
        val builder =
            AlertDialog.Builder(context!!, R.style.AlertDialogTheme)
        builder.setMessage(message)
        builder.setCancelable(true)
        builder.setPositiveButton(
            "Yes"
        ) { dialog, id ->
            dialog.cancel()
            mListener.onYesClicked()
        }
        builder.setNegativeButton(
            "No"
        ) { dialog, id ->
            dialog.cancel()
            mListener.onNoClicked()
        }
        val alert11 = builder.create()
        alert11.show()
    }

    interface DialogCLick {
        fun onYesClicked()
        fun onNoClicked()
    }

    fun showImagePickerDialog(
        context: Context?,
        imagePickerDialogListener: ImagePickerDialogListener
    ) {
        val options = arrayOf(
            "Take photo", "Gallery"
        )

        val builder =
            AlertDialog.Builder(context!!)
        builder.setTitle("Select Image")
        builder.setItems(options) { dialog, which ->
            if ("Take photo".equals(options[which], true)) {
                imagePickerDialogListener.onCameraClicked()
            } else if ("Gallery".equals(options[which], true)) {
                imagePickerDialogListener.onGalleryClicked()
            }
            // the user clicked on colors[which]
        }
        builder.show()
    }

    interface ImagePickerDialogListener {
        fun onCameraClicked()
        fun onGalleryClicked()
    }

    fun showTimePickerDialog(
        context: Context?,
        title: String,
        _listener: TimePickerDialogListener
    ) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mcurrentTime[Calendar.MINUTE]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            context,
            OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                _listener.onTimeSelected("$selectedHour:$selectedMinute")
            },
            hour,
            minute,
            true
        ) //Yes 24 hour time

        mTimePicker.setTitle(title)
        mTimePicker.show()
    }


    interface TimePickerDialogListener {
        fun onTimeSelected(time: String?)
    }
}