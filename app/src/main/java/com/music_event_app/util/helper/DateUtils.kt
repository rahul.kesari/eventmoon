package com.music_event_app.util.helper

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    open fun getTodayDateTime(): String? {
        val c = Calendar.getInstance().time
        val sdf = SimpleDateFormat("MM-dd-yyyy HH:mm",Locale.US)
        return sdf.format(c)
    }
}