package com.music_event_app.util.helper

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * Created by rahul
 */

class NetworkHelper {

    companion object Utils {
        private fun getNetworkInfo(context: Context): NetworkInfo? {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo
        }

        fun isConnected(context: Context): Boolean {
            val info = getNetworkInfo(context)
            return info != null && info.isConnected
        }
    }
}

