package com.music_event_app.util.helper

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.Toast
import es.dmoral.toasty.Toasty

object MessagesUtils {
    fun showToastInfo(context: Context?, message: String?) {
        Toasty.info(context!!, message!!, Toast.LENGTH_SHORT, true).show()
    }

    fun showToastError(context: Context?, message: String?) {
        Toasty.error(context!!, message!!, Toast.LENGTH_SHORT, true).show()
    }

    fun showToastSuccess(context: Context?, message: String?) {
        Toasty.success(context!!, message!!, Toast.LENGTH_SHORT, true).show()
    }

    fun showToastWarning(context: Context?, message: String?) {
        Toasty.warning(context!!, message!!, Toast.LENGTH_SHORT, true).show()
    }

    fun showToastNormal(context: Context?, message: String?) {
        Toasty.normal(context!!, message!!).show()
    }

    fun showToastNormal(
        context: Context?,
        message: String?,
        resource: Drawable?
    ) {
        Toasty.normal(context!!, message!!, resource).show()
    }
}