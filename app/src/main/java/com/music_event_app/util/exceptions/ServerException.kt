package com.music_event_app.util.exceptions

class ServerException(message: String?) : Exception(message)


class DataNotFoundException(message: String?) : Exception(message)