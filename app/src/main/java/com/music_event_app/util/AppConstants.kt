package com.music_event_app.util

import com.music_event_app.BuildConfig

class AppConstants {
    companion object {

        //        const val API_BASE_URL = BuildConfig.SERVER_URL
        const val API_BASE_URL = "https://api.eventmoon.com/api/"
        const val DEFAULT_LANGUAGE = "en"
        const val RESPONSE_REQUEST_400 = 400
        const val SPLASH_DELAY = 2000
        const val API_KEY = "AIzaSyDFLAIQ5qNi0KmELocazTLA6ErecgOCZvk"
        const val PLACE_API = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=$API_KEY&input="
        const val PLACE_DETAIL_API = "https://maps.googleapis.com/maps/api/place/details/json?key=$API_KEY&place_id="
    }

    enum class API_RESPONSE_MESSAGES {
        SERVER_ERROR,
        BAD_REQUEST
    }

    object PreferenceConstants {

        const val CITY = "CITY"
        const val STATE = "STATE"
        const val COUNTRY = "COUNTRY"
        const val LATITUDE = "LATITUDE"
        const val LONGITUDE = "LONGITUDE"
        const val ADDRESS = "ADDRESS"

        const val SELECTED_ADDRESS = "SELECTED_ADDRESS"
        const val SELECTED_LATITUDE = "SELECTED_LATITUDE"
        const val SELECTED_LONGITUDE = "SELECTED_LONGITUDE"

        //userProfiles preferences
        const val USER_ID = "userId"
        const val USER_NAME = "userName"
        const val NAME = "name"
        const val EMAIL_ID = "emailId"
        const val USER_IMAGE_PATH = "userImagePath"
        const val ACCESS_TOKEN = "accessToken"
        const val FB_ID = "fbId"
        const val GOOGLE_Id = "gId"
        const val FLY_SOLO = "flySolo"
        const val COUPLE = "couple"
        const val GROUP = "group"

        const val PROFILE_TYPE = "PROFILE_TYPE"

        //for app intro
        const val IS_INTRO_DONE = "is_intro_done"

        const val IS_TOKEN_REFRESHED = "IS_TOKEN_REFRESHED"
        const val FCM_TOKEN = "FCM_TOKEN"
    }

    object FragmentConstants {
        const val HOME_FRAGMENT = "HOME_FRAGMENT"
        const val LOCATION_FRAGMENT = "LOCATION_FRAGMENT"
        const val EVENT_SEARCH_FRAGMENT = "EVENT_SEARCH_FRAGMENT"
        const val REWARD_FRAGMENT = "REWARD_FRAGMENT"
    }


    object DRAWER_CONSTANTS {
        const val PROFILE = "PROFILE"
        const val REFERRAL_CODE = "REFERRAL_CODE"
        const val REWARD_POINT = "REWARD_POINT"
        const val HOME = "HOME"
        const val MY_EVENTS = "MY_EVENTS"
        const val EVENT_SEARCH = "EVENT_SEARCH"
        const val LOGIN = "LOGIN"
        const val LOGOUT = "LOGOUT"
    }

    object EVENTBUS_CONSTANTS {
        const val FOUND_LOCATION = "FOUND_LOCATION"
        const val LOCATION_DENIED = "LOCATION_DENIED"
    }

    object ADDRESS_TYPE {
        const val GPSL = "GPSL"
        const val DL = "DL"
        const val SL = "SL"
    }
}