package com.music_event_app.ui.termscondition;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.music_event_app.databinding.ActivityTermsBinding
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.util.helper.CommonHelper
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import com.music_event_app.R
import com.music_event_app.util.helper.SnackBarHelper

class TermsConditionActivity : BaseActivity<TermsConditionViewModel, ActivityTermsBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_terms
    override val viewModel: TermsConditionViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupInsets(binding.topLayout)
        initViews()
//        setAppBarHeight()
        observeViewModel()
        viewModel.getTermsAndCondition()
    }

    private fun initViews() {
        binding.toolbarLayout.toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    fun observeViewModel() {
        with(viewModel) {
            responseLiveData.observe(this@TermsConditionActivity, Observer {
                binding.termsTV.text = CommonHelper.fromHtml(it.Results)
            })

            isProcessing.observe(this@TermsConditionActivity, Observer {
                if (it) binding.progressLayout.visibility =
                    View.VISIBLE else binding.progressLayout.visibility = View.GONE
            })

            mSnackbarHandler.observe(this@TermsConditionActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@TermsConditionActivity,object:
                        SnackBarHelper.RetryActionClicked{
                        override fun onRetryClicked() {
                            viewModel.getTermsAndCondition()
                        }
                    })
                }else{
                    SnackBarHelper.showError(it.message,this@TermsConditionActivity)
                }
            })
        }
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, TermsConditionActivity::class.java)
            context.startActivity(intent)
        }
    }

//    private fun setAppBarHeight() {
//
//        binding.toolbarLayout.appBarLayout.layoutParams = CoordinatorLayout.LayoutParams(
//            ViewGroup.LayoutParams.MATCH_PARENT,
//            getStatusBarHeight() + dpToPx(48 + 56)
//        )
//    }
//
//    private fun getStatusBarHeight(): Int {
//        var result = 0
//        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
//        if (resourceId > 0) {
//            result = resources.getDimensionPixelSize(resourceId)
//        }
//        return result
//    }
//
//    private fun dpToPx(dp: Int): Int {
//        val density = resources
//            .displayMetrics.density
//        return Math.round(dp.toFloat() * density)
//    }
}