package com.music_event_app.ui.termscondition

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.TermsConditionResponse
import com.music_event_app.domain.usecase.LoginSignupUseCase
import com.music_event_app.domain.usecase.TermsConditionUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

class TermsConditionViewModel constructor(private val termsConditionUseCase: TermsConditionUseCase):BaseViewModel(){
    val TAG = TermsConditionViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarHandler = MutableLiveData<ErrorModel?>()
    val responseLiveData = MutableLiveData<TermsConditionResponse>()
    val isProcessing = MutableLiveData<Boolean>()

    @ExperimentalCoroutinesApi
    fun getTermsAndCondition() {
        isProcessing.value = true
        termsConditionUseCase.invoke(scope, null, object : UseCaseResponse<TermsConditionResponse> {
            override fun onSuccess(result: TermsConditionResponse) {
                Log.i(TAG, "result: $result")
                isProcessing.value = false
                responseLiveData.value = result
            }

            override fun onError(errorModel: ErrorModel?) {
                isProcessing.postValue(false)
                mSnackbarHandler.postValue(errorModel)
            }
        })
    }
}