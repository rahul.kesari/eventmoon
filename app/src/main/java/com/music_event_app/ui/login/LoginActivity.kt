package com.music_event_app.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.facebook.*
import com.facebook.internal.CallbackManagerImpl
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.music_event_app.R
import com.music_event_app.databinding.ActivityLoginBinding
import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.domain.model.Results
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.ui.base.LocationBaseActivity
import com.music_event_app.ui.dialogs.ForgetPasswordDialog
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.ui.signup.SignupActivity
import com.music_event_app.ui.termscondition.TermsConditionActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.hideKeyboard
import com.music_event_app.util.helper.Prefs
import com.music_event_app.util.helper.SnackBarHelper
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class LoginActivity : BaseActivity<LoginViewModel, ActivityLoginBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_login
    override val viewModel: LoginViewModel by viewModel()


    private val TAG = "LoginActivity"
    private val RC_SIGN_IN = 5


    private var accessToken: AccessToken? = null
    private var callbackManager: CallbackManager? = null
    private var ids: String? = ""
    var mLoginRequest: LoginRequest? = null
    var apiHitCounter: Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initFacebook()
        initViews()
        observeViewModel()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initViews() {
        binding.btnLogin.setOnClickListener {
            if (validateFields()) {
                loginWithEmail()
            }
        }

        binding.createaccBtn.setOnClickListener {
            SignupActivity.start(this)
        }

        binding.googleBtn.setOnClickListener {
            handleGoogleLogin()
        }

        binding.facebookBtn.setOnClickListener {
            handleFacebookLogin()
        }

        binding.termsTv.setOnClickListener {
            TermsConditionActivity.start(this)
        }

        binding.forgetpasswordBtn.setOnClickListener {
            openForgetPasswordDialog()
        }

        binding.skipBtn.setOnClickListener {
            HomeActivity.start(this)
        }
    }

    private fun validateFields(): Boolean {
        if (binding.emailEt.text.toString().isEmpty()) {
            SnackBarHelper.showErrorFromTop(
                getString(R.string.enter_email_string),
                this@LoginActivity,
                binding.container
            )
            return false
        } else if (!validateEmail(binding.emailEt.text.toString())) {
            SnackBarHelper.showErrorFromTop(
                getString(R.string.enter_valid_email_string),
                this@LoginActivity,
                binding.container
            )
            return false
        } else if (binding.passowrdEt.text.toString().isEmpty()) {
            SnackBarHelper.showErrorFromTop(
                getString(R.string.enter_password_string),
                this@LoginActivity,
                binding.container
            )
            return false
        } else if (binding.passowrdEt.text.toString().length < 5) {
            SnackBarHelper.showErrorFromTop(
                getString(R.string.password_length_check),
                this@LoginActivity,
                binding.container
            )
            return false
        } else {
            return true
        }
    }

    private fun initFacebook() {

        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    accessToken = loginResult.accessToken

                    getFacebookUserProfile(accessToken)
                }

                override fun onCancel() {
                    Toast.makeText(
                        this@LoginActivity,
                        "Login with facebook canceled.",
                        Toast.LENGTH_LONG
                    ).show()
                }

                override fun onError(error: FacebookException) {
                    Toast.makeText(this@LoginActivity, error.message, Toast.LENGTH_LONG).show()
                }
            })
    }


    private fun handleFacebookLogin() {
        accessToken = AccessToken.getCurrentAccessToken()

        if (accessToken != null) {
            getFacebookUserProfile(accessToken)
        } else {
            LoginManager.getInstance().logInWithReadPermissions(
                this@LoginActivity,
                Arrays.asList("public_profile", "email", "user_friends")
            )
        }
    }

    private fun getFacebookUserProfile(accessToken: AccessToken?) {
        val request = GraphRequest.newMeRequest(accessToken) { jsonObject, response ->
            try {

                if (jsonObject != null) {
                    loginWithFacebook(jsonObject)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        val parameters = Bundle()
        parameters.putString("fields", "id, name, first_name,last_name, email")
        request.parameters = parameters
        request.executeAsync()
    }


    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }


    private fun handleGoogleLogin() {
        var googleSignInClient: GoogleSignInClient? = null
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (RC_SIGN_IN == requestCode) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            if (account != null && account.id != null)
                loginWithGoogle(account)
        } catch (e: ApiException) {
            Log.d(TAG, "signInResult:failed code=" + e.statusCode)
        }
    }


    fun observeViewModel() {
        with(viewModel) {
            responseLiveData.observe(this@LoginActivity, Observer {
                if (it.Results is String) {
                    SnackBarHelper.showErrorFromTop(
                        it.Results,
                        this@LoginActivity,
                        binding.container
                    )
                } else {
                    viewModel.saveResponseToLocalStorage(it.Results)
                }
            })

            isDataSavedLocally.observe(this@LoginActivity, Observer {
                if (it) HomeActivity.start(this@LoginActivity)
            })

            isProcessing.observe(this@LoginActivity, Observer {
                if (it) {
                    binding.progressLayout.visibility = View.VISIBLE
                    hideKeyboard()
                } else binding.progressLayout.visibility = View.GONE
            })

            mSnackbarText.observe(this@LoginActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@LoginActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@LoginActivity)
                }
            })

        }
    }


    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> viewModel.loginWithGoogle(mLoginRequest)
            2 -> viewModel.loginWithFacebook(mLoginRequest)
            3 -> viewModel.loginWithEmail(mLoginRequest)
            4 -> viewModel.forgetPassword(mLoginRequest)
        }
    }


    private fun loginWithGoogle(account: GoogleSignInAccount?) {
        apiHitCounter = 1
        var loginRequest = LoginRequest(
            City = Prefs.getString(AppConstants.PreferenceConstants.STATE),
            Country = Prefs.getString(AppConstants.PreferenceConstants.COUNTRY),
            State = Prefs.getString(AppConstants.PreferenceConstants.STATE),
            appname = "eventmoon",
            deviceToken = "",
            deviceType = "Android",
            emailId = account?.email,
            gid = account?.id,
            isTermsChecked = "1",
            name = account?.displayName
        )
        mLoginRequest = loginRequest
        viewModel.loginWithGoogle(loginRequest)
    }


    private fun loginWithFacebook(jsonObject: JSONObject) {
        apiHitCounter = 2
        var loginRequest = LoginRequest(
            City = Prefs.getString(AppConstants.PreferenceConstants.STATE),
            Country = Prefs.getString(AppConstants.PreferenceConstants.COUNTRY),
            State = Prefs.getString(AppConstants.PreferenceConstants.STATE),
            appname = "eventmoon",
            deviceToken = "",
            deviceType = "Android",
            emailId = jsonObject.optString("email"),
            fbid = jsonObject.getString("id"),
            isTermsChecked = "1",
            name = jsonObject.getString("first_name") + " " + jsonObject.getString("last_name"),
            filepath = "http://graph.facebook.com/" + jsonObject.getString("id") + "/picture?type=large"
        )
        mLoginRequest = loginRequest
        viewModel.loginWithFacebook(loginRequest)
    }


    private fun loginWithEmail() {
        apiHitCounter = 3
        var loginRequest = LoginRequest(
            City = Prefs.getString(AppConstants.PreferenceConstants.STATE),
            Country = Prefs.getString(AppConstants.PreferenceConstants.COUNTRY),
            State = Prefs.getString(AppConstants.PreferenceConstants.STATE),
            appname = "eventmoon",
            deviceToken = "",
            deviceType = "Android",
            emailId = binding.emailEt.text.toString(),
            isTermsChecked = "1",
            password = binding.passowrdEt.text.toString()
        )
        mLoginRequest = loginRequest
        viewModel.loginWithEmail(loginRequest)
    }

    private fun forgetPassword(email: String?) {
        apiHitCounter = 4
        var loginRequest = LoginRequest(
            emailId = email
        )
        mLoginRequest = loginRequest
        viewModel.forgetPassword(loginRequest)
    }

    private fun openForgetPasswordDialog() {
        val dialog =
            ForgetPasswordDialog(this, object : ForgetPasswordDialog.ForgetPasswordListener {
                override fun onEmail(email: String?) {
                    forgetPassword(email)
                }
            })
        dialog.show()
    }

}
