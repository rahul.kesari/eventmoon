package com.music_event_app.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.music_event_app.R
import com.music_event_app.databinding.ActivityHomeBinding
import com.music_event_app.databinding.ActivitySplashBinding
import com.music_event_app.ui.appintro.AppIntroActivity
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.ui.login.LoginActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.TransitionAnimation
import com.music_event_app.util.extensions.newFragmentInstance
import com.music_event_app.util.helper.Prefs
import org.koin.android.viewmodel.ext.android.viewModel

class SplashActivity : BaseActivity<SplashViewModel, ActivitySplashBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_splash
    override val viewModel: SplashViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        navigateToIntro()
    }

    private fun navigateToIntro() {
        Handler().postDelayed({
            if (Prefs.getBoolean(AppConstants.PreferenceConstants.IS_INTRO_DONE, false)) {
                //check if user is logged in
                if(Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN,"")!!.isEmpty()){
                    //not logged in
                    LoginActivity.start(this)
                }else{
                    HomeActivity.start(this)
                }
            } else {
                AppIntroActivity.start(this)
            }

            finish()
        }, AppConstants.SPLASH_DELAY.toLong())
    }
}
