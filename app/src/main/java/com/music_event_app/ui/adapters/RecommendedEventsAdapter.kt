package com.music_event_app.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.*
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.locationsearch.Prediction
import com.music_event_app.domain.model.story.StoryListItem
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.util.helper.CommonHelper
import com.music_event_app.util.helper.GlideUtils

/**
 * Created by rahul
 */

class RecommendedEventsAdapter(
    private val datas: ArrayList<EventItem?>,
    private val onItemClickListener: RecyclerItemListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                ItemEventRecommendedBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return RecommendEventVH(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RecommendEventVH) {

            val data = (datas[position] as EventItem?)


            holder.itemBinding.parentCard.setOnClickListener {
                onItemClickListener?.onItemSelected(data as Any, position)
            }
            GlideUtils.loadImage(
                mContext,
                data?.imagepath,
                holder.itemBinding.eventIv,
                R.color.gray_color
            )
            holder.itemBinding.eventNameTv.text = data?.eventName
            holder.itemBinding.locationNameTv.text = data?.location
            holder.itemBinding.timeTv.text = data?.startTime
            holder.itemBinding.distanceTv.text = data?.NearMeDistabnce + " mile(s) away"

//            holder.itemBinding.peoplesGoingCountTV.text =
//                "" + data?.activityCount!! + " peoples are going"

        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class RecommendEventVH(val itemBinding: ItemEventRecommendedBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

}

