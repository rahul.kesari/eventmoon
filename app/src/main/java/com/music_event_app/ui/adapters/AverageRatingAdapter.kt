package com.music_event_app.ui.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.*
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.reviewsingle.RatingByVotesModel
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.util.helper.CommonHelper
import com.music_event_app.util.helper.GlideUtils
import kotlinx.android.synthetic.main.activity_rating_list.view.*

/**
 * Created by rahul
 */

class AverageRatingAdapter(
    private val datas: List<RatingByVotesModel?>,
    private val onItemClickListener: RecyclerItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TAG = EventListAdapter.javaClass.name

    companion object {

        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                ItemAverageRatingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return AverageRatingViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is AverageRatingViewHolder) {

            val data = datas[position] as RatingByVotesModel
            holder.itemBinding.starRatingRB.rating = data.Ratings!!.toFloat()
            holder.itemBinding.totalVotesTV.text = data.VotesByRating+" Vote(s)"
            holder.itemBinding.progressBar.progress = data.PercentageByVotes!!.toFloat().toInt()

        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class AverageRatingViewHolder(val itemBinding: ItemAverageRatingBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

}

