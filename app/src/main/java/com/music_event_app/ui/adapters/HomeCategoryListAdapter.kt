package com.music_event_app.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.ItemCategoryBinding
import com.music_event_app.databinding.ProgressbarItemBinding
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener

/**
 * Created by rahul
 */

class HomeCategoryListAdapter(
    private val datas: List<CategoryItem?>,
    private val onItemClickListener: RecyclerItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    open var selectedCategoryId: Int? = -3

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return CategoryViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CategoryViewHolder) {

            holder.itemBinding.categoryNameTv.text = (datas[position] as CategoryItem).CategoryName
            holder.itemBinding.categoryNameTv.setOnClickListener {
                if (selectedCategoryId != datas[position]?.Categoryid) {
                    selectedCategoryId = datas[position]?.Categoryid
                    onItemClickListener?.onItemSelected(datas[position] as Any, position)
                }
            }
            if (selectedCategoryId == (datas[position] as CategoryItem).Categoryid) {
                holder.itemBinding.categoryNameTv.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.white
                    )
                )
                holder.itemBinding.categoryNameTv.background =
                    ContextCompat.getDrawable(mContext, R.drawable.category_bg)
            } else {
                holder.itemBinding.categoryNameTv.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.black
                    )
                )
                holder.itemBinding.categoryNameTv.background = null
            }
        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class CategoryViewHolder(val itemBinding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

}

