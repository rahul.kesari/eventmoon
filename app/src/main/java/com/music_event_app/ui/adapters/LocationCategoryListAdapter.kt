package com.music_event_app.ui.adapters

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.ItemLocationCategoryBinding
import com.music_event_app.databinding.ProgressbarItemBinding
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.nearbyevents.TimeCategory
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener


/**
 * Created by rahul
 */

class LocationCategoryListAdapter(
    private val datas: List<TimeCategory?>,
    private val onItemClickListener: RecyclerItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    open var selectedCategoryId: Int? = -3

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                ItemLocationCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)



            val layoutParams: ViewGroup.LayoutParams = itemBinding.parent.layoutParams
            layoutParams.width = ((getScreenWidth() * 0.34).toInt())
            itemBinding.parent.layoutParams = layoutParams

            return CategoryViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CategoryViewHolder) {

            holder.itemBinding.categoryNameTV.text = (datas[position] as TimeCategory).category
            holder.itemBinding.categoryNameTV.setOnClickListener {
                if (selectedCategoryId != datas[position]?.categoryId) {
                    selectedCategoryId = datas[position]?.categoryId
                    onItemClickListener?.onItemSelected(datas[position] as Any, position)
                }
            }
            if (selectedCategoryId == (datas[position] as TimeCategory).categoryId) {
                holder.itemBinding.timeTV.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.blue
                    )
                )
            } else {
                holder.itemBinding.timeTV.setTextColor(
                    ContextCompat.getColor(
                        mContext,
                        R.color.black
                    )
                )
            }
        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class CategoryViewHolder(val itemBinding: ItemLocationCategoryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

    fun getScreenWidth(): Int {
        return Resources.getSystem().getDisplayMetrics().widthPixels
    }
}

