package com.music_event_app.ui.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.ItemCategoryBinding
import com.music_event_app.databinding.ItemEventListBinding
import com.music_event_app.databinding.ItemEventSearchChildListBinding
import com.music_event_app.databinding.ProgressbarItemBinding
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.util.helper.CommonHelper
import com.music_event_app.util.helper.GlideUtils

/**
 * Created by rahul
 */

class EventSearchChildListAdapter(
    private val datas: List<EventItem?>,
    private val onItemClickListener: EventChildListItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TAG = EventListAdapter.javaClass.name

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                ItemEventSearchChildListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return EventSearchChildViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventSearchChildViewHolder) {

            val data = datas[position] as EventItem

            holder.itemBinding.categoryNameTv.text = (data?.categoryName)

            if (data?.categoryName!!.isEmpty()) {
                holder.itemBinding.categoryNameTv.visibility = View.GONE
            } else {
                holder.itemBinding.categoryNameTv.visibility = View.VISIBLE
            }

            if (data?.isInterested.equals("0")) {
                holder.itemBinding.letsGoBtn.visibility = View.VISIBLE
            } else {
                holder.itemBinding.letsGoBtn.visibility = View.INVISIBLE
            }

            holder.itemBinding.parentCard.setOnClickListener {
                onItemClickListener?.onItemClicked(data, position)
            }
            holder.itemBinding.letsGoBtn.setOnClickListener {
                onItemClickListener?.onReminderClicked(data,position)
            }

            GlideUtils.loadImage(
                mContext,
                data.imagepath,
                holder.itemBinding.eventIv,
                R.color.gray_color
            )
            holder.itemBinding.eventNameTv.text = data?.eventName
            holder.itemBinding.locationNameTv.text = data?.location
            holder.itemBinding.timeTv.text = CommonHelper.fromHtml("<html><font color='#ff0000'>"+ data?.startTime +"</font> "+" <b><font color='#ff0000' size='50px'>" + data?.nearMe + " miles</font></b></html>")
        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class EventSearchChildViewHolder(val itemBinding: ItemEventSearchChildListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

    interface EventChildListItemListener {
        fun onItemClicked(item: Any, position: Int)

        fun onReminderClicked(item: Any, position: Int)
    }
}

