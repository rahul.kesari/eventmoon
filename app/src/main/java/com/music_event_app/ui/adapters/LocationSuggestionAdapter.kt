package com.music_event_app.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.*
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.locationsearch.Prediction
import com.music_event_app.domain.model.story.StoryListItem
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.util.helper.GlideUtils

/**
 * Created by rahul
 */

class LocationSuggestionAdapter(
    private val datas: ArrayList<Prediction?>,
    private val onItemClickListener: RecyclerItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                ItemLocationSuggestionBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return LocationSuggestionViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LocationSuggestionViewHolder) {

            val data = (datas[position] as Prediction)
            holder.itemBinding.locationNameTv.text = data.description
            holder.itemBinding.parentCard.setOnClickListener {
                onItemClickListener?.onItemSelected(datas[position] as Any, position)
            }

        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class LocationSuggestionViewHolder(val itemBinding: ItemLocationSuggestionBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

}

