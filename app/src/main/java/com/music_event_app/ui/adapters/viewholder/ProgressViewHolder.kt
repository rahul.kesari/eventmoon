package com.music_event_app.ui.adapters.viewholder

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.databinding.ProgressbarItemBinding

class ProgressViewHolder(private val itemBinding: ProgressbarItemBinding) : RecyclerView.ViewHolder(itemBinding.root) {
    fun bind() {
        Log.d("ProgressViewHolder", "progressbar ")
        itemBinding.progressBar1.setIndeterminate(true)
    }
}