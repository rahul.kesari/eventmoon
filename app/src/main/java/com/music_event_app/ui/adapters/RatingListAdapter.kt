package com.music_event_app.ui.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.ItemCategoryBinding
import com.music_event_app.databinding.ItemEventListBinding
import com.music_event_app.databinding.ItemRatinglistAdapterBinding
import com.music_event_app.databinding.ProgressbarItemBinding
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.reviewsingle.ReviewDescriptionWithModel
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.util.helper.CommonHelper
import com.music_event_app.util.helper.GlideUtils

/**
 * Created by rahul
 */

class RatingListAdapter(
    private val datas: List<ReviewDescriptionWithModel?>,
    private val onItemClickListener: RatingRecyclerItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TAG = EventListAdapter.javaClass.name

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding = ItemRatinglistAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return RatingViewHolder(itemBinding)
        } else {
            val itemBinding = ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RatingViewHolder) {
            val  data = datas.get(position)
            GlideUtils.loadImage(mContext,data!!.UserImagePath,holder.itemBinding.profileIv,R.drawable.create_image)
            holder.itemBinding.userNameTV.text = data.Name
            holder.itemBinding.startRatingRB.rating = data.Ratings!!.toFloat()
            holder.itemBinding.likeCountTV.text= data.TotalLike
            holder.itemBinding.disLikeCountTV.text = data.TotalDisLike
            holder.itemBinding.timeTV.text = data.PostedDate

            holder.itemBinding.likeIV.setOnClickListener {
                onItemClickListener.onLikeClicked(data,position)
            }

            holder.itemBinding.disLikeIV.setOnClickListener {
                onItemClickListener.onDislikeClicked(data,position)
            }
        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class RatingViewHolder(val itemBinding: ItemRatinglistAdapterBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }


    interface RatingRecyclerItemListener {
        fun onLikeClicked(item: Any,position: Int)

        fun onDislikeClicked(item: Any,position: Int)
    }
}



