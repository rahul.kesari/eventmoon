package com.music_event_app.ui.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.databinding.EventSearchListItemBinding
import com.music_event_app.databinding.ProgressbarItemBinding
import com.music_event_app.domain.model.eventsearch.Day
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener

/**
 * Created by rahul
 */

class EventSearchListAdapter(
    private val datas: List<Day?>,
    private val onItemClickListener: EventListItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TAG = EventSearchListAdapter.javaClass.name

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                EventSearchListItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return EventSearchViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventSearchViewHolder) {

            val data = datas[position] as Day

            holder.itemBinding.dayTV.text = (data?.day)

            val eventSearchListAdapter =
                EventSearchChildListAdapter(data?.data, object : EventSearchChildListAdapter.EventChildListItemListener {
                    override fun onItemClicked(item: Any, childposition: Int) {
                        onItemClickListener.onItemClicked(item,position,childposition)
                    }

                    override fun onReminderClicked(item: Any, childposition: Int) {
                        onItemClickListener.onReminderClicked(item,position,childposition)
                    }

                })
            holder.itemBinding.eventsRV.layoutManager =
                LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
            holder.itemBinding.eventsRV.isNestedScrollingEnabled = false
            holder.itemBinding.eventsRV.adapter = eventSearchListAdapter

        } else {

            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class EventSearchViewHolder(val itemBinding: EventSearchListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

    interface EventListItemListener {
        fun onItemClicked(item: Any, parentPosition: Int, childPosition: Int)

        fun onReminderClicked(item: Any,  parentPosition: Int, childPosition: Int)
    }
}

