package com.music_event_app.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.ItemCategoryBinding
import com.music_event_app.databinding.ItemEventSuggestionBinding
import com.music_event_app.databinding.ItemStoryListBinding
import com.music_event_app.databinding.ProgressbarItemBinding
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.story.StoryListItem
import com.music_event_app.ui.adapters.viewholder.ProgressViewHolder
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.util.helper.GlideUtils

/**
 * Created by rahul
 */

class EventSuggestionAdapter(
    private val datas: List<String?>,
    private val onItemClickListener: RecyclerItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_PROG = 1
        const val VIEW_ITEM = 0
    }

    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        if (viewType == VIEW_ITEM) {
            val itemBinding =
                ItemEventSuggestionBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return EventSuggestionViewHolder(itemBinding)
        } else {
            val itemBinding =
                ProgressbarItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProgressViewHolder(itemBinding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventSuggestionViewHolder) {

            val data = (datas[position] as String)
            holder.itemBinding.eventListSuggestionTv.text = data
            holder.itemBinding.parentCard.setOnClickListener {
                onItemClickListener?.onItemSelected(datas[position] as Any, position)
            }

        } else {
            (holder as ProgressViewHolder).bind()
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (datas.get(position) != null) VIEW_ITEM else VIEW_PROG
    }


    class EventSuggestionViewHolder(val itemBinding: ItemEventSuggestionBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
    }

}

