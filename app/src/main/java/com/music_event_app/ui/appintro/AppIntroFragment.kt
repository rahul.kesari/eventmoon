package com.music_event_app.ui.appintro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil

import androidx.fragment.app.Fragment;

import com.music_event_app.R;
import com.music_event_app.databinding.FragmentAppintrofirstBinding;

class AppIntroFragment : Fragment() {

    lateinit var mBinding: FragmentAppintrofirstBinding
    var screenNo: Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_appintrofirst, container, false)
        screenNo = arguments?.getInt(ARG_PARAM) as Int
        handleScreens()
        return mBinding.root
    }


    private fun handleScreens() {
        when (screenNo) {
            1 -> {
//                mBinding.titleIv.setImageResource(R.drawable.app_intro_one)
                mBinding.titleIv.setAnimation("appintro_screen_1.json")
                mBinding.titleIv.loop(true)
                mBinding.titleIv.playAnimation()
                mBinding.titleTv.text = getString(R.string.app_intro_title_one)
//                mBinding.subTitleTv.text = getString(R.string.app_intro_subtitle_one)
            }
            2 -> {
                mBinding.titleIv.setAnimation("appintro_screen_2.json")
                mBinding.titleIv.loop(true)
                mBinding.titleIv.playAnimation()

//                mBinding.titleIv.setImageResource(R.drawable.ic_app_intro_two)
                mBinding.titleTv.text = getString(R.string.app_intro_title_two)
//                mBinding.subTitleTv.text = getString(R.string.app_intro_subtitle_two)
            }
            else -> {
                mBinding.titleIv.setAnimation("appintro_screen_3.json")
                mBinding.titleIv.loop(true)
                mBinding.titleIv.playAnimation()

//                mBinding.titleIv.setImageResource(R.drawable.ic_app_intro_three)
                mBinding.titleTv.text = getString(R.string.app_intro_title_three)
//                mBinding.subTitleTv.text = getString(R.string.app_intro_subtitle_three)
            }
        }
    }


    companion object {
        private const val ARG_PARAM = "param"
        fun newInstance(id: Int): AppIntroFragment {
            val fragment = AppIntroFragment()
            val args = Bundle()
            args.putInt(ARG_PARAM, id)
            fragment.arguments = args
            return fragment
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser && isAdded){
            mBinding.titleIv.playAnimation()
        }
    }
}