package com.music_event_app.ui.appintro

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.music_event_app.R
import com.music_event_app.databinding.ActivityAppintroBinding
import com.music_event_app.ui.adapters.pageradapter.HomePagerAdapter
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.ui.login.LoginActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.Prefs

class AppIntroActivity : AppCompatActivity() {


    lateinit var binding: ActivityAppintroBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        binding = DataBindingUtil.setContentView(this, R.layout.activity_appintro)
        initViewPager()
    }

    private fun initViewPager() {
        val adapter = HomePagerAdapter(supportFragmentManager)
        adapter.addFragment(AppIntroFragment.newInstance(1), "")
        adapter.addFragment(AppIntroFragment.newInstance(2), "")
        adapter.addFragment(AppIntroFragment.newInstance(3), "")
        binding.viewpager.offscreenPageLimit = 3
        binding.viewpager.adapter = adapter

        binding.viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 2) {
                    binding.skipBtn.visibility = View.VISIBLE
                    binding.startBtn.visibility = View.VISIBLE
                } else {
                    binding.skipBtn.visibility = View.GONE
                    binding.startBtn.visibility = View.GONE
                }
            }
        })

        binding.startBtn.setOnClickListener {
            Prefs.putBoolean(AppConstants.PreferenceConstants.IS_INTRO_DONE, true)
            LoginActivity.start(this)
            finish()
        }
        binding.skipBtn.setOnClickListener {
            Prefs.putBoolean(AppConstants.PreferenceConstants.IS_INTRO_DONE, true)
            HomeActivity.start(this)
            finish()
        }
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, AppIntroActivity::class.java)
            context.startActivity(intent)
        }
    }
}
