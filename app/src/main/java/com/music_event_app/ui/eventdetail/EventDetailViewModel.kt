package com.music_event_app.ui.eventdetail

import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.EventDetailUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

class EventDetailViewModel  constructor(private val eventDetailUseCase: EventDetailUseCase)  :BaseViewModel(){

    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel>()
    val eventDetailLiveData = MutableLiveData<BaseResponse>()
    val isProcessing = MutableLiveData<Boolean>()
    val mSnackbarHandler = MutableLiveData<ErrorModel?>()
    val eventRatingSubmitLiveData = MutableLiveData<BaseResponse>()

    val eventReviewLiveData = MutableLiveData<BaseResponse>()
    val eventReminderLiveData = MutableLiveData<BaseResponse>()

    @ExperimentalCoroutinesApi
    fun loadEventDetail(commonRequest: CommonRequest) {
        isProcessing.value = true
        eventDetailUseCase.loadEventDetail(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventDetailLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }


    @ExperimentalCoroutinesApi
    fun submitRatings(commonRequest: CommonRequest) {
        isProcessing.value = true
        eventDetailUseCase.submitRatings(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventRatingSubmitLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun getEventReview(commonRequest: CommonRequest) {
        isProcessing.value = true
        eventDetailUseCase.getEventReview(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventReviewLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun hitReminder(commonRequest: CommonRequest) {
        isProcessing.value = true
        eventDetailUseCase.hitReminder(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventReminderLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }
}