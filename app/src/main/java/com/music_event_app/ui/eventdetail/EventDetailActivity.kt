package com.music_event_app.ui.eventdetail

import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.music_event_app.R
import com.music_event_app.databinding.ActivityEventDetailBinding
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.model.reviewsingle.SingleReview
import com.music_event_app.ui.adapters.RecommendedEventsAdapter
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.ui.buyticket.BuyTicketActivity
import com.music_event_app.ui.dialogs.RateFeedbackDialog
import com.music_event_app.ui.ratinglist.RatingListActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList

class EventDetailActivity : BaseActivity<EventDetailViewModel, ActivityEventDetailBinding>(),
    OnMapReadyCallback {

    override var frameContainerId: Int = R.id.container
    override val layoutId: Int = R.layout.activity_event_detail
    override val viewModel: EventDetailViewModel by viewModel()
    private var mMap: GoogleMap? = null
    private val zoom = 15f

    private lateinit var recommendedEventsAdapter: RecommendedEventsAdapter
    private var recommendEventList = ArrayList<EventItem?>()
    var apiHitCounter: Int = 1
    private var eventid = 0
    private var reviewText = ""
    private var reviewRatings = 0.0f
    private var isInterested = "0" //for handling notification subscription is on or off
    private var ticketUrl = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        eventid = intent.extras!!.getInt(EVENT_ID)
        setSupportActionBar(binding.toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        initViews()
        initRecommendedRecycler()
        addGoogleMap()
        observeViewModel()
        getEventDetail()
        handleNotificationAnimation()

    }

    private fun initViews() {

        binding.eventContent.rateEventBtn.setOnClickListener {
            showRateDialog()
        }

        binding.toolbar.setNavigationOnClickListener {
            finish()
        }

        binding.shareIV.setOnClickListener {

            CommonHelper.shareContent(
                this@EventDetailActivity,
                "Hey there,\n\nPlease use this awesome application for local Music Events!!!.\n\n https://play.google.com/store/apps/details?id=com.music_event_app"
            )

        }

        binding.eventContent.viewAllReviewBtn.setOnClickListener {

            RatingListActivity.start(this@EventDetailActivity, eventid)

        }

        binding.eventContent.notifyAnimationView.setOnClickListener {
            if (isInterested == "0") {

                binding.eventContent.notifyAnimationView.visibility = View.VISIBLE
                //subscribe for notification
                isInterested = "1"
                binding.eventContent.notifyAnimationView.playAnimation()

                //hit notification Reminder API
                hitReminder()
            }
        }

        binding.eventContent.seeAllEventBtn.setOnClickListener {
            if (ticketUrl.isNotEmpty())
               BuyTicketActivity.start(this@EventDetailActivity,ticketUrl)
        }
    }

    private fun handleNotificationAnimation() {

        binding.eventContent.notifyAnimationView.addAnimatorListener(object :
            Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                //Your code for remove the fragment
//                binding.eventContent.notificationActiveIV.visibility = View.VISIBLE
//                binding.eventContent.notifyAnimationView.visibility = View.INVISIBLE

            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationRepeat(animation: Animator?) {

            }
        })
    }


    private fun showRateDialog() {

        val rateFeedbackDialog = RateFeedbackDialog(this, object : RateFeedbackDialog.RateFeedbackDialgListener {
                override fun onSubmit(review: String?, ratings: Float) {

                    reviewText = review!!
                    reviewRatings = ratings
                    submitReviewRatings()

                }
            })
        rateFeedbackDialog.show()

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap?.uiSettings?.isMapToolbarEnabled = false
        mMap?.uiSettings?.isMyLocationButtonEnabled = false
        mMap?.uiSettings?.isZoomControlsEnabled = false
        mMap?.uiSettings?.isIndoorLevelPickerEnabled = false
        mMap?.animateCamera(CameraUpdateFactory.zoomIn())
        mMap?.animateCamera(CameraUpdateFactory.zoomTo(zoom), 5000, null)
    }

    /*Add Map in our fragment */
    private fun addGoogleMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this@EventDetailActivity)
    }

    companion object {

        const val EVENT_ID = "EVENT_ID"
        fun start(context: Context, eventId: Int) {
            val intent = Intent(context, EventDetailActivity::class.java)
            intent.putExtra(EVENT_ID, eventId)
            context.startActivity(intent)
        }
    }

    private fun initRecommendedRecycler() {

        recommendedEventsAdapter = RecommendedEventsAdapter(recommendEventList, object :
            RecyclerItemListener {
            override fun onItemSelected(item: Any, position: Int) {
                EventDetailActivity.start(this@EventDetailActivity, (item as? EventItem)?.eventId!!)
            }
        })
        binding.eventContent.recommendedEventsRV.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.eventContent.recommendedEventsRV.adapter = recommendedEventsAdapter

    }


    fun observeViewModel() {

        with(viewModel) {

            isProcessing.observe(this@EventDetailActivity, Observer {
                if (it) binding.progressLayout.visibility =
                    View.VISIBLE else binding.progressLayout.visibility = View.GONE
            })

            eventDetailLiveData.observe(this@EventDetailActivity, Observer {
                val eventDetail = ObjectConverter.getEventDetailResponse(it.Results)
                renderEventDetailResponse(eventDetail)
                getEventReview()
            })

            eventRatingSubmitLiveData.observe(this@EventDetailActivity, Observer {
                MessagesUtils.showToastSuccess(
                    this@EventDetailActivity,
                    "Review Submitted Successfully"
                )
            })

            eventReminderLiveData.observe(this@EventDetailActivity, Observer {

            })

            eventReviewLiveData.observe(this@EventDetailActivity, Observer {
                val eventDetail = ObjectConverter.getEventSingleReviewResponse(it.Results)
                renderEventReview(eventDetail)
            })

            mSnackbarHandler.observe(this@EventDetailActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@EventDetailActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@EventDetailActivity)
                }
            })
        }
    }

    private fun renderEventDetailResponse(data: EventItem?) {

        isInterested = data?.isInterested!!
        ticketUrl = data?.ticketUrl!!


        if (isInterested.equals("0")) {
            binding.eventContent.notifyAnimationView.visibility = View.VISIBLE
            binding.eventContent.notificationActiveIV.visibility = View.GONE
        } else {
            binding.eventContent.notifyAnimationView.visibility = View.INVISIBLE
            binding.eventContent.notificationActiveIV.visibility = View.VISIBLE
        }

        GlideUtils.loadImage(this, data!!.imagepath, binding.eventIv, R.drawable.header_bg)

        if (data.interestedCount.equals("0")) {
            binding.eventContent.interestedTV.visibility = View.GONE
        } else {
            binding.eventContent.interestedTV.text = data.interestedCount + " People are interested"
        }
        binding.eventContent.eventDateTv.text = data.startTime
        binding.eventContent.eventNameTVTwo.text = data.eventName!!.trim()
        binding.eventContent.addressTV.text = data.location
        binding.eventContent.distanceTV.text = data.nearMe + " miles away"
        binding.eventContent.eventDetailsTV.text = data.eventDescription


        if (data.IsTrending.equals("0")) {
            binding.eventContent.trendingLayout.visibility = View.INVISIBLE
        } else {
            binding.eventContent.trendingLayout.visibility = View.VISIBLE
        }

        if (data.nearMeEvents?.size!! > 0) {
            recommendEventList.addAll(data.nearMeEvents)
            recommendedEventsAdapter.notifyDataSetChanged()
        } else {
            binding.eventContent.recommendedEventLayout.visibility = View.GONE
        }

        binding.eventContent.totalRatingsTV.text =
            String.format(Locale.US, "%.2f", data.ratings!!.toFloat())
        binding.eventContent.totalStartRatingRB1.rating = data.ratings!!.toFloat()
        binding.eventContent.totalStarRatingRB2.rating = data.ratings!!.toFloat()
        binding.eventContent.totalReviewsCountTV.text = "(" + data.ratingCount + ")"

        binding.eventContent.getDirectionBtn.setOnClickListener {
            CommonHelper.navigateToMap(this@EventDetailActivity, data.latitude!!, data.longitude!!)
        }

        try {
            val longlat = LatLng(
                data.latitude!!.toDouble(),
                data.longitude!!.toDouble()
            )
            dropMarker(longlat, data.location!!)
            moveCamera(longlat)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun renderEventReview(data: SingleReview?) {

        if (data?.reviewDescriptionWithModel == null || data?.reviewDescriptionWithModel?.size!! == 0) {
            binding.eventContent.ratingLayout.ratingLayout.visibility = View.GONE
            binding.eventContent.viewAllReviewBtn.visibility = View.GONE
            return
        }

        GlideUtils.loadImage(
            this@EventDetailActivity,
            data?.reviewDescriptionWithModel?.get(0)?.UserImagePath,
            binding.eventContent.ratingLayout.profileIv,
            R.drawable.create_image
        )
        binding.eventContent.ratingLayout.userNameTV.text =
            data?.reviewDescriptionWithModel?.get(0)?.Name
        binding.eventContent.ratingLayout.ratingBar.rating =
            data?.reviewDescriptionWithModel?.get(0)?.Ratings!!.toFloat()
        binding.eventContent.ratingLayout.reviewDateTV.text =
            data?.reviewDescriptionWithModel?.get(0)?.PostedDate
        binding.eventContent.ratingLayout.commentsTV.text =
            data?.reviewDescriptionWithModel?.get(0)?.ReviewText

    }

    private fun dropMarker(mUsersLocation: LatLng, location: String) {
        val markerOptions = MarkerOptions()
        markerOptions.position(mUsersLocation)
        markerOptions.title(location)
        mMap!!.addMarker(markerOptions)
    }

    private fun moveCamera(location: LatLng) {
        if (mMap == null) return
        val cameraPosition = CameraPosition.Builder()
            .target(location) // Sets the center of the map to Mountain View
            .zoom(zoom) // Sets the zoom
            .bearing(0f) // Sets the orientation of the camera to east
            .tilt(30f) // Sets the tilt of the camera to 30 degrees
            .build() // .tilt(30)                    // Creates a CameraPosition from the builder
        mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun getEventDetail() {
        apiHitCounter = 1

        val address = CommonHelper.getAddressToUse()
        val commonRequest = CommonRequest(pageNumber = 0)

        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.dateTimeNow = DateUtils.getTodayDateTime()
        commonRequest.latitude = address[0]
        commonRequest.longitude = address[1]
        commonRequest.isFromWeb = "0"
        commonRequest.eventId = eventid

        viewModel.loadEventDetail(commonRequest)
    }

    private fun getEventReview() {
        apiHitCounter = 3
        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.eventId = eventid
        viewModel.getEventReview(commonRequest)
    }

    private fun submitReviewRatings() {
        apiHitCounter = 2

        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.ratings = reviewRatings
        commonRequest.reviewText = reviewText
        commonRequest.eventId = eventid

        viewModel.submitRatings(commonRequest)
    }

    private fun hitReminder() {
        apiHitCounter = 4

        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.startDate = DateUtils.getTodayDateTime()
//        commonRequest.image =
        commonRequest.deviceType = "android"
        commonRequest.appName = "eventmoon"
        commonRequest.eventId = eventid

        viewModel.hitReminder(commonRequest)
    }

    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> getEventDetail()
            2 -> submitReviewRatings()
            3 -> getEventReview()
            4 -> hitReminder()
        }
    }

}
