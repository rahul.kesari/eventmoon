package com.music_event_app.ui.editprofile;

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.github.dhaval2404.imagepicker.ImagePicker
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.music_event_app.ui.base.BaseActivity
import org.koin.android.viewmodel.ext.android.viewModel
import com.music_event_app.R
import com.music_event_app.databinding.ActivityEditProfileBinding
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.GlideUtils
import com.music_event_app.util.helper.MessagesUtils
import com.music_event_app.util.helper.Prefs
import com.music_event_app.util.helper.SnackBarHelper
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.net.URLEncoder

class EditProfileActivity : BaseActivity<EditProfileViewModel, ActivityEditProfileBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_edit_profile
    override val viewModel: EditProfileViewModel by viewModel()

    private var photoPath = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupInsets(binding.topLayout)
        initViews()
        observeViewModel()
        setProfileData()
    }

    private fun initViews() {

        editViews(false)

        binding.topLayout.toolbar.setNavigationOnClickListener {
            finish()
        }
        binding.profileIv.setOnClickListener {
            initPermission()
        }
        binding.editBtn.setOnClickListener {
            editViews(true)
        }
        binding.saveBtn.setOnClickListener {
            saveDetails()
        }
    }

    private fun editViews(isEdit: Boolean) {

        binding.nameET.isFocusableInTouchMode = isEdit
        binding.emailET.isFocusableInTouchMode = isEdit
        binding.profileIv.isEnabled = isEdit

        if (isEdit) {
            binding.saveBtn.visibility = View.VISIBLE
            binding.editBtn.visibility = View.GONE
        } else {
            binding.saveBtn.visibility = View.GONE
            binding.editBtn.visibility = View.VISIBLE
        }
    }


    fun observeViewModel() {
        with(viewModel) {
            responseLiveData.observe(this@EditProfileActivity, Observer {
                if (it.Results is String) {
                    SnackBarHelper.showErrorFromTop(
                        it.Results,
                        this@EditProfileActivity,
                        binding.topLayout
                    )
                } else {
                    viewModel.saveResponseToLocalStorage(it.Results)
                }
                editViews(false)
            })

            isDataSavedLocally.observe(this@EditProfileActivity, Observer {
                if (it)
                    MessagesUtils.showToastSuccess(this@EditProfileActivity, "Updated successfully")
                finish()
            })

            isProcessing.observe(this@EditProfileActivity, Observer {
                if (it) binding.progressLayout.visibility =
                    View.VISIBLE else binding.progressLayout.visibility = View.GONE
            })

            mSnackbarHandler.observe(this@EditProfileActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message
                        , this@EditProfileActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {

                            saveDetails()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@EditProfileActivity)
                }
            })
        }
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, EditProfileActivity::class.java)
            context.startActivity(intent)
        }
    }

    private fun setProfileData() {

        binding.fullNameTV.setText(Prefs.getString(AppConstants.PreferenceConstants.NAME))
        binding.nameET.setText(Prefs.getString(AppConstants.PreferenceConstants.NAME))
        binding.emailET.setText(Prefs.getString(AppConstants.PreferenceConstants.EMAIL_ID))

        GlideUtils.loadImage(
            this@EditProfileActivity,
            Prefs.getString(AppConstants.PreferenceConstants.USER_IMAGE_PATH),
            binding.profileIv,
            R.drawable.create_image
        )
        binding.groupCB.isChecked = Prefs.getBoolean(AppConstants.PreferenceConstants.GROUP, false)
        binding.coupleCB.isChecked =
            Prefs.getBoolean(AppConstants.PreferenceConstants.COUPLE, false)
        binding.flysoloCB.isChecked =
            Prefs.getBoolean(AppConstants.PreferenceConstants.FLY_SOLO, false)

    }

    private fun pickPicture() {
        ImagePicker.with(this)
            .cameraOnly()
            .crop() //Crop image(Optional), Check Customization for more option
            .compress(624)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                624,
                624
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            val fileUri = data?.data

            binding.profileIv.setImageURI(fileUri)

            //You can also get File Path from intent
            photoPath = ImagePicker.getFilePath(data)!!


        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initPermission() {

        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted())
                        pickPicture()

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token?.continuePermissionRequest()
                }
            }).withErrorListener {

            }.check()

    }

    private fun saveDetails() {
        val builder = MultipartBody.Builder()
            .setType(MultipartBody.FORM)

        builder.addFormDataPart(
            "userID",
            Prefs.getInt(AppConstants.PreferenceConstants.USER_ID).toString()
        )
        builder.addFormDataPart(
            "userName",
            Prefs.getString(AppConstants.PreferenceConstants.USER_NAME).toString()
        )

        if(Prefs.getString(AppConstants.PreferenceConstants.PROFILE_TYPE,"").equals("Couple",true)){
            builder.addFormDataPart("groupType", "Couple")
        }else if(Prefs.getString(AppConstants.PreferenceConstants.PROFILE_TYPE,"").equals("Group",true)){
            builder.addFormDataPart("groupType", "Group")
        }else{
            builder.addFormDataPart("groupType", "FlySolo")
        }


        builder.addFormDataPart("emailId", binding.emailET.text.toString())
        builder.addFormDataPart(
            "accessToken",
            Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN).toString()
        )
        builder.addFormDataPart("name", binding.nameET.text.toString())

        if (photoPath.isNotEmpty()) {
            val MEDIA_TYPE_PNG = MediaType.parse("image/*")
            val file = File(photoPath)
            val fileName = photoPath.substring(photoPath.lastIndexOf("/") + 1)
            val addFormDataPart = builder.addFormDataPart(
                "filepath",
                URLEncoder.encode(fileName, "utf-8"),
                RequestBody.create(MEDIA_TYPE_PNG, file)
            )
        }
        viewModel.submitProfileData(builder.build())
    }
}