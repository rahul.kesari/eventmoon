package com.music_event_app.ui.editprofile

import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.TermsConditionResponse
import com.music_event_app.domain.usecase.EditProfileUseCase
import com.music_event_app.domain.usecase.TermsConditionUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.ObjectConverter
import com.music_event_app.util.helper.Prefs
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.MultipartBody

class EditProfileViewModel constructor(private val editProfileUseCase: EditProfileUseCase):BaseViewModel(){
    val TAG = EditProfileViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarHandler = MutableLiveData<ErrorModel?>()
    val responseLiveData = MutableLiveData<BaseResponse>()
    val isProcessing = MutableLiveData<Boolean>()
    val isDataSavedLocally = MutableLiveData<Boolean>()


    @ExperimentalCoroutinesApi
    fun submitProfileData(multipartBody: MultipartBody) {
        isProcessing.value = true
        editProfileUseCase.submitProfileDetail(scope, multipartBody, object : UseCaseResponse<BaseResponse> {
            override fun onSuccess(result: BaseResponse) {
                isProcessing.value = false
                responseLiveData.value = result
            }

            override fun onError(errorModel: ErrorModel?) {
                isProcessing.postValue(false)
                mSnackbarHandler.postValue(errorModel)
            }
        })
    }



    fun saveResponseToLocalStorage(responseData: Any?) {

        val results = ObjectConverter.getLoginResponse(responseData)

        Prefs.putString(AppConstants.PreferenceConstants.USER_NAME, results?.userName)
        Prefs.putString(AppConstants.PreferenceConstants.NAME, results?.name)
        Prefs.putString(AppConstants.PreferenceConstants.EMAIL_ID, results?.emailId)
        Prefs.putString(AppConstants.PreferenceConstants.USER_IMAGE_PATH, results?.userImagePath)

        Prefs.putString(AppConstants.PreferenceConstants.PROFILE_TYPE,results?.profileType)

        isDataSavedLocally.postValue(true)
    }
}