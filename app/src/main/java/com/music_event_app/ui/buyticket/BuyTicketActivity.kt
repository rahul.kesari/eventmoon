package com.music_event_app.ui.buyticket;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.music_event_app.R
import com.music_event_app.databinding.ActivityBuyTicketBinding
import com.music_event_app.ui.base.BaseActivity
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class BuyTicketActivity : BaseActivity<BuyTicketViewModel, ActivityBuyTicketBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_buy_ticket
    override val viewModel: BuyTicketViewModel by viewModel()

    private var url: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        url = intent.extras?.getString(TICKET_URL)
        setupInsets(binding.topLayout)
        initViews()
    }

    private fun initViews() {

        binding.toolbarLayout.titleTV.text = "Buy Ticket"
        binding.toolbarLayout.toolbar.setNavigationOnClickListener {
            finish()
        }
        binding.progressLayout.visibility = View.VISIBLE
        binding.webView.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (progress >= 80) {
                    binding.progressLayout.visibility = View.GONE
                }
            }
        })

        binding.webView.getSettings().setJavaScriptEnabled(true)
        binding.webView.loadUrl(url)
        binding.webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                url: String
            ): Boolean {
                view.loadUrl(url)
                return false
            }
        })
    }


    companion object {

        const val TICKET_URL = "TICKET_URL"

        fun start(context: Context, ticketUrl: String) {
            val intent = Intent(context, BuyTicketActivity::class.java)
            intent.putExtra(TICKET_URL, ticketUrl)
            context.startActivity(intent)
        }
    }


}