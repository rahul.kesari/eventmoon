package com.music_event_app.ui.signup

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.usecase.LoginSignupUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.ObjectConverter
import com.music_event_app.util.helper.Prefs
import kotlinx.coroutines.ExperimentalCoroutinesApi

class SignupViewModel constructor(private val loginSignupUseCase: LoginSignupUseCase)  :BaseViewModel(){

    val TAG = SignupViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel?>()
    val responseLiveData = MutableLiveData<BaseResponse>()
    val isProcessing = MutableLiveData<Boolean>()
    val isDataSavedLocally = MutableLiveData<Boolean>()

    @ExperimentalCoroutinesApi
    fun loginWithGoogle(loginRequest: LoginRequest?) {

        isProcessing.value = true
        loginSignupUseCase.loginWithGoogle(
            scope,
            loginRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    responseLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }


    @ExperimentalCoroutinesApi
    fun loginWithFacebook(loginRequest: LoginRequest?) {

        isProcessing.value = true
        loginSignupUseCase.loginWithFacebook(
            scope,
            loginRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    responseLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun registerWithEmail(loginRequest: LoginRequest?) {

        isProcessing.value = true
        loginSignupUseCase.registerWithEmail(
            scope,
            loginRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    responseLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }


    fun saveResponseToLocalStorage(responseData: Any?) {

        val results = ObjectConverter.getLoginResponse(responseData)

        Prefs.putInt(AppConstants.PreferenceConstants.USER_ID, results?.userId!!)
        Prefs.putString(AppConstants.PreferenceConstants.USER_NAME, results?.userName)
        Prefs.putString(AppConstants.PreferenceConstants.NAME, results?.name)
        Prefs.putString(AppConstants.PreferenceConstants.EMAIL_ID, results?.emailId)
        Prefs.putString(AppConstants.PreferenceConstants.USER_IMAGE_PATH, results?.userImagePath)
        Prefs.putString(AppConstants.PreferenceConstants.ACCESS_TOKEN, results?.accessToken)
        Prefs.putString(AppConstants.PreferenceConstants.FB_ID, results?.fbId)
        Prefs.putString(AppConstants.PreferenceConstants.GOOGLE_Id, results?.gId)

        Prefs.putString(AppConstants.PreferenceConstants.PROFILE_TYPE,results?.profileType)

        isDataSavedLocally.postValue(true)
    }
}