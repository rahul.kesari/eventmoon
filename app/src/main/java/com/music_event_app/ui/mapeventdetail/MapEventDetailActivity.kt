package com.music_event_app.ui.mapeventdetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.music_event_app.R
import com.music_event_app.databinding.ActivityMapEventDetailBinding
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.ui.adapters.RecommendedEventsAdapter
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.*
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.collections.ArrayList

class MapEventDetailActivity : BaseActivity<MapEventDetailViewModel, ActivityMapEventDetailBinding>(),
    OnMapReadyCallback {

    override var frameContainerId: Int = R.id.container
    override val layoutId: Int = R.layout.activity_map_event_detail
    override val viewModel: MapEventDetailViewModel by viewModel()
    private var mMap: GoogleMap? = null
    private val zoom = 15f

    var apiHitCounter: Int = 1
    private var eventid = 0


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        eventid = intent.extras!!.getInt(EVENT_ID)
//        setSupportActionBar(binding.toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        initViews()
        addGoogleMap()
        observeViewModel()
        getEventDetail()

    }

    private fun initViews() {




    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap?.uiSettings?.isMapToolbarEnabled = false
        mMap?.uiSettings?.isMyLocationButtonEnabled = false
        mMap?.uiSettings?.isZoomControlsEnabled = false
        mMap?.uiSettings?.isIndoorLevelPickerEnabled = false
        mMap?.animateCamera(CameraUpdateFactory.zoomIn())
        mMap?.animateCamera(CameraUpdateFactory.zoomTo(zoom), 5000, null)
    }

    /*Add Map in our fragment */
    private fun addGoogleMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this@MapEventDetailActivity)
    }

    companion object {

        const val EVENT_ID = "EVENT_ID"
        fun start(context: Context, eventId: Int) {
            val intent = Intent(context, MapEventDetailActivity::class.java)
            intent.putExtra(EVENT_ID, eventId)
            context.startActivity(intent)
        }
    }




    fun observeViewModel() {

        with(viewModel) {

            isProcessing.observe(this@MapEventDetailActivity, Observer {
//                if (it) binding.progressLayout.visibility =
//                    View.VISIBLE else binding.progressLayout.visibility = View.GONE
            })

            eventDetailLiveData.observe(this@MapEventDetailActivity, Observer {
                val eventDetail = ObjectConverter.getEventDetailResponse(it.Results)

            })


            mSnackbarHandler.observe(this@MapEventDetailActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@MapEventDetailActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@MapEventDetailActivity)
                }
            })
        }
    }




    private fun dropMarker(mUsersLocation: LatLng, location: String) {
        val markerOptions = MarkerOptions()
        markerOptions.position(mUsersLocation)
        markerOptions.title(location)
        mMap!!.addMarker(markerOptions)
    }

    private fun moveCamera(location: LatLng) {
        if (mMap == null) return
        val cameraPosition = CameraPosition.Builder()
            .target(location) // Sets the center of the map to Mountain View
            .zoom(zoom) // Sets the zoom
            .bearing(0f) // Sets the orientation of the camera to east
            .tilt(30f) // Sets the tilt of the camera to 30 degrees
            .build() // .tilt(30)                    // Creates a CameraPosition from the builder
        mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun getEventDetail() {
        apiHitCounter = 1

        val address = CommonHelper.getAddressToUse()
        val commonRequest = CommonRequest(pageNumber = 0)

        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.dateTimeNow = DateUtils.getTodayDateTime()
        commonRequest.latitude = address[0]
        commonRequest.longitude = address[1]
        commonRequest.isFromWeb = "0"
        commonRequest.eventId = eventid

        viewModel.loadEventDetail(commonRequest)
    }

    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> getEventDetail()
        }
    }

}
