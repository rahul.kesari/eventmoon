package com.music_event_app.ui.mapeventdetail

import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.EventDetailUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

class MapEventDetailViewModel  constructor(private val eventDetailUseCase: EventDetailUseCase)  :BaseViewModel(){

    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel>()
    val eventDetailLiveData = MutableLiveData<BaseResponse>()
    val isProcessing = MutableLiveData<Boolean>()
    val mSnackbarHandler = MutableLiveData<ErrorModel?>()

    @ExperimentalCoroutinesApi
    fun loadEventDetail(commonRequest: CommonRequest) {
        isProcessing.value = true
        eventDetailUseCase.loadEventDetail(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventDetailLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

}