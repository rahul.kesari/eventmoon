package com.music_event_app.ui.myevents

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.MyEventUseCase
import com.music_event_app.domain.usecase.RatingListUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

class MyEventListViewModel constructor(private val myEventUseCase: MyEventUseCase) :
    BaseViewModel() {

    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel?>()

    val isProcessing = MutableLiveData<Boolean>()
    val mSnackbarHandler = MutableLiveData<ErrorModel?>()

    val TAG = MyEventListViewModel::class.java.name
    val eventLiveData = MutableLiveData<BaseResponse>()


    @ExperimentalCoroutinesApi
    fun loadMyEventLists(commonRequest: CommonRequest) {
        isProcessing.value = true
        myEventUseCase.loadMyEventsLists(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    Log.i(TAG, "result: $result")

                    isProcessing.value = false
                    eventLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

}