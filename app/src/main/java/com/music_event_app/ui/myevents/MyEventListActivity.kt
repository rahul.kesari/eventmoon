package com.music_event_app.ui.myevents

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.OvershootInterpolator
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.dhaval2404.imagepicker.ImagePicker
import com.music_event_app.R
import com.music_event_app.databinding.ActivityMyEventListBinding
import com.music_event_app.databinding.ActivityRatingListBinding
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.model.reviewsingle.RatingByVotesModel
import com.music_event_app.domain.model.reviewsingle.ReviewDescriptionWithModel
import com.music_event_app.domain.model.reviewsingle.SingleReview
import com.music_event_app.ui.adapters.AverageRatingAdapter
import com.music_event_app.ui.adapters.EventListAdapter
import com.music_event_app.ui.adapters.RatingListAdapter
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.ui.createnewevent.CreateNewEventActivity
import com.music_event_app.ui.dialogs.RateFeedbackDialog
import com.music_event_app.ui.eventdetail.EventDetailActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.*
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import org.koin.android.viewmodel.ext.android.viewModel

class MyEventListActivity : BaseActivity<MyEventListViewModel, ActivityMyEventListBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_my_event_list
    override val viewModel: MyEventListViewModel by viewModel()

    private val TAG: String = MyEventListActivity.javaClass.name

    private lateinit var ratingListAdapter: RatingListAdapter
    private var ratingList = ArrayList<ReviewDescriptionWithModel?>()

    private var totalPage = 0
    private var currentPage = 1
    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    private var eventList = ArrayList<EventItem?>()
    var apiHitCounter: Int = 1
    private lateinit var eventListAdapter: EventListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupInsets(binding.topLayout)
        initViews()
        initEventRecycler()
        observeViewModel()
        loadMyEventLists()
    }

    private fun initViews() {
        binding.titleTV.text = getString(R.string.my_events)
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
        binding.createNewEventBtn.setOnClickListener {
            CreateNewEventActivity.start(this@MyEventListActivity)
        }
    }


    private fun initEventRecycler() {

        binding.eventsRV.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))

        val linearLayoutManager = LinearLayoutManager(this@MyEventListActivity, LinearLayoutManager.VERTICAL, false)
        binding.eventsRV.layoutManager = linearLayoutManager

        scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                if (currentPage < totalPage) {
                    currentPage += 1
                    binding.eventsRV.post(Runnable {
                        ratingList.add(null)
                        ratingListAdapter?.notifyItemInserted(ratingListAdapter?.itemCount!!)
                        loadMyEventLists()
                    })
                }
            }
        }


        eventListAdapter = EventListAdapter(eventList, object : EventListAdapter.EventListItemListener {

                override fun onItemClicked(item: Any, position: Int) {
                    EventDetailActivity.start(this@MyEventListActivity, (item as EventItem?)?.eventId!!)
                }

                override fun onReminderClicked(item: Any, position: Int) {

//                    mSelectedPosition = position
//                    hitReminder()
                }
            })

        binding.eventsRV.adapter = ScaleInAnimationAdapter(eventListAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }
    }

    fun observeViewModel() {
        with(viewModel) {

            isProcessing.observe(this@MyEventListActivity, Observer {
                if (it) binding.progressLayout.visibility =
                    View.VISIBLE else binding.progressLayout.visibility = View.GONE
            })


            eventLiveData.observe(this@MyEventListActivity, Observer {
                if (it.Results is String) {
                    MessagesUtils.showToastInfo(this@MyEventListActivity, it.Results)

                } else {

                    if (currentPage > 1) {
                        eventList.removeAt(eventList.size - 1)
                        eventListAdapter?.notifyItemRemoved(eventList.size)
                    } else {
                        eventList.clear()
                        eventListAdapter?.notifyDataSetChanged()
                    }

                    eventList.addAll(ObjectConverter.getEventResponseList(it.Results))
                    totalPage = it.totalPageCount!!

                    eventListAdapter?.notifyDataSetChanged()
                    scrollListener?.setLoading()
                }
            })

            mSnackbarHandler.observe(this@MyEventListActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@MyEventListActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@MyEventListActivity)
                }
            })
        }
    }



    companion object {

        fun start(context: Context) {
            val intent = Intent(context, MyEventListActivity::class.java)
            context.startActivity(intent)
        }
    }


    private fun loadMyEventLists() {

        apiHitCounter = 1
        val commonRequest = CommonRequest(pageNumber = currentPage)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        viewModel.loadMyEventLists(commonRequest)
    }



    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> loadMyEventLists()
        }
    }


}
