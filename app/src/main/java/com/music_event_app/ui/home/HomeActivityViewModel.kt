package com.music_event_app.ui.home

import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.HomeActivityUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import com.music_event_app.ui.home.fragments.eventsearchfragment.EventSearchFragmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

class HomeActivityViewModel constructor(private val homeActivityUseCase: HomeActivityUseCase)  :BaseViewModel(){

    val TAG = EventSearchFragmentViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel>()
    val deviceTokenUpdateResponseData = MutableLiveData<BaseResponse>()
    val isProcessing = MutableLiveData<Boolean>()


    @ExperimentalCoroutinesApi
    fun updateDeviceToken(commonRequest: CommonRequest) {
        isProcessing.value = true
        homeActivityUseCase.updateDeviceToken(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    deviceTokenUpdateResponseData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }
}