package com.music_event_app.ui.home.fragments.eventsearchfragment

import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.EventSearchUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi


/**
 * An interactor that calls the actual implementation of [AlbumViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class EventSearchFragmentViewModel constructor(private val eventSearchUseCase: EventSearchUseCase) : BaseViewModel() {

    val TAG = EventSearchFragmentViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel>()
    val eventSearchData = MutableLiveData<BaseResponse>()
    val eventSearchSuggestionData = MutableLiveData<BaseResponse>()
    val isProcessing = MutableLiveData<Boolean>()

    val eventReminderLiveData = MutableLiveData<BaseResponse>()


    @ExperimentalCoroutinesApi
    fun loadEventSearch(commonRequest: CommonRequest) {
        isProcessing.value = true
        eventSearchUseCase.loadEventSearch(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventSearchData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun loadEventSearchSuggestion(commonRequest: CommonRequest) {
        isProcessing.value = true
        eventSearchUseCase.loadEventSearchSuggestion(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    isProcessing.value = false
                    eventSearchSuggestionData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun hitReminder(commonRequest: CommonRequest) {
        isProcessing.value = true
        eventSearchUseCase.hitReminder(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventReminderLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }
}