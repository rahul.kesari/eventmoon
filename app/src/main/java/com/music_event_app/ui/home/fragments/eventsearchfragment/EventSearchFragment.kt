package com.music_event_app.ui.home.fragments.eventsearchfragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.view.animation.OvershootInterpolator
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.FragmentEventSearchBinding
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.eventsearch.Day
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.ui.adapters.EventSearchListAdapter
import com.music_event_app.ui.adapters.EventSuggestionAdapter
import com.music_event_app.ui.adapters.StoryListAdapter
import com.music_event_app.ui.base.BaseFragment
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.ui.eventdetail.EventDetailActivity
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.ui.home.fragments.homefragment.HomeFragment
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.hideKeyboard
import com.music_event_app.util.helper.*
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel

class EventSearchFragment :
    BaseFragment<EventSearchFragmentViewModel, FragmentEventSearchBinding>() {

    override val viewModel: EventSearchFragmentViewModel by viewModel()
    override var title: String = "EventSearchFragment"
    override var menuId: Int = 0
    override val toolBarId: Int = 0
    override val layoutId: Int = R.layout.fragment_event_search

    lateinit var eventSearchListAdapter: EventSearchListAdapter
    private var eventSearchList = ArrayList<Day?>()
    private var apiHitCounter: Int = 1
    private var totalPage = 0
    private var currentPage = 1

    private var eventSuggestionList = ArrayList<String?>()
    lateinit var eventSuggestionAdapter: EventSuggestionAdapter
    private var searchedText: String? = null
    private var mSelectedParentPosition = 0
    private var mSelectedChildPosition = 0

    @ExperimentalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initEventSearchRecycler()
        initEventSuggestionRecycler()
        observeViewModel()
        loadEventSearchList()
    }

    private fun initEventSearchRecycler() {
        binding.eventRecyclerSearch.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        binding.eventRecyclerSearch.layoutManager = linearLayoutManager

        binding.eventRecyclerSearch.isNestedScrollingEnabled = false
        binding.scrollView.viewTreeObserver
            .addOnScrollChangedListener(ViewTreeObserver.OnScrollChangedListener {
                val view =
                    binding.scrollView.getChildAt(binding.scrollView.childCount - 1) as View
                val diff: Int =
                    view.bottom - (binding.scrollView.height + binding.scrollView.scrollY)

                if (diff == 0) {
                    // your pagination code
                    if (currentPage < totalPage) {

                        binding.eventRecyclerSearch.post(Runnable {
                            if (eventSearchList.get(eventSearchList.size - 1) != null) {
                                currentPage += 1
                                eventSearchList.add(null)
                                eventSearchListAdapter?.notifyItemInserted(
                                    eventSearchListAdapter?.itemCount!!
                                )
                                loadEventSearchList()
                            }
                        })
                    }
                }
            })

        eventSearchListAdapter =
            EventSearchListAdapter(
                eventSearchList,
                object : EventSearchListAdapter.EventListItemListener {
                    override fun onItemClicked(item: Any, parentPosition: Int, childPosition: Int) {
                        EventDetailActivity.start(activity!!, (item as EventItem).eventId!!)
                    }

                    override fun onReminderClicked(
                        item: Any,
                        parentPosition: Int,
                        childPosition: Int
                    ) {
                        mSelectedParentPosition = parentPosition
                        mSelectedChildPosition = childPosition
                        hitReminder()
                    }
                })

        binding.eventRecyclerSearch.adapter =
            ScaleInAnimationAdapter(eventSearchListAdapter).apply {
                setFirstOnly(true)
                setDuration(500)
                setInterpolator(OvershootInterpolator(.5f))
            }
    }


    private fun initEventSuggestionRecycler() {
        binding.eventSuggestionRv.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        binding.eventSuggestionRv.layoutManager = linearLayoutManager

        eventSuggestionAdapter =
            EventSuggestionAdapter(eventSuggestionList, object : RecyclerItemListener {
                override fun onItemSelected(item: Any, position: Int) {

                    hideKeyboard()
                    // set event name into search box at home.
                    (activity as HomeActivity).setEventName(item as String)
                    showHideSuggestionCard(false)
                    loadEventSearchList()
                }
            })

        binding.eventSuggestionRv.adapter = ScaleInAnimationAdapter(eventSuggestionAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }
    }

    fun observeViewModel() {

        with(viewModel) {
            eventSearchData.observe(this@EventSearchFragment, Observer {

                if (it.Results is String) {
                    MessagesUtils.showToastInfo(context, "No Data Found")
                    eventSearchList.clear()
                    eventSearchListAdapter?.notifyDataSetChanged()
                } else {

                    if (currentPage > 1) {
                        eventSearchList.removeAt(eventSearchList.size - 1)
                        eventSearchListAdapter?.notifyItemRemoved(eventSearchList.size)
                    } else {
                        eventSearchList.clear()
                        eventSearchListAdapter?.notifyDataSetChanged()
                    }

                    val eventSearchResponse = ObjectConverter.getEventSearchResponse(it.Results)
                    eventSearchList.addAll(eventSearchResponse!!.Day)

                    totalPage = it.totalPageCount!!

                    eventSearchListAdapter?.notifyDataSetChanged()
                }
            })


            eventSearchSuggestionData.observe(this@EventSearchFragment, Observer {
                if (it.Results is String) {
                    // no data found clear list
                    MessagesUtils.showToastInfo(context, it.Results)

                } else {
                    val eventSearchResponse = ObjectConverter.getSuggestionListResponse(it.Results)
                    eventSuggestionList.addAll(eventSearchResponse)
                    eventSuggestionAdapter?.notifyDataSetChanged()
                }
            })

            eventReminderLiveData.observe(this@EventSearchFragment, Observer {

                eventSearchList[mSelectedParentPosition]!!.data[mSelectedChildPosition]?.isInterested = "1"
                eventSearchListAdapter.notifyDataSetChanged()

            })

            isProcessing.observe(this@EventSearchFragment, Observer {
                if (currentPage > 1 && apiHitCounter == 2)
                    return@Observer

                if (it) {
                    binding.progressLayout.visibility = View.VISIBLE

                } else binding.progressLayout.visibility = View.GONE
            })

            mSnackbarText.observe(this@EventSearchFragment, Observer {
                if (it.code == 404 && apiHitCounter == 2) {
                    SnackBarHelper.showErrorFromTop(it.message, activity!!, binding.container)
                    eventSuggestionList.clear()
                    eventSuggestionAdapter?.notifyDataSetChanged()
                } else if (it.code == 404 && apiHitCounter == 1) {
                    SnackBarHelper.showErrorFromTop(it.message, activity!!, binding.container)
                    eventSearchList.clear()
                    eventSearchListAdapter?.notifyDataSetChanged()
                } else if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, activity!!, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, activity!!)
                }
            })
        }
    }

    private fun loadEventSearchList() {

        apiHitCounter = 1
        val commonRequest = CommonRequest(pageNumber = "" + currentPage)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID, 0)
        commonRequest.accessToken =
            Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN, "")

        val address = CommonHelper.getAddressToUse()
        commonRequest.latitude = address[0]
        commonRequest.longitude = address[1]

        commonRequest.dateTimeNow = DateUtils.getTodayDateTime()
        commonRequest.eventName = (activity as HomeActivity).binding.searchEventET.text.toString()
        viewModel.loadEventSearch(commonRequest)
    }


    private fun loadEventSearchSuggestion() {
        apiHitCounter = 2
        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID, 0)
        commonRequest.accessToken =
            Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN, "")

        val address = CommonHelper.getAddressToUse()
        commonRequest.latitude = address[0]
        commonRequest.longitude = address[1]

        commonRequest.SearchText = searchedText
        commonRequest.dateTimeNow = DateUtils.getTodayDateTime()

        viewModel.loadEventSearchSuggestion(commonRequest)
    }


    private fun hitReminder() {
        apiHitCounter = 3

        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.startDate = DateUtils.getTodayDateTime()
//        commonRequest.image =
        commonRequest.deviceType = "android"
        commonRequest.appName = "eventmoon"
        commonRequest.eventId = eventSearchList[mSelectedParentPosition]!!.data[mSelectedChildPosition]?.eventId!!

        viewModel.hitReminder(commonRequest)
    }

    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> loadEventSearchList()
            2 -> loadEventSearchSuggestion()
            3 -> hitReminder()
        }
    }

    companion object {

        val FRAGMENT_NAME = EventSearchFragment::class.java.name
        fun newInstance() =
            EventSearchFragment().apply {
                val bundle = Bundle()
                arguments = bundle
            }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(searchedText: String?) {
        this.searchedText = searchedText
        showHideSuggestionCard(true)
        loadEventSearchSuggestion()
    }

    private fun showHideSuggestionCard(show: Boolean) {
        if (show) {
            binding.eventRecyclerSearch.visibility = View.GONE
            binding.searchCardView.visibility = View.VISIBLE
        } else {
            eventSuggestionList.clear()
            eventSuggestionAdapter.notifyDataSetChanged()
            binding.searchCardView.visibility = View.GONE
            binding.eventRecyclerSearch.visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
        super.onDestroy()

    }

    override val progressBar: ProgressBar?
        get() = TODO("Not yet implemented")
}