package com.music_event_app.ui.home.fragments.drawer

import android.app.Activity
import android.view.View
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.drawerlayout.widget.DrawerLayout.DrawerListener
import com.music_event_app.R


class EndDrawerToggle(activity: Activity, private val drawerLayout: DrawerLayout, toolbar: Toolbar, openDrawerContentDescRes: Int, closeDrawerContentDescRes: Int
) :
    DrawerListener {
//    private val arrowDrawable: DrawerArrowDrawable
    private val toggleButton: AppCompatImageButton
    private val openDrawerContentDesc: String
    private val closeDrawerContentDesc: String
    fun syncState() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            setPosition(1f)
        } else {
            setPosition(0f)
        }
    }

    fun toggle() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    fun setPosition(position: Float) {
        if (position == 1f) {
//            arrowDrawable.setVerticalMirror(true)
            toggleButton.contentDescription = closeDrawerContentDesc
        } else if (position == 0f) {
//            arrowDrawable.setVerticalMirror(false)
            toggleButton.contentDescription = openDrawerContentDesc
        }
//        arrowDrawable.progress = position
    }

    override fun onDrawerStateChanged(newState: Int) {}
    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
        setPosition(Math.min(1f, Math.max(0f, slideOffset)))
    }

    override fun onDrawerClosed(drawerView: View) {
        setPosition(0f)
    }

    override fun onDrawerOpened(drawerView: View) {
        setPosition(1f)
    }

    init {
        openDrawerContentDesc = activity.getString(openDrawerContentDescRes)
        closeDrawerContentDesc = activity.getString(closeDrawerContentDescRes)
//        arrowDrawable = DrawerArrowDrawable(toolbar.getContext())
//        arrowDrawable.direction = DrawerArrowDrawable.ARROW_DIRECTION_END
        toggleButton = AppCompatImageButton(
            toolbar.getContext(), null,
            R.attr.toolbarNavigationButtonStyle
        )
//        toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(toolbar.context,R.drawable.ic_drawer))
        toolbar.addView(toggleButton, Toolbar.LayoutParams(GravityCompat.END))
        toggleButton.setImageDrawable(ContextCompat.getDrawable(toolbar.context,R.drawable.ic_drawer))
        toggleButton.setOnClickListener { toggle()
        }
    }
}