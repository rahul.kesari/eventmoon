package com.music_event_app.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import com.google.android.material.appbar.AppBarLayout.ScrollingViewBehavior
import com.music_event_app.R
import com.music_event_app.databinding.ActivityHomeBinding
import com.music_event_app.domain.model.eventbusmodel.EventBusModel
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.ui.base.LocationBaseActivity
import com.music_event_app.ui.editprofile.EditProfileActivity
import com.music_event_app.ui.home.fragments.drawer.DrawerFragment
import com.music_event_app.ui.home.fragments.drawer.EndDrawerToggle
import com.music_event_app.ui.home.fragments.eventsearchfragment.EventSearchFragment
import com.music_event_app.ui.home.fragments.homefragment.HomeFragment
import com.music_event_app.ui.home.fragments.locationfragment.LocationNearbyFragment
import com.music_event_app.ui.login.LoginActivity
import com.music_event_app.ui.myevents.MyEventListActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.TransitionAnimation
import com.music_event_app.util.helper.CommonHelper
import com.music_event_app.util.helper.DebouncingQueryTextListener
import com.music_event_app.util.helper.Prefs
import com.music_event_app.util.helper.SnackBarHelper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.greenrobot.eventbus.EventBus
import org.koin.android.viewmodel.ext.android.viewModel


class HomeActivity : LocationBaseActivity<HomeActivityViewModel, ActivityHomeBinding>(),
    DrawerFragment.FragmentDrawerListener {

    override var frameContainerId: Int = R.id.container
    override val layoutId: Int = R.layout.activity_home
    override val viewModel: HomeActivityViewModel by viewModel()
    private var drawerFragment: DrawerFragment? = null
    private var mDrawerToggle: EndDrawerToggle? = null

    private val TAG = HomeActivity.javaClass.name
    private var isEventNameClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupInsets(binding.drawerLayout)
        initViews()
        initSearchViews()
        setUpDrawer()
        handleChangeColor(1)
        observeViewModel()

    }


    override fun onResume() {
        super.onResume()
        handleDeviceTokenToUpdate()
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, HomeActivity::class.java)
            context.startActivity(intent)
        }
    }


    private fun setUpDrawer() {
        setSupportActionBar(binding.toolbar)
//        binding.toolbar.title=""
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        drawerFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_navigation_drawer) as DrawerFragment?
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        binding.drawerIV.setOnClickListener {
           handleDrawer()
        }
        drawerFragment?.setDrawerListener(this)
    }

    open fun handleDrawer(){
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }


    private fun initViews() {
        binding.homeLl.setOnClickListener {
            handleChangeColor(1)
        }

        binding.locationLl.setOnClickListener {
            handleChangeColor(2)
        }

        binding.eventSearchLl.setOnClickListener {
            handleChangeColor(3)
        }
        binding.rewardLl.setOnClickListener {
            handleChangeColor(4)
        }
    }

    override fun onDrawerItemSelected(position: String) {
        if (binding.drawerLayout.isDrawerVisible(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }

        when (position) {
            AppConstants.DRAWER_CONSTANTS.PROFILE -> {
                EditProfileActivity.start(this@HomeActivity)
            }
            AppConstants.DRAWER_CONSTANTS.LOGIN -> {
                LoginActivity.start(this@HomeActivity)
                finish()
            }
            AppConstants.DRAWER_CONSTANTS.LOGOUT -> {

                CommonHelper.showConfirmationDialog(
                    this,
                    getString(R.string.logout_msg_string),
                    object : CommonHelper.DialogCLick {
                        override fun onNoClicked() {

                        }

                        override fun onYesClicked() {
                            try {

                                Prefs.clear().commit()
                                LoginActivity.start(this@HomeActivity)
                                finish()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    })

            }
            AppConstants.DRAWER_CONSTANTS.MY_EVENTS -> {
                MyEventListActivity.start(this@HomeActivity)
            }
        }
    }

    private fun handleChangeColor(position: Int) {

        binding.homeTv.setTextColor(ContextCompat.getColor(this, R.color.gray_color))
        binding.locationTv.setTextColor(ContextCompat.getColor(this, R.color.gray_color))
        binding.eventSearchTv.setTextColor(ContextCompat.getColor(this, R.color.gray_color))
        binding.rewardTv.setTextColor(ContextCompat.getColor(this, R.color.gray_color))


        binding.homeIv.setImageResource(R.drawable.tab_home)
        binding.locationIv.setImageResource(R.drawable.tab_location)
        binding.eventSearchIv.setImageResource(R.drawable.tab_profile)
        binding.rewardIv.setImageResource(R.drawable.tab_rewards)

        when (position) {
            1 -> {
                showAppBarLayout()

                binding.homeTv.setTextColor(ContextCompat.getColor(this, R.color.primaryColor))
                binding.homeIv.setImageResource(R.drawable.tab_home_selected)

                navigateTo(
                    HomeFragment.newInstance(),
                    AppConstants.FragmentConstants.HOME_FRAGMENT,
                    TransitionAnimation.NO_ANIMATION,
                    false
                )

                binding.pageTitleTv.visibility = View.VISIBLE
                binding.pageSubTitleTv.text = "New Heaven"
                binding.searchEventET.visibility = View.GONE

            }
            2 -> {

                hideAppBarLayout()
                binding.locationTv.setTextColor(ContextCompat.getColor(this, R.color.primaryColor))
                binding.locationIv.setImageResource(R.drawable.tab_location_selected)

                navigateTo(
                    LocationNearbyFragment.newInstance(),
                    AppConstants.FragmentConstants.LOCATION_FRAGMENT,
                    TransitionAnimation.NO_ANIMATION,
                    false
                )
            }
            3 -> {

                showAppBarLayout()
                binding.eventSearchTv.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.primaryColor
                    )
                )
                binding.eventSearchIv.setImageResource(R.drawable.tab_profile_selected)

                navigateTo(
                    EventSearchFragment.newInstance(),
                    AppConstants.FragmentConstants.EVENT_SEARCH_FRAGMENT,
                    TransitionAnimation.NO_ANIMATION,
                    false
                )

                binding.pageTitleTv.visibility = View.GONE
                binding.pageSubTitleTv.text = getString(R.string.find_the_events_around_you_string)
                binding.searchEventET.visibility = View.VISIBLE
            }
            4 -> {
                SnackBarHelper.showInfoFromTop(
                    msg = getString(R.string.under_development_msg),
                    context = this@HomeActivity,
                    view = binding.coordinatorLayout
                )
//                binding.rewardTv.setTextColor(ContextCompat.getColor(this,R.color.primaryColor))
//                binding.rewardIv.setImageResource(R.drawable.tab_rewards_selected)
//
//                navigateTo(RewardsFragment.newInstance(),AppConstants.FragmentConstants.REWARD_FRAGMENT,TransitionAnimation.NO_ANIMATION,false)
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun initSearchViews() {

        binding.searchEventET.addTextChangedListener(DebouncingQueryTextListener(
            this@HomeActivity
        ) { newText ->
            newText?.let {

                if (isEventNameClicked) {
                    isEventNameClicked = false
                    return@let
                }
                EventBus.getDefault().post(it)
            }
        })
    }

    open fun setEventName(eventName: String) {
        isEventNameClicked = true
        binding.searchEventET.setText(eventName)
        binding.searchEventET.setSelection(binding.searchEventET.text.toString().length)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.fragments.forEachIndexed { index, fragment ->
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun gotCurrentLocation() {
        EventBus.getDefault()
            .post(EventBusModel(AppConstants.EVENTBUS_CONSTANTS.FOUND_LOCATION, null))
    }

    override fun onPermissionDenied() {
        EventBus.getDefault()
            .post(EventBusModel(AppConstants.EVENTBUS_CONSTANTS.LOCATION_DENIED, null))
    }

    private fun handleDeviceTokenToUpdate() {
        if (Prefs.getBoolean(
                AppConstants.PreferenceConstants.IS_TOKEN_REFRESHED,
                false
            ) && Prefs.getInt(AppConstants.PreferenceConstants.USER_ID, 0) != 0
        ) {

            val request = CommonRequest(pageNumber = 0)
            request.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
            request.deviceToken = Prefs.getString(AppConstants.PreferenceConstants.FCM_TOKEN)
            request.deviceType = "android"
            request.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
            viewModel.updateDeviceToken(request)
        }
    }

    private fun observeViewModel() {
        with(viewModel) {
            deviceTokenUpdateResponseData.observe(this@HomeActivity, Observer {
                Prefs.putBoolean(AppConstants.PreferenceConstants.IS_TOKEN_REFRESHED, false)
            })
        }
    }

    private fun hideAppBarLayout() {
        val params = binding.container.layoutParams as CoordinatorLayout.LayoutParams
        params.behavior = null
//        binding.container.requestLayout()
    }

    private fun showAppBarLayout() {
        val params = binding.container.layoutParams as CoordinatorLayout.LayoutParams
        params.behavior = ScrollingViewBehavior()
//        binding.container.requestLayout()
    }
}



