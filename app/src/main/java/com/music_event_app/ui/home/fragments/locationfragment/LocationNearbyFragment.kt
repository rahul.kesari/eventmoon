package com.music_event_app.ui.home.fragments.locationfragment

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SnapHelper
import com.ethanhua.skeleton.RecyclerViewSkeletonScreen
import com.ethanhua.skeleton.Skeleton
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.music_event_app.R
import com.music_event_app.databinding.FragmentLocationBinding
import com.music_event_app.domain.model.nearbyevents.Event
import com.music_event_app.domain.model.nearbyevents.TimeCategory
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.ui.adapters.LocationCategoryListAdapter
import com.music_event_app.ui.base.BaseFragment
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.ui.dialogs.sheets.LocationFilterSheet
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.hideKeyboard
import com.music_event_app.util.helper.DateUtils
import com.music_event_app.util.helper.ObjectConverter
import com.music_event_app.util.helper.Prefs
import com.music_event_app.util.helper.SnackBarHelper
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel


class LocationNearbyFragment :
    BaseFragment<LocationNearbyFragmentViewModel, FragmentLocationBinding>(),
    OnMapReadyCallback {

    override val viewModel: LocationNearbyFragmentViewModel by viewModel()
    override var title: String = ""
    override var menuId: Int = 0
    override val toolBarId: Int = 0
    override val layoutId: Int = R.layout.fragment_location
    private var mMap: GoogleMap? = null
    private val zoom = 15f
    private lateinit var categoryListAdapter: LocationCategoryListAdapter
    private var timecategoryList = ArrayList<TimeCategory?>()

    private var eventsList = ArrayList<Event?>()

    private var apiHitCounter: Int = 1
    private var selectedRadius = 20

    private lateinit var mSelectedLatitude: String
    private lateinit var mSelectedLongitude: String
    private lateinit var categorySkeleton: RecyclerViewSkeletonScreen

    private fun initCategoryRecycler() {

        timecategoryList.add(TimeCategory("Music", 1, "10:30 AM"))
        timecategoryList.add(TimeCategory("Rock", 1, "10:30 AM"))
        timecategoryList.add(TimeCategory("Conference", 1, "10:30 AM"))
        timecategoryList.add(TimeCategory("Jazz", 1, "10:30 AM"))
        timecategoryList.add(TimeCategory("Sports", 1, "10:30 AM"))
        timecategoryList.add(TimeCategory("Games", 1, "10:30 AM"))
        timecategoryList.add(TimeCategory("Tech", 1, "10:30 AM"))

        binding.categoryRV.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))
        binding.categoryRV.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        categoryListAdapter =
            LocationCategoryListAdapter(timecategoryList, object : RecyclerItemListener {
                override fun onItemSelected(item: Any, position: Int) {
                    categoryListAdapter.notifyDataSetChanged()
//                loadInitialEvents()
                }
            })
        val snapHelper: SnapHelper = GravitySnapHelper(Gravity.CENTER)
        snapHelper.attachToRecyclerView(binding.categoryRV)

        binding.categoryRV.adapter = ScaleInAnimationAdapter(categoryListAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }
    }

    @ExperimentalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        addGoogleMap()
        initViews()
        initCategoryRecycler()
    }


    private fun initViews() {
        binding.drawerIV.setOnClickListener {
            (activity as HomeActivity).handleDrawer()
        }
        binding.filterBtn.setOnClickListener {

            val bottomSlideDialog = LocationFilterSheet()
            bottomSlideDialog.setUpDialog(activity!! as HomeActivity, defaultradius = 20)
            bottomSlideDialog.setOnFilterListener(object :
                LocationFilterSheet.LocationFilterListener {
                override fun onClicked(selectedRadius: Int?) {

                }
            })
        }
    }

    /*Add Map in our fragment */
    private fun addGoogleMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this@LocationNearbyFragment)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap?.uiSettings?.isMapToolbarEnabled = false
        mMap?.uiSettings?.isMyLocationButtonEnabled = false
        mMap?.uiSettings?.isZoomControlsEnabled = false
        mMap?.uiSettings?.isIndoorLevelPickerEnabled = false
        mMap?.animateCamera(CameraUpdateFactory.zoomIn())
        mMap?.animateCamera(CameraUpdateFactory.zoomTo(zoom), 5000, null)
    }


    fun observeViewModel() {
        with(viewModel) {
            eventResponseLiveData.observe(this@LocationNearbyFragment, Observer {
                if (it.Results is String) {
                    SnackBarHelper.showErrorFromTop(
                        it.Results,
                        activity!!,
                        binding.container
                    )
                } else {

                    val data = ObjectConverter.getEventNearbyResponse(it.Results)
                    timecategoryList.clear()
                    timecategoryList.addAll(data?.time_categories!!)
                    categoryListAdapter.notifyDataSetChanged()


                }
            })

            isProcessing.observe(this@LocationNearbyFragment, Observer {
                if (it) {
                    showTimeCategorySkeleton()
                    hideKeyboard()
                } else {
                    categorySkeleton?.hide()
                }
            })

            mSnackbarText.observe(this@LocationNearbyFragment, Observer {
                if (it.code == 404) {
                    SnackBarHelper.showErrorFromTop(it.message, activity!!, binding.container)
                    eventsList.clear()
                } else if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, activity!!, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, activity!!)
                }
            })
        }
    }

    private fun showTimeCategorySkeleton() {
        categorySkeleton = Skeleton.bind(binding.categoryRV)
            .adapter(categoryListAdapter)
            .shimmer(true)
            .count(10)
            .color(R.color.color_a2878787)
            .angle(20)
            .duration(1000)
            .load(R.layout.item_skeleton_category)
            .show()
    }

    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> loadNearbyEvents()
        }
    }

    private fun loadNearbyEvents() {

        apiHitCounter = 1
        val commonRequest = CommonRequest(pageNumber = 0)
//        commonRequest.categoryId = categoryListAdapter.selectedCategoryId
//        commonRequest.latitude = mLatitude
//        commonRequest.longitude = mLongitude

        commonRequest.latitude = "41.303447"
        commonRequest.longitude = "-72.926259"
        commonRequest.radius = selectedRadius
        commonRequest.time = "15:30"
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.dateTimeNow = DateUtils.getTodayDateTime()
        viewModel.loadNearByEvents(commonRequest)
    }


    companion object {

        val FRAGMENT_NAME = LocationNearbyFragment::class.java.name
        fun newInstance() =
            LocationNearbyFragment().apply {
                val bundle = Bundle()
                arguments = bundle
            }
    }

    /**
     * @param view is custom marker layout which we will convert into bitmap.
     * @param bitmap is the image which you want to show in marker.
     * @return
     */
//    private fun getMarkerBitmapFromView(view: View, bitmap: Bitmap): Bitmap? {
//        mCustomMarkerView = ((context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE)) as LayoutInflater).inflate(R.layout.event_marker_layout, null)
//        mMarkerImageView.setImageBitmap(bitmap)
//        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
//        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
//        view.buildDrawingCache()
//        val returnedBitmap = Bitmap.createBitmap(view.measuredWidth, view.measuredHeight, Bitmap.Config.ARGB_8888)
//        val canvas = Canvas(returnedBitmap)
//        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
//        val drawable: Drawable = view.background
//        drawable?.draw(canvas)
//        view.draw(canvas)
//        return returnedBitmap
//    }

    open fun createMarker(context: Context, point: LatLng?, event: Event): MarkerOptions? {
        val marker = MarkerOptions()
        marker.position(point!!)
        val px: Int = context.resources.getDimensionPixelSize(R.dimen.map_marker_diameter)
        val markerView: View =
            (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                R.layout.event_marker_layout,
                null
            )
        markerView.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        markerView.layout(0, 0, px, px)
        markerView.buildDrawingCache()
        val bedNumberTextView = markerView.findViewById<View>(R.id.nameTV) as AppCompatTextView
        val mDotMarkerBitmap = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(mDotMarkerBitmap)
        bedNumberTextView.text = event.address
        markerView.draw(canvas)
        marker.icon(BitmapDescriptorFactory.fromBitmap(mDotMarkerBitmap))
        return marker
    }

    private fun addCustomMarkerFromURL() {
//        if (mMap == null) {
//            return
//        }
//        // adding a marker with image from URL using glide image loading library
//        Glide.with(getApplicationContext())
//            .load("").asBitmap().fitCenter()
//            .into(object : SimpleTarget<Bitmap?>() {
//                fun onResourceReady(
//                    bitmap: Bitmap,
//                    glideAnimation: GlideAnimation<in Bitmap?>?
//                ) {
//                    mGoogleMap.addMarker(
//                        MarkerOptions().position(mDummyLatLng)
//                            .icon(
//                                BitmapDescriptorFactory.fromBitmap(
//                                    getMarkerBitmapFromView(
//                                        mCustomMarkerView,
//                                        bitmap
//                                    )
//                                )
//                            )
//                    )
//                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mDummyLatLng, 13f))
//                }
//            })
    }


    override val progressBar: ProgressBar?
        get() = TODO("Not yet implemented")

}