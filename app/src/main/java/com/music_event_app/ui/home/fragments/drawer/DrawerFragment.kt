package com.music_event_app.ui.home.fragments.drawer

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.music_event_app.R
import com.music_event_app.databinding.FragmentDrawerBinding
import com.music_event_app.ui.base.BaseFragment
import com.music_event_app.ui.login.LoginActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.CommonHelper
import com.music_event_app.util.helper.GlideUtils
import com.music_event_app.util.helper.Prefs
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel

class DrawerFragment : BaseFragment<DrawerViewModel, FragmentDrawerBinding>() {

    override val viewModel: DrawerViewModel by viewModel()
    override var title: String = "Notes"
    override var menuId: Int = 0
    override val toolBarId: Int = 0
    override val layoutId: Int = R.layout.fragment_drawer
    private var drawerListener: FragmentDrawerListener? = null
    val TAG_ = DrawerFragment.javaClass.name

    @ExperimentalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initViews()
    }

    override fun onResume() {
        super.onResume()
        setDataToViews()
    }

    companion object {

        val FRAGMENT_NAME = DrawerFragment::class.java.name
    }

    override val progressBar: ProgressBar?
        get() = TODO("Not yet implemented")


    interface FragmentDrawerListener {
        fun onDrawerItemSelected(position: String)
    }

    fun setDrawerListener(listener: FragmentDrawerListener) {
        this.drawerListener = listener
    }


    private fun initViews() {

        Log.d(
            TAG_,
            "Drawerfragment >>>> " + Prefs.getString(
                AppConstants.PreferenceConstants.NAME,
                "Event Moon"
            )
        )



        binding.profileLl.setOnClickListener {
            drawerListener?.onDrawerItemSelected(AppConstants.DRAWER_CONSTANTS.PROFILE)
        }
        binding.referralCodeLl.setOnClickListener {
            drawerListener?.onDrawerItemSelected(AppConstants.DRAWER_CONSTANTS.REFERRAL_CODE)
        }
        binding.rewardPointLl.setOnClickListener {
            drawerListener?.onDrawerItemSelected(AppConstants.DRAWER_CONSTANTS.REWARD_POINT)
        }
        binding.homeLl.setOnClickListener {
            drawerListener?.onDrawerItemSelected(AppConstants.DRAWER_CONSTANTS.HOME)
        }
        binding.myEventLl.setOnClickListener {
            drawerListener?.onDrawerItemSelected(AppConstants.DRAWER_CONSTANTS.MY_EVENTS)
        }
        binding.eventSearchLl.setOnClickListener {
            drawerListener?.onDrawerItemSelected(AppConstants.DRAWER_CONSTANTS.EVENT_SEARCH)
        }
        binding.loginLogoutLl.setOnClickListener {
            if (CommonHelper.isUserLoggedIn()) {
                drawerListener?.onDrawerItemSelected(AppConstants.DRAWER_CONSTANTS.LOGOUT)
            } else {
                drawerListener?.onDrawerItemSelected(AppConstants.DRAWER_CONSTANTS.LOGIN)
            }

        }

        if (CommonHelper.isUserLoggedIn()) {
            binding.loginLogoutTv.text = getString(R.string.logout_string)
        } else {
            binding.loginLogoutTv.text = getString(R.string.login_string)
        }

    }

    private fun setDataToViews() {
        try {
            binding.userNameTv.setText(
                Prefs.getString(
                    AppConstants.PreferenceConstants.NAME,
                    "Event Moon"
                )
            )
            GlideUtils.loadImage(
                activity,
                Prefs.getString(AppConstants.PreferenceConstants.USER_IMAGE_PATH),
                binding.profileIv,
                R.drawable.create_image
            )
        } catch (e: Exception) {
        }

    }
}