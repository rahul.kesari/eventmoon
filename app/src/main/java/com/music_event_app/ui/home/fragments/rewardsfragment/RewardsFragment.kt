package com.music_event_app.ui.home.fragments.rewardsfragment

import android.os.Bundle
import android.widget.ProgressBar
import com.music_event_app.R
import com.music_event_app.databinding.FragmentDrawerBinding
import com.music_event_app.databinding.FragmentRewardsBinding
import com.music_event_app.ui.base.BaseFragment
import com.music_event_app.ui.home.fragments.eventsearchfragment.EventSearchFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel

class RewardsFragment : BaseFragment<RewardsFragmentViewModel, FragmentRewardsBinding>() {

    override val viewModel: RewardsFragmentViewModel by viewModel()
    override var title: String = "Notes"
    override var menuId: Int = 0
    override val toolBarId: Int = 0
    override val layoutId: Int = R.layout.fragment_rewards

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        adapter = NotesAdapter()

    }

    @ExperimentalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        binding.viewmodel = viewModel
//        binding.homeRecyclerView.adapter = adapter

//        viewModel.loadAlbums()
    }


    companion object {

        val FRAGMENT_NAME = RewardsFragment::class.java.name
        fun newInstance() =
            RewardsFragment().apply {
                val bundle = Bundle()
                arguments = bundle
            }
    }

    override val progressBar: ProgressBar?
        get() = TODO("Not yet implemented")


}