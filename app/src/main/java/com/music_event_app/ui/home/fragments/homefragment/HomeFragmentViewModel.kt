package com.music_event_app.ui.home.fragments.homefragment

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.HomeFragmentUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi


/**
 * An interactor that calls the actual implementation of [AlbumViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class HomeFragmentViewModel constructor(private val homeUseCase: HomeFragmentUseCase) : BaseViewModel() {

    val TAG = HomeFragmentViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel>()
    val categoryLiveData = MutableLiveData<BaseResponse>()
    val eventLiveData = MutableLiveData<BaseResponse>()
    val storyLiveData = MutableLiveData<BaseResponse>()
    val isProcessing = MutableLiveData<Boolean>()
    val isCategoryProcessing = MutableLiveData<Boolean>()
    val isEventProcessing = MutableLiveData<Boolean>()

    val eventReminderLiveData = MutableLiveData<BaseResponse>()

    @ExperimentalCoroutinesApi
    fun loadCategories(commonRequest: CommonRequest) {
        isCategoryProcessing.value = true
        homeUseCase.loadCategories(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    Log.i(TAG, "result: $result")
                    isCategoryProcessing.value = false
                    categoryLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isCategoryProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun loadEventLists(commonRequest: CommonRequest) {
        isEventProcessing.value = true
        homeUseCase.loadEventLists(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    Log.i(TAG, "result: $result")

                    isEventProcessing.value = false
                    eventLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isEventProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun loadStories(commonRequest: CommonRequest) {
        isProcessing.value = true
        homeUseCase.loadStories(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    storyLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun hitReminder(commonRequest: CommonRequest) {
        isProcessing.value = true
        homeUseCase.hitReminder(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventReminderLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }


}