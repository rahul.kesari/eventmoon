package com.music_event_app.ui.home.fragments.rewardsfragment

import androidx.lifecycle.MutableLiveData
import com.music_event_app.domain.usecase.TermsConditionUseCase
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi


/**
 * An interactor that calls the actual implementation of [AlbumViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class RewardsFragmentViewModel constructor(private val getAlbumsUseCase: TermsConditionUseCase) : BaseViewModel() {

    val TAG = RewardsFragmentViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<String>()
    val isDataLoaded = MutableLiveData<Boolean>()

    @ExperimentalCoroutinesApi
    fun loadAlbums() {
//        getAlbumsUseCase.invoke(scope, null, object : UseCaseResponse<List<Note>> {
//            override fun onSuccess(result: List<Note>) {
//                Log.i(TAG, "result: $result")
//                empty.value = result.isEmpty()
//                isDataLoaded.value = true
//                albumsReceivedLiveData.value = result
//            }
//
//            override fun onError(errorModel: ErrorModel?) {
//                isDataLoaded.postValue(false)
//                mSnackbarText.postValue(errorModel?.message)
//            }
//
//        })
    }


}