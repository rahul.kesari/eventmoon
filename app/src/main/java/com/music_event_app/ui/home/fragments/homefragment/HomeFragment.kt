package com.music_event_app.ui.home.fragments.homefragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver.OnScrollChangedListener
import android.view.animation.OvershootInterpolator
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ethanhua.skeleton.RecyclerViewSkeletonScreen
import com.ethanhua.skeleton.Skeleton
import com.music_event_app.R
import com.music_event_app.databinding.FragmentHomeBinding
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.eventbusmodel.EventBusModel
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.model.story.StoryListItem
import com.music_event_app.ui.adapters.EventListAdapter
import com.music_event_app.ui.adapters.HomeCategoryListAdapter
import com.music_event_app.ui.adapters.StoryListAdapter
import com.music_event_app.ui.base.BaseFragment
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.ui.eventdetail.EventDetailActivity
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.ui.locationsearch.LocationSearchActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.hideKeyboard
import com.music_event_app.util.helper.*
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.coroutines.ExperimentalCoroutinesApi
import lt.neworld.spanner.Spanner
import lt.neworld.spanner.Spans.click
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : BaseFragment<HomeFragmentViewModel, FragmentHomeBinding>() {

    override val viewModel: HomeFragmentViewModel by viewModel()
    override var title: String = ""
    override var menuId: Int = 0
    override val toolBarId: Int = 0
    override val layoutId: Int = R.layout.fragment_home
    private lateinit var categoryListAdapter: HomeCategoryListAdapter
    private var apiHitCounter: Int = 1
    private var categoryList = ArrayList<CategoryItem?>()
    private var eventList = ArrayList<EventItem?>()
    private var storyList = ArrayList<StoryListItem?>()

    private lateinit var eventListAdapter: EventListAdapter
    private var totalPage = 0
    private var currentPage = 1

    private lateinit var storyListAdapter: StoryListAdapter
    private var mLatitude: String? = ""
    private var mLongitude: String? = ""
    private var mSelectedPosition = 0

    private lateinit var eventSkeleton: RecyclerViewSkeletonScreen
    private lateinit var categorySkeleton: RecyclerViewSkeletonScreen

    @ExperimentalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)

        initViews()
        setAddressDefault()

        initInitialAddress()

        initCategoryRecycler()
        initStoryRecycler()
        initEventRecycler()
        observeViewModel()
        loadCategories()
    }

    private fun showEventListSkeleton() {

        eventSkeleton = Skeleton.bind(binding.eventListRv)
            .adapter(eventListAdapter)
            .load(R.layout.item_skeleton_event)
            .shimmer(true)
            .count(10)
            .color(R.color.color_a2878787)
            .angle(20)
            .duration(1000)
            .show()
    }

    private fun showCategoryListSkeleton() {
        categorySkeleton = Skeleton.bind(binding.categoryRv)
            .adapter(categoryListAdapter)
            .shimmer(true)
            .count(10)
            .color(R.color.color_a2878787)
            .angle(20)
            .duration(1000)
            .load(R.layout.item_skeleton_category)
            .show()
    }


    private fun initInitialAddress() {

        val onClickListener =
            View.OnClickListener { _ ->
//                MessagesUtils.showToastInfo(activity, "Clicked")
                (activity as HomeActivity).initLocationWithCheck()
            }
        val spannable =
            Spanner("You are viewing events for Las Vegas. To view events happening near by you Click Here.")
                .span("Click Here", click(onClickListener))
        binding.changeAddressTV.setText(spannable)
        binding.changeAddressTV.setOnClickListener {
//            MessagesUtils.showToastInfo(activity, "Clicked")
            (activity as HomeActivity).initLocationWithCheck()
        }
    }


    private fun initViews() {
        binding.filterLocationIV.setOnClickListener {
            LocationSearchActivity.startForResult(activity!!)
        }
    }

    private fun initStoryRecycler() {
        binding.storyListRv.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))

        binding.storyListRv.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        storyListAdapter = StoryListAdapter(storyList, object : RecyclerItemListener {
            override fun onItemSelected(item: Any, position: Int) {
            }
        })

        binding.storyListRv.adapter = ScaleInAnimationAdapter(storyListAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }
    }

    private fun initCategoryRecycler() {
        binding.categoryRv.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))

        binding.categoryRv.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        categoryListAdapter = HomeCategoryListAdapter(categoryList, object : RecyclerItemListener {
            override fun onItemSelected(item: Any, position: Int) {
                categoryListAdapter.notifyDataSetChanged()
                loadInitialEvents()
            }
        })

        binding.categoryRv.adapter = ScaleInAnimationAdapter(categoryListAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }
    }

    private fun loadInitialEvents() {
        currentPage = 1
        totalPage = 0
        eventList.clear()
        eventListAdapter.notifyDataSetChanged()
        loadEventLists()
    }

    private fun initEventRecycler() {

        binding.eventListRv.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.eventListRv.layoutManager = linearLayoutManager

        binding.eventListRv.isNestedScrollingEnabled = false
        binding.scrollView.viewTreeObserver
            .addOnScrollChangedListener(OnScrollChangedListener {
                val view =
                    binding.scrollView.getChildAt(binding.scrollView.childCount - 1) as View
                val diff: Int =
                    view.bottom - (binding.scrollView.height + binding.scrollView.scrollY)

                if (diff == 0) {
                    // your pagination code
                    if (currentPage < totalPage) {

                        binding.eventListRv.post(Runnable {
                            try {
                                if (eventList.get(eventList.size - 1) != null) {
                                    currentPage += 1
                                    eventList.add(null)
                                    eventListAdapter?.notifyItemInserted(eventListAdapter?.itemCount!!)
                                    loadEventLists()
                                }
                            }catch (e:Exception){
                                e.printStackTrace()
                            }
                        })
                    }
                }
            })
        eventListAdapter =
            EventListAdapter(eventList, object : EventListAdapter.EventListItemListener {

                override fun onItemClicked(item: Any, position: Int) {
                    EventDetailActivity.start(activity!!, (item as EventItem?)?.eventId!!)
                }

                override fun onReminderClicked(item: Any, position: Int) {

                    mSelectedPosition = position
                    hitReminder()
                }
            })

        binding.eventListRv.adapter = ScaleInAnimationAdapter(eventListAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }
    }


    private fun loadCategories() {
        apiHitCounter = 1
        val commonRequest = CommonRequest(pageNumber = 0)
        viewModel.loadCategories(commonRequest)
    }

    private fun loadEventLists() {

        apiHitCounter = 2
        val commonRequest = CommonRequest(pageNumber = currentPage)
        commonRequest.categoryId = categoryListAdapter.selectedCategoryId
        commonRequest.latitude = mLatitude
        commonRequest.longitude = mLongitude
//        commonRequest.latitude = "41.303447"
//        commonRequest.longitude = "-72.926259"
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.dateTimeNow = DateUtils.getTodayDateTime()
        viewModel.loadEventLists(commonRequest)
    }

    private fun loadStories() {

        apiHitCounter = 3
        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.latitude = mLatitude
        commonRequest.longitude = mLongitude
        viewModel.loadStories(commonRequest)
    }


    fun observeViewModel() {
        with(viewModel) {
            categoryLiveData.observe(this@HomeFragment, Observer {
                if (it.Results is String) {
                    SnackBarHelper.showErrorFromTop(
                        it.Results,
                        activity!!,
                        binding.container
                    )
                } else {

                    categoryList.clear()

                    /*add first 3 categories constant*/
                    categoryList.add(CategoryItem("Feeds", -3, ""))
                    categoryList.add(CategoryItem("Today", -1, ""))
                    categoryList.add(CategoryItem("Tomorrow", -2, ""))

                    categoryList.addAll(ObjectConverter.getCategoryResponse(it.Results))
                    categoryListAdapter.notifyDataSetChanged()

                }
//                loadStories()
                loadEventLists()
            })


            eventLiveData.observe(this@HomeFragment, Observer {
                if (it.Results is String) {
                    MessagesUtils.showToastInfo(context, it.Results)

                } else {

                    if (currentPage > 1) {
                        eventList.removeAt(eventList.size - 1)
                        eventListAdapter?.notifyItemRemoved(eventList.size)
                    } else {
                        eventList.clear()
                        eventListAdapter?.notifyDataSetChanged()
                    }

                    val data = ObjectConverter.getEventResponseList(it.Results)

                    if (currentPage == 1)
                        data.shuffle()

                    eventList.addAll(data)
                    totalPage = it.totalPageCount!!

                    eventListAdapter?.notifyDataSetChanged()

                }
            })

            eventReminderLiveData.observe(this@HomeFragment, Observer {

                eventList[mSelectedPosition]?.isInterested = "1"
                eventListAdapter.notifyItemChanged(mSelectedPosition)

            })

            storyLiveData.observe(this@HomeFragment, Observer {
                if (it.Results is String) {
                    SnackBarHelper.showErrorFromTop(
                        it.Results,
                        activity!!,
                        binding.container
                    )
                } else {
                    storyList.clear()
                    storyList.addAll(ObjectConverter.getStoryListResponse(it.Results))
                    storyListAdapter.notifyDataSetChanged()
                }

            })

            isProcessing.observe(this@HomeFragment, Observer {
                if (currentPage > 1 && apiHitCounter == 2)
                    return@Observer

                if (it) {
                    binding.progressLayout.visibility = View.VISIBLE
                    hideKeyboard()
                } else {
                    if (categoryList.size > 0)
                        categorySkeleton?.hide()

                    eventSkeleton?.hide()
                    binding.progressLayout.visibility = View.GONE
                }
            })

            isCategoryProcessing.observe(this@HomeFragment, Observer {
                if (currentPage > 1 && apiHitCounter == 2)
                    return@Observer

                if (it) {
                    showCategoryListSkeleton()
                    hideKeyboard()
                } else {
                    categorySkeleton?.hide()

                }
            })

            isEventProcessing.observe(this@HomeFragment, Observer {
                if (currentPage > 1 && apiHitCounter == 2)
                    return@Observer

                if (it) {
                    showEventListSkeleton()
                    hideKeyboard()
                } else {
                    eventSkeleton?.hide()

                }
            })


            mSnackbarText.observe(this@HomeFragment, Observer {
                if (it.code == 404 && apiHitCounter == 2) {
                    SnackBarHelper.showErrorFromTop(it.message, activity!!, binding.container)
                    eventList.clear()
                    eventListAdapter?.notifyDataSetChanged()
                    Log.d(TAG, " called>>>>> $apiHitCounter")
                } else if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, activity!!, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, activity!!)
                }
            })
        }
    }

    private fun hitReminder() {
        apiHitCounter = 4

        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.startDate = DateUtils.getTodayDateTime()
//        commonRequest.image =
        commonRequest.deviceType = "android"
        commonRequest.appName = "eventmoon"
        commonRequest.eventId = eventList[mSelectedPosition]?.eventId!!

        viewModel.hitReminder(commonRequest)
    }

    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> loadCategories()
            2 -> loadEventLists()
            3 -> loadStories()
            4 -> hitReminder()
        }
    }


    companion object {

        val FRAGMENT_NAME = HomeFragment::class.java.name

        fun newInstance() =
            HomeFragment().apply {
                val bundle = Bundle()
                arguments = bundle
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        Log.d(FRAGMENT_NAME, "HomeFragment >>>>>")
        if (resultCode == Activity.RESULT_OK) {
            setAddressDefault()
            loadEventLists()
        }
    }

    override val progressBar: ProgressBar? get() = TODO("Not yet implemented")

    private fun setAddressDefault() {
        val address = CommonHelper.getAddressToUse()
        mLatitude = address[0]
        mLongitude = address[1]
        (activity as HomeActivity).binding.pageSubTitleTv.setText(address[2])

        //type of address
        when (address[3]) {
            AppConstants.ADDRESS_TYPE.GPSL -> {
                binding.changeAddressRL.visibility = View.GONE
            }
            AppConstants.ADDRESS_TYPE.DL -> {
                binding.changeAddressRL.visibility = View.VISIBLE

            }
            AppConstants.ADDRESS_TYPE.SL -> {
                binding.changeAddressRL.visibility = View.GONE
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(eventBusModel: EventBusModel?) {
        when (eventBusModel?.eventType) {
            AppConstants.EVENTBUS_CONSTANTS.FOUND_LOCATION -> {

                Prefs.putString(AppConstants.PreferenceConstants.SELECTED_LATITUDE, "")

                setAddressDefault()
                loadInitialEvents()
            }
            AppConstants.EVENTBUS_CONSTANTS.LOCATION_DENIED -> {
                setAddressDefault()
            }
        }
    }
}