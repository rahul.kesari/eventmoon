package com.music_event_app.ui.home.fragments.locationfragment

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.LocationNearbyUseCase
import com.music_event_app.domain.usecase.TermsConditionUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi


/**
 * An interactor that calls the actual implementation of [AlbumViewModel](which is injected by DI)
 * it handles the response that returns data &
 * contains a list of actions, event steps
 */
class LocationNearbyFragmentViewModel constructor(private val locationNearbyUseCase: LocationNearbyUseCase) : BaseViewModel() {

    val TAG = LocationNearbyFragmentViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel>()
    val isProcessing = MutableLiveData<Boolean>()
    val eventResponseLiveData = MutableLiveData<BaseResponse>()

    @ExperimentalCoroutinesApi
    fun loadNearByEvents(commonRequest: CommonRequest) {
        isProcessing.value = true
        locationNearbyUseCase.loadNearByEvents(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
//                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    eventResponseLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }



}