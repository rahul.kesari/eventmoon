package com.music_event_app.ui.resetpassword

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.domain.usecase.LoginSignupUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import com.music_event_app.ui.login.LoginViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.IOException

class ResetPasswordViewModel constructor(private val loginSignupUseCase: LoginSignupUseCase) :
    BaseViewModel() {

    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel?>()
    val responseLiveData = MutableLiveData<BaseResponse>()
    val isProcessing = MutableLiveData<Boolean>()

    val TAG = ResetPasswordViewModel::class.java.name

    @ExperimentalCoroutinesApi
    fun resetPassword(loginRequest: LoginRequest?) {
        isProcessing.value = true
        loginSignupUseCase.resetPassword(
            scope,
            loginRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    responseLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }
}