package com.music_event_app.ui.resetpassword

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.music_event_app.R
import com.music_event_app.databinding.ActivityHomeBinding
import com.music_event_app.databinding.ActivityResetpasswordBinding
import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.hideKeyboard
import com.music_event_app.util.helper.Prefs
import com.music_event_app.util.helper.SnackBarHelper
import org.koin.android.viewmodel.ext.android.viewModel

class ResetPasswordActivity : BaseActivity<ResetPasswordViewModel, ActivityResetpasswordBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_resetpassword
    override val viewModel: ResetPasswordViewModel by viewModel()

    private val TAG: String = ResetPasswordActivity.javaClass.name
    var mLoginRequest: LoginRequest? = null
    var verificationCode: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        https://uat.eventmoon.com/#/resetpassword?verification=b80ffd58-26fe-43ec-8e4a-a148b1b3eb43
        val uri: Uri? = intent.data
        verificationCode = uri?.toString()!!.substring(uri?.toString()!!.indexOf("=") + 1)

        Log.d(
            TAG,
            "Verificaton Code>>>>  " + verificationCode + "    " + intent.data.toString() + "   " + uri.toString()
        )
        initViews()
        observeViewModel()
    }

    private fun initViews() {
        binding.resetPasswordBtn.setOnClickListener {
            if (validateFields()) {
                resetPassword()
            }
        }
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ResetPasswordActivity::class.java)
            context.startActivity(intent)
        }
    }


    private fun validateFields(): Boolean {
        if (binding.newPasswordEt.text.toString().isEmpty()) {
            SnackBarHelper.showErrorFromTop(
                getString(R.string.enter_new_password_string),
                this@ResetPasswordActivity,
                binding.container
            )
            return false
        } else if (binding.confirmPassowrdEt.text.toString().isEmpty()) {
            SnackBarHelper.showErrorFromTop(
                getString(R.string.confirm_password_string),
                this@ResetPasswordActivity,
                binding.container
            )
            return false
        } else if (binding.newPasswordEt.text.toString().length < 5 || binding.confirmPassowrdEt.text.toString().length < 5) {
            SnackBarHelper.showErrorFromTop(
                getString(R.string.password_length_check),
                this@ResetPasswordActivity,
                binding.container
            )
            return false
        } else if (!binding.newPasswordEt.text.toString()
                .equals(binding.confirmPassowrdEt.text.toString())
        ) {
            SnackBarHelper.showErrorFromTop(
                getString(R.string.password_not_matched_string),
                this@ResetPasswordActivity,
                binding.container
            )
            return false
        } else {
            return true
        }
    }

    fun observeViewModel() {
        with(viewModel) {
            responseLiveData.observe(this@ResetPasswordActivity, Observer {
                if (it.Results is String) {
                    SnackBarHelper.showSuccessFromTop(
                        it.Results,
                        this@ResetPasswordActivity,
                        binding.container
                    )
                    HomeActivity.start(this@ResetPasswordActivity)
                }
            })


            isProcessing.observe(this@ResetPasswordActivity, Observer {
                if (it) {
                    binding.progressLayout.visibility = View.VISIBLE
                    hideKeyboard()
                } else binding.progressLayout.visibility = View.GONE
            })

            mSnackbarText.observe(this@ResetPasswordActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@ResetPasswordActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            viewModel.resetPassword(mLoginRequest)
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@ResetPasswordActivity)
                }
            })

        }
    }


    private fun resetPassword() {
        var loginRequest = LoginRequest(
            password = binding.confirmPassowrdEt.text.toString(),
            VerificationCode = verificationCode
        )
        mLoginRequest = loginRequest
        viewModel.resetPassword(loginRequest)
    }

}
