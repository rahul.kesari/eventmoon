package com.music_event_app.ui.dialogs;

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import com.music_event_app.R
import com.music_event_app.databinding.DialogForgetPasswordBinding
import com.music_event_app.ui.login.LoginActivity
import com.music_event_app.util.helper.MessagesUtils


public class ForgetPasswordDialog(private val mContext: Activity,private val mListener: ForgetPasswordListener) :
    Dialog(mContext, R.style.mydialog) {

    lateinit var binding: DialogForgetPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding = DialogForgetPasswordBinding.inflate(LayoutInflater.from(mContext))
        setContentView(binding.root)
        setCancelable(true)
        initViews()
    }

    private fun initViews() {
        binding.crossIv.setOnClickListener {
            dismiss()
        }
        binding.submitBtn.setOnClickListener {
            if(validateFields()){
                mListener.onEmail(binding.emailEt.text.toString())
                dismiss()
            }
        }
    }

    private fun validateFields(): Boolean {
        return if (binding.emailEt.text.toString().isEmpty()) {
            MessagesUtils.showToastError(context, context.getString(R.string.enter_email_string))
            return false
        } else if (!(mContext as LoginActivity).validateEmail(binding.emailEt.text.toString())) {
            MessagesUtils.showToastError(
                context,
                context.getString(R.string.enter_valid_email_string)
            )
            return false
        } else {
            true
        }
    }


    interface ForgetPasswordListener {
        fun onEmail(email: String?)
    }
}