package com.music_event_app.ui.dialogs;

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import com.music_event_app.R
import com.music_event_app.databinding.DialogForgetPasswordBinding
import com.music_event_app.databinding.DialogRateFeedbackBinding
import com.music_event_app.ui.login.LoginActivity
import com.music_event_app.util.helper.MessagesUtils


 class RateFeedbackDialog(private val mContext: Activity, private val mListener: RateFeedbackDialgListener) :

    Dialog(mContext, R.style.mydialog) {

    lateinit var binding: DialogRateFeedbackBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding = DialogRateFeedbackBinding.inflate(LayoutInflater.from(mContext))
        setContentView(binding.root)
        setCancelable(true)
        initViews()
    }

    private fun initViews() {
        binding.cancelBtn.setOnClickListener {
            dismiss()
        }
        binding.submitBtn.setOnClickListener {
            if (validateFields()) {
                mListener.onSubmit(binding.reviewET.text.toString(), binding.ratingBar.rating)
                dismiss()
            }
        }
    }

    private fun validateFields(): Boolean {
        return true
    }


    interface RateFeedbackDialgListener {
        fun onSubmit(review: String?, ratings: Float)
    }
}