package com.music_event_app.ui.dialogs.sheets;

import android.view.LayoutInflater
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.music_event_app.R
import com.music_event_app.databinding.SheetLocationFilterRadiusBinding
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams


class LocationFilterSheet {
    private var mContext: AppCompatActivity? = null
    private var listener: LocationFilterListener? = null

    var bottomSheetDialog: BottomSheetDialog? = null

    private var radius = 20
    lateinit var binding: SheetLocationFilterRadiusBinding


    fun setUpDialog(@NonNull context: AppCompatActivity, defaultradius: Int?) {
        mContext = context
        radius = defaultradius!!
        binding =
            SheetLocationFilterRadiusBinding.inflate(LayoutInflater.from(mContext), null, false)
        bottomSheetDialog = BottomSheetDialog(context, R.style.BottomSheetDialog)
        bottomSheetDialog?.setContentView(binding.root)

        initViews()
        bottomSheetDialog?.show()
    }

    private fun hideDialog() {
        if (bottomSheetDialog != null) {
            if (bottomSheetDialog!!.isShowing) bottomSheetDialog?.dismiss()
        }
    }


    interface LocationFilterListener {
        fun onClicked(selectedRadius: Int?)
    }

    private fun initViews() {

        binding.radiusSeekbar.setProgress(radius.toFloat())
        binding.applyBtn.setOnClickListener {
            listener?.onClicked(binding.radiusSeekbar.progress)
            hideDialog()
        }

        binding.crossBtn.setOnClickListener {
            hideDialog()
        }


        binding.radiusSeekbar.onSeekChangeListener = object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams) {
                binding.radiusTxt.setText("${seekParams.progress} Miles")
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {}
            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {}
        }
    }

    fun setOnFilterListener(onCommunicationListener: LocationFilterListener?) {
        listener = onCommunicationListener
    }
}
