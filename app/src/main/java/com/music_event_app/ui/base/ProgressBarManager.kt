package com.music_event_app.ui.base

import android.widget.ProgressBar

interface ProgressBarManager {
    val progressBar: ProgressBar?
}