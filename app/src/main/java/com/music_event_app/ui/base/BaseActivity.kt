package com.music_event_app.ui.base

import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.music_event_app.util.extensions.TransitionAnimation
import com.music_event_app.util.extensions.setCustomAnimation
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

abstract class BaseActivity<V : BaseViewModel, B : ViewDataBinding>: AppCompatActivity(), BaseViewGroup<V, B>, NavigationListener {

    final override lateinit var binding: B
    abstract var frameContainerId: Int
    val backCallback: MutableLiveData<OnBackPressedListener?> = MutableLiveData()
    private var lastFragmentTag = ""
    private val EMAIL_PATTERN = "[a-zA-z_.0-9]+@[a-zA-Z]+[.][a-zA-Z.]+"

    private var pattern: Pattern? = null
    private var matcher: Matcher? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.lifecycleOwner = this
    }

    override fun onBackPressed() {
        if (backCallback.value == null) {
            if (!canBack())
                super.onBackPressed()
        } else {
            if (backCallback.value?.onBackPressed(this) == false){
                if (!canBack()) {
                    super.onBackPressed()
                }
            }
        }
    }

    private fun canBack(): Boolean {
        if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
            return true
        }

        supportFragmentManager.getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1)
            .name?.let {
            supportFragmentManager.findFragmentByTag(it)?.apply {
                if (childFragmentManager.backStackEntryCount != 0) {
                    childFragmentManager.popBackStack()
                    return true
                }

            }
        }
        return false
    }

    override fun onDestroy() {
        if (backCallback.value != null) {
            backCallback.value = null
        }
        super.onDestroy()
    }

    override fun navigateTo(fragment: Fragment, backStackTag: String, animationType: TransitionAnimation, isAdd: Boolean) {
        if (lastFragmentTag.equals(backStackTag, ignoreCase = true)) {
            Log.e("BaseActivity", "Cannot navigate to the current fragment. It's already visible on the screen")
            return
        }

        if (frameContainerId == 0) {
            Log.e("BaseActivity", "No container is defined to navigate on!")
            return
        }

        if (supportFragmentManager == null) {
            Log.e("BaseActivity", "supportFragmentManager is null")
            return
        }

        if (supportFragmentManager.backStackEntryCount > 0 && isAdd) {
            val tag = supportFragmentManager
                .getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1)
                .name

            supportFragmentManager.findFragmentByTag(tag)?.apply {
                this.onPause()
            }
        }

        supportFragmentManager.findFragmentByTag(backStackTag)?.let {
            supportFragmentManager.popBackStack(backStackTag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            return
        }


        supportFragmentManager.beginTransaction().apply {
            setCustomAnimation(animationType)
            if (isAdd) {
                add(frameContainerId, fragment, backStackTag)
            } else {
                replace(frameContainerId, fragment, backStackTag)
            }
            //addToBackStack(backStackTag)
            commitAllowingStateLoss()
        }

        lastFragmentTag = backStackTag
    }

    open fun setupInsets(topView:View) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            topView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        } else {
//            moviesRecyclerView.updatePadding(top = toolbarHeight + baseMoviesPadding)
        }
        ViewCompat.setOnApplyWindowInsetsListener(
            topView
        ) { v: View?, insets: WindowInsetsCompat ->
            val params = topView.layoutParams as FrameLayout.LayoutParams
            params.setMargins(0, 0, 0, insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
    }

    open fun validateEmail(hex: String?): Boolean {
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern?.matcher(hex)
        return matcher!!.matches()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

}