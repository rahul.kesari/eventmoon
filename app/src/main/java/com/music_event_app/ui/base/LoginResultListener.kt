package com.music_event_app.ui.base

interface LoginResultListener {

    fun onResult(status: Boolean)
}
