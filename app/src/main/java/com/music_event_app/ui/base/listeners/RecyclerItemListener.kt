package com.music_event_app.ui.base.listeners


import java.text.FieldPosition

/**
 * Created by Rahul
 */

interface RecyclerItemListener {
    fun onItemSelected(item: Any,position: Int)
}
