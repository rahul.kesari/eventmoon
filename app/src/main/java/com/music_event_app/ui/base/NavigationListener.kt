package com.music_event_app.ui.base

import androidx.fragment.app.Fragment
import com.music_event_app.util.extensions.TransitionAnimation

interface NavigationListener {

    fun navigateTo(
        fragment: Fragment,
        backStackTag: String,
        animationType: TransitionAnimation,
        isAdd: Boolean
    )

}