package com.music_event_app.ui.base

import androidx.fragment.app.FragmentActivity

interface OnBackPressedListener {

    fun onBackPressed(activity: FragmentActivity) : Boolean

}
