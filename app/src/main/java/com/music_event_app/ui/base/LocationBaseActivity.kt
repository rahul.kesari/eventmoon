package com.music_event_app.ui.base

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.music_event_app.BuildConfig
import com.music_event_app.R
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.TransitionAnimation
import com.music_event_app.util.extensions.setCustomAnimation
import com.music_event_app.util.helper.Prefs
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

abstract class LocationBaseActivity<V : BaseViewModel, B : ViewDataBinding> : AppCompatActivity(),
    BaseViewGroup<V, B>, NavigationListener, ActivityCompat.OnRequestPermissionsResultCallback,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    LocationListener {

    final override lateinit var binding: B
    abstract var frameContainerId: Int
    val backCallback: MutableLiveData<OnBackPressedListener?> = MutableLiveData()
    private var lastFragmentTag = ""


    private val TAG = "LocationBaseActivity"

    /*For Location Pick*/
    private var mGoogleApiClient: GoogleApiClient? = null
    protected var mLastLocation: Location? = null
    private val REQUEST_CODE_SETTING = 168
    private var mCurrentLatitute: String? = null
    private var mCurrentLongitute: String? = null
    private val REQUEST_CODE_PERMISSION = 1
    private var address: List<Address> =
        ArrayList()
    private var mGeocoder: Geocoder? = null

    private var mLocationRequest: LocationRequest? = null
    private val REQUEST_CHECK_SETTINGS = 0x1
    private val EMAIL_PATTERN = "[a-zA-z_.0-9]+@[a-zA-Z]+[.][a-zA-Z.]+"

    private var pattern: Pattern? = null
    private var matcher: Matcher? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.lifecycleOwner = this
    }

    override fun onBackPressed() {
        if (backCallback.value == null) {
            if (!canBack())
                super.onBackPressed()
        } else {
            if (backCallback.value?.onBackPressed(this) == false) {
                if (!canBack()) {
                    super.onBackPressed()
                }
            }
        }
    }

    private fun canBack(): Boolean {
        if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
            return true
        }

        supportFragmentManager.getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1)
            .name?.let {
                supportFragmentManager.findFragmentByTag(it)?.apply {
                    if (childFragmentManager.backStackEntryCount != 0) {
                        childFragmentManager.popBackStack()
                        return true
                    }

                }
            }
        return false
    }

    override fun onDestroy() {
        if (backCallback.value != null) {
            backCallback.value = null
        }
        super.onDestroy()
    }

    override fun navigateTo(
        fragment: Fragment,
        backStackTag: String,
        animationType: TransitionAnimation,
        isAdd: Boolean
    ) {
        if (lastFragmentTag.equals(backStackTag, ignoreCase = true)) {
            Log.e(
                "BaseActivity",
                "Cannot navigate to the current fragment. It's already visible on the screen"
            )
            return
        }

        if (frameContainerId == 0) {
            Log.e("BaseActivity", "No container is defined to navigate on!")
            return
        }

        if (supportFragmentManager == null) {
            Log.e("BaseActivity", "supportFragmentManager is null")
            return
        }

        if (supportFragmentManager.backStackEntryCount > 0 && isAdd) {
            val tag = supportFragmentManager
                .getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1)
                .name

            supportFragmentManager.findFragmentByTag(tag)?.apply {
                this.onPause()
            }
        }

        supportFragmentManager.findFragmentByTag(backStackTag)?.let {
            supportFragmentManager.popBackStack(
                backStackTag,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
            return
        }


        supportFragmentManager.beginTransaction().apply {
            setCustomAnimation(animationType)
            if (isAdd) {
                add(frameContainerId, fragment, backStackTag)
            } else {
                replace(frameContainerId, fragment, backStackTag)
            }
            //addToBackStack(backStackTag)
            commitAllowingStateLoss()
        }

        lastFragmentTag = backStackTag
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "onActivityResult: ")
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            // final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
            when (requestCode) {
                REQUEST_CHECK_SETTINGS -> when (resultCode) {
                    Activity.RESULT_OK -> startLocationUpdates()
                    Activity.RESULT_CANCELED ->                             //show gps not enabled message
                        mGoogleApiClient?.disconnect()
                    else -> {
                    }
                }
            }
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Log.d(TAG, "onRequestPermissionsResult: ");
        if (requestCode == REQUEST_CODE_PERMISSION) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            // Check if the only required permission has been granted
            var i = 0
            val len = permissions.size
            while (i < len) {
                val permission = permissions[i]
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(LocationImagePickerBaseActivity.this, "PERMISSION_GRANTED", Toast.LENGTH_SHORT).show();
                    getLocation()
                } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    val showRationale =
                        ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        )
                    if (!showRationale) {
                        // user denied flagging NEVER ASK AGAIN
                        // you can either enable some fall back,
                        // disable features of your app
                        // or open another dialog explaining
                        // again the permission and directing to
                        // the app setting
                        openAppPermissionDailog()
                    } else if (Manifest.permission.ACCESS_FINE_LOCATION == permission) {
                        // showRationale(permission, R.string.permission_denied_contacts);
                        // user denied WITHOUT never ask again
                        // this is a good place to explain the user
                        // why you need the permission and ask if he want
                        // to accept it (the rationale)
                        openPermissionDailogDetail()
                    }
                }
                i++
            }
        }
    }


    open fun openAppPermissionDailog() {
        val alert =
            AlertDialog.Builder(
                this
            )
        alert.setTitle(resources.getString(R.string.grantpermission))
        alert.setMessage(resources.getString(R.string.opentoturnonlocation))
        alert.setPositiveButton(
            resources.getString(R.string.cancel),
            DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        alert.setNegativeButton(
            "OK"
        ) { dialog, which -> //dialog.dismiss();
            val myAppSettings = Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:$packageName")
            )
            myAppSettings.addCategory(Intent.CATEGORY_DEFAULT)
            myAppSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivityForResult(myAppSettings, REQUEST_CODE_SETTING)
            finish()
        }
        alert.show()
    }


    override fun onStop() {
        super.onStop()
        try {
            if (mGoogleApiClient!!.isConnected()) {
                stopLocationUpdates()
            }
        } catch (e: Exception) {
        }
        try {
            mGoogleApiClient?.disconnect()
        } catch (ae: Exception) {
        }
        Log.d(TAG, "onStop: ")
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        // stopLocationUpdates();
    }

    protected open fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        Log.d(TAG, "Location update stopped .......................")
    }


    open fun initLocationWithCheck() {
        if (Build.VERSION.SDK_INT >= 23) {
            verifyLocationPermissions()
        } else {
            getLocation()
        }
    }

    open fun verifyLocationPermissions() {
        val permission = ActivityCompat.checkSelfPermission(
            this@LocationBaseActivity,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        val coarsepermission = ActivityCompat.checkSelfPermission(
            this@LocationBaseActivity,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (permission != PackageManager.PERMISSION_GRANTED || coarsepermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this@LocationBaseActivity,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                REQUEST_CODE_PERMISSION
            )
        } else {
            getLocation()
        }
    }


    private fun getLocation() {
        if (BuildConfig.DEBUG) Log.d(TAG, "getLocation: executed")
        if (mGoogleApiClient == null) {
            mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API) //                    .addApi(Places.GEO_DATA_API)
                //                    .addApi(Places.PLACE_DETECTION_API)
                .build()
        }
        mGoogleApiClient?.connect()
    }


    protected open fun startLocationUpdates() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(
                this@LocationBaseActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this@LocationBaseActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        if (mGoogleApiClient!!.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this@LocationBaseActivity
            )
        }
    }


    override fun onLocationChanged(location: Location) {
        mCurrentLatitute = location.latitude.toString()
        mCurrentLongitute = location.longitude.toString()
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onLocationChanged: ")
        }
        mGeocoder = Geocoder(this@LocationBaseActivity, Locale.getDefault())
        try {
            address = mGeocoder!!.getFromLocation(
                mCurrentLatitute!!.toDouble(),
                mCurrentLongitute!!.toDouble(),
                1
            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            getAddress()
        } catch (e: Exception) {
        }
    }

    private fun openPermissionDailogDetail() {
        val alert =
            AlertDialog.Builder(
                this
            )
        alert.setTitle(getString(R.string.denyDialogHeading))
        alert.setMessage(getString(R.string.denyDialogDetail))
        alert.setPositiveButton(
            getString(R.string.imsure),
            DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
                onPermissionDenied()
            })
        alert.setNegativeButton(
            getString(R.string.retry),
            DialogInterface.OnClickListener { dialog, which -> requestLocationPermission() })
        alert.show()
    }

    override fun onConnected(bundle: Bundle?) {
        Log.d(TAG, "onConnected: ")
        createLocationRequest()
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)
        builder.setAlwaysShow(true)
        val result =
            LocationServices.SettingsApi.checkLocationSettings(
                mGoogleApiClient,
                builder.build()
            )
        result.setResultCallback(ResultCallback { locationSettingsResult ->
            val status =
                locationSettingsResult.status
            Log.d(TAG, "onResult: $status")
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                    // showCurrentPlace();
                    if (ActivityCompat.checkSelfPermission(
                            this@LocationBaseActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this@LocationBaseActivity,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        return@ResultCallback
                    }
                    mLastLocation =
                        LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
                    if (mLastLocation != null) {
                        if (BuildConfig.DEBUG) {
                            Log.d(
                                TAG,
                                "latitude: -  " + mLastLocation?.getLatitude()
                            )
                            Log.d(
                                TAG,
                                "longitude: -  " + mLastLocation?.getLongitude()
                            )
                        }
                        mCurrentLatitute = mLastLocation?.getLatitude().toString()
                        mCurrentLongitute = mLastLocation?.getLongitude().toString()
                        mGeocoder =
                            Geocoder(this@LocationBaseActivity, Locale.getDefault())
                        try {
                            address = mGeocoder!!.getFromLocation(
                                mCurrentLatitute!!.toDouble(),
                                mCurrentLongitute!!.toDouble(),
                                1
                            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            getAddress()
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "onResult: $address")
                            }
                            Log.d(
                                TAG,
                                "onResult: " + address.get(0).getAddressLine(1)
                            )
                        } catch (ee: Exception) {
                            ee.printStackTrace()
                        }
                    } else {
                        startLocationUpdates()
                    }
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(
                            this@LocationBaseActivity,
                            REQUEST_CHECK_SETTINGS
                        )
                    } catch (e: SendIntentException) {
                        // Ignore the error.
                    }
                    mGoogleApiClient?.disconnect()
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
                LocationSettingsStatusCodes.CANCELED -> {
                    Log.d(TAG, "onResult: canceled")
                    onPermissionDenied()
                    mGoogleApiClient?.disconnect()
                }
            }
        })
    }

    override fun onConnectionSuspended(i: Int) {
        Log.d(TAG, "onConnectionSuspended: ")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        //show error message
//        finish();
        Log.d(TAG, "onConnectionFailed: ")
    }


    protected open fun createLocationRequest() {
        Log.d(TAG, "createLocationRequest: ")
        mLocationRequest = LocationRequest()
        mLocationRequest?.setInterval(3000)
        mLocationRequest?.setFastestInterval(1000)
        mLocationRequest?.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
    }


    private fun getAddress() {
        if (address != null && mCurrentLatitute!!.isNotEmpty()) {
            //String mUserAddLine = address.get(0).getAddressLine(0);//house no. // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            var mUserAddLine = ""
            val state: String = address.get(0).adminArea
            val city: String = address.get(0).locality
            val country: String = address.get(0).countryName

            Prefs.putString(AppConstants.PreferenceConstants.CITY, city)
            Prefs.putString(AppConstants.PreferenceConstants.COUNTRY, country)
            Prefs.putString(AppConstants.PreferenceConstants.STATE, state)
            Prefs.putString(AppConstants.PreferenceConstants.LATITUDE, mCurrentLatitute)
            Prefs.putString(AppConstants.PreferenceConstants.LONGITUDE, mCurrentLongitute)
            Log.d(TAG, "getAddress: $state   $country   $city")

            gotCurrentLocation()
            mGoogleApiClient?.disconnect()
        }
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            //   Toast.makeText(this, "shouldShowRequestPermissionRationale_if", Toast.LENGTH_SHORT).show();
        }
        ActivityCompat.requestPermissions(
            this@LocationBaseActivity,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            REQUEST_CODE_PERMISSION
        )
    }

    open fun validateEmail(hex: String?): Boolean {
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern?.matcher(hex)
        return matcher!!.matches()
    }

    open fun setupInsets(topView: View) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            topView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        } else {
//            moviesRecyclerView.updatePadding(top = toolbarHeight + baseMoviesPadding)
        }
        ViewCompat.setOnApplyWindowInsetsListener(
            topView
        ) { v: View?, insets: WindowInsetsCompat ->
            val params = topView.layoutParams as FrameLayout.LayoutParams
            params.setMargins(0, 0, 0, insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
    }

    abstract fun gotCurrentLocation()

    abstract fun onPermissionDenied()
}