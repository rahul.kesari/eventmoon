package com.music_event_app.ui.createnewevent

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.locationsearch.LocationDetailResponse
import com.music_event_app.domain.model.locationsearch.LocationSearchResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.MyEventUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.MultipartBody

class CreateNewEventViewModel constructor(private val myEventUseCase: MyEventUseCase) :
    BaseViewModel() {

    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel?>()

    val categoryLiveData = MutableLiveData<BaseResponse>()
    val eventSubmitLiveData = MutableLiveData<BaseResponse>()

    val isProcessing = MutableLiveData<Boolean>()
    val mSnackbarHandler = MutableLiveData<ErrorModel?>()
    val locationSearchLiveData = MutableLiveData<LocationSearchResponse>()
    val locationDetailLiveData = MutableLiveData<LocationDetailResponse>()

    val TAG = CreateNewEventViewModel::class.java.name


    @ExperimentalCoroutinesApi
    fun loadCategories(commonRequest: CommonRequest) {
        isProcessing.value = true
        myEventUseCase.loadCategories(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    categoryLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun searchLocation(url: String) {
        isProcessing.value = true
        myEventUseCase.searchLocation(
            scope,
            url,
            object : UseCaseResponse<LocationSearchResponse> {
                override fun onSuccess(result: LocationSearchResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    locationSearchLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarHandler.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun locationDetail(url: String) {
        isProcessing.value = true
        myEventUseCase.locationDetail(
            scope,
            url,
            object : UseCaseResponse<LocationDetailResponse> {
                override fun onSuccess(result: LocationDetailResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    locationDetailLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarHandler.postValue(errorModel)
                }
            })
    }


    @ExperimentalCoroutinesApi
    fun createNewEvent(multipartBody: MultipartBody) {
        isProcessing.value = true
        myEventUseCase.createNewEvent(
            scope,
            multipartBody,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventSubmitLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }
}