package com.music_event_app.ui.createnewevent

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dhaval2404.imagepicker.ImagePicker
import com.music_event_app.R
import com.music_event_app.databinding.ActivityCreateNewEventBinding
import com.music_event_app.domain.model.category.CategoryItem
import com.music_event_app.domain.model.locationsearch.LocationDetailResponse
import com.music_event_app.domain.model.locationsearch.Prediction
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.ui.adapters.LocationSuggestionAdapter
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.ui.login.LoginActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.net.URLEncoder


class CreateNewEventActivity :
    BaseActivity<CreateNewEventViewModel, ActivityCreateNewEventBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_create_new_event
    override val viewModel: CreateNewEventViewModel by viewModel()

    private val TAG: String = CreateNewEventActivity.javaClass.name

    private var apiHitCounter = 0
    private var categoryList = ArrayList<CategoryItem?>()
    private var photoPath = ""
    private lateinit var locationSuggestionAdapter: LocationSuggestionAdapter
    private var locationList = ArrayList<Prediction?>()

    private var placeDetailResponse: LocationDetailResponse? = null
    private var stopSearch = false
    private var selectedPlaceId: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupInsets(binding.container)
        initViews()
        initSearchViews()
        initLocationRecycler()
        observeViewModel()
        getCategories()
    }

    private fun initViews() {
        binding.toolbarLayout.titleTV.text = getString(R.string.create_event_string)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener {
            finish()
        }
        binding.imageRl.setOnClickListener {
            showImageChooseDialog()
        }

        binding.startDateTv.setOnClickListener {
            CommonHelper.openCalendarDialog(this@CreateNewEventActivity, object :
                CommonHelper.DateListener {

                override fun onDateFetched(dayofMonth: String, monthOfyear: String, year: String) {
                    val chdate = "$monthOfyear-$dayofMonth-$year"
                    binding.startDateTv.setText(chdate!!)
                }
            })
        }

        binding.endDateTv.setOnClickListener {
            CommonHelper.openCalendarDialog(this@CreateNewEventActivity, object :
                CommonHelper.DateListener {
                override fun onDateFetched(dayofMonth: String, monthOfyear: String, year: String) {
                    val chdate = "$monthOfyear-$dayofMonth-$year"
                    binding.endDateTv.setText(chdate!!)
                }
            })
        }

        binding.startTimeTv.setOnClickListener {
            CommonHelper.showTimePickerDialog(
                this@CreateNewEventActivity,
                "select start time",
                object : CommonHelper.TimePickerDialogListener {
                    override fun onTimeSelected(time: String?) {
                        binding.startTimeTv.setText(time!!)
                    }
                })
        }

        binding.endTimeTv.setOnClickListener {
            CommonHelper.showTimePickerDialog(
                this@CreateNewEventActivity,
                "select end time",
                object : CommonHelper.TimePickerDialogListener {
                    override fun onTimeSelected(time: String?) {
                        binding.endTimeTv.setText(time!!)
                    }
                })
        }

        binding.createEventBtn.setOnClickListener {
            if (validateFields(true))
                createEvent()
        }
    }


    private fun validateFields(showerror: Boolean): Boolean {
        if (photoPath == null || photoPath.isEmpty()) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Select Event Image",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        } else if (binding.eventCategorySpinner.selectedItemPosition == 0) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Select Event Category",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        } else if (binding.eventNameTv.text.toString().isEmpty()) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Enter Event Name",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        } else if (binding.locationNameTv.text.toString().isEmpty()) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Search & select location",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        } else if (binding.startDateTv.text.toString().isEmpty()) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Select Start Date",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        } else if (binding.endDateTv.text.toString().isEmpty()) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Select End Date",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        } else if (binding.startTimeTv.text.toString().isEmpty()) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Select Start Time",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        } else if (binding.endTimeTv.text.toString().isEmpty()) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Select End Time",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        } else if (binding.descriptionEt.text.toString().isEmpty()) {
            if (showerror) {
                SnackBarHelper.showErrorFromTop(
                    "Enter Description",
                    this@CreateNewEventActivity,
                    binding.container
                )
            }
            return false
        }
        return true
    }

    private fun initLocationRecycler() {
        binding.locationSearchRV.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        locationSuggestionAdapter = LocationSuggestionAdapter(locationList, object :
            RecyclerItemListener {
            override fun onItemSelected(item: Any, position: Int) {
                stopSearch = true
                selectedPlaceId = (item as Prediction).place_id
                binding.locationNameTv.setText((item as Prediction).description)
                binding.locationNameTv.setSelection((item as Prediction).description!!.length)
                binding.locationSearchRV.visibility = View.GONE
                locationDetail()
            }
        })

        binding.locationSearchRV.adapter = locationSuggestionAdapter
    }

    fun showImageChooseDialog() {
        CommonHelper.showImagePickerDialog(this@CreateNewEventActivity,
            object : CommonHelper.ImagePickerDialogListener {
                override fun onCameraClicked() {
                    pickPicture()
                }

                override fun onGalleryClicked() {
                    pickGallery()
                }
            })
    }


    fun observeViewModel() {
        with(viewModel) {

            isProcessing.observe(this@CreateNewEventActivity, Observer {
                if (it) binding.progressLayout.visibility =
                    View.VISIBLE else binding.progressLayout.visibility = View.GONE
            })

            eventSubmitLiveData.observe(this@CreateNewEventActivity, Observer {
                MessagesUtils.showToastSuccess(
                    this@CreateNewEventActivity,
                    getString(R.string.event_msg_success)
                )
            })


            categoryLiveData.observe(this@CreateNewEventActivity, Observer {
                if (it.Results is String) {
                    SnackBarHelper.showErrorFromTop(
                        it.Results,
                        this@CreateNewEventActivity,
                        binding.container
                    )
                } else {

                    categoryList.clear()

                    /*add first 3 categories constant*/
                    categoryList.add(CategoryItem("Feeds", -3, ""))
                    categoryList.add(CategoryItem("Today", -1, ""))
                    categoryList.add(CategoryItem("Tomorrow", -2, ""))

                    categoryList.addAll(ObjectConverter.getCategoryResponse(it.Results))
                    initCategoryAdapter()
                }
            })

            locationSearchLiveData.observe(this@CreateNewEventActivity, Observer {
                binding.locationSearchRV.visibility = View.VISIBLE
                locationList.clear()
                locationList.addAll(it.predictions)
                locationSuggestionAdapter.notifyDataSetChanged()
            })

            locationDetailLiveData.observe(this@CreateNewEventActivity, Observer {
                placeDetailResponse = it

            })

            mSnackbarHandler.observe(this@CreateNewEventActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@CreateNewEventActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@CreateNewEventActivity)
                }
            })
        }
    }

    @ExperimentalCoroutinesApi
    private fun initSearchViews() {

        binding.locationNameTv.addTextChangedListener(DebouncingQueryTextListener(
            this@CreateNewEventActivity
        ) { newText ->
            newText?.let {

                if (stopSearch) {
                    stopSearch = false
                    return@let
                }
                Log.d("DebounceTest", "value: $it")
                searchLocation()
            }
        })
    }

    companion object {

        fun start(context: Context) {
            val intent = Intent(context, CreateNewEventActivity::class.java)

            context.startActivity(intent)
        }
    }

    private fun initCategoryAdapter() {

        val categories = arrayOfNulls<String>(categoryList.size + 1)

        categories[0] = "Event Category"
        categoryList.forEachIndexed { index, categoryItem ->
            categories[index + 1] = categoryItem?.CategoryName
        }

        val categoryAdapter = ArrayAdapter<CharSequence>(this, R.layout.spinner_text, categories)
        categoryAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown)
        binding.eventCategorySpinner.adapter = categoryAdapter
    }

    private fun getCategories() {
        apiHitCounter = 1
        val commonRequest = CommonRequest(pageNumber = 0)
        viewModel.loadCategories(commonRequest)
    }

    private fun searchLocation() {
        apiHitCounter = 2
        viewModel.searchLocation(AppConstants.PLACE_API + binding.locationNameTv.text.toString())
    }

    private fun locationDetail() {
        if (selectedPlaceId!!.isEmpty())
            return
        apiHitCounter = 3
        viewModel.locationDetail(AppConstants.PLACE_DETAIL_API + selectedPlaceId)
    }


    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> getCategories()
            2 -> searchLocation()
            3 -> locationDetail()
            4 -> createEvent()
        }
    }

    private fun pickGallery() {
        ImagePicker.with(this)
            .galleryOnly()
            .crop() //Crop image(Optional), Check Customization for more option
            .compress(624)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                624,
                624
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    private fun pickPicture() {
        ImagePicker.with(this)
            .cameraOnly()
            .crop() //Crop image(Optional), Check Customization for more option
            .compress(624)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                624,
                624
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            val fileUri = data?.data

            binding.eventIv.setImageURI(fileUri)

            //You can also get File Path from intent
            photoPath = ImagePicker.getFilePath(data)!!
            binding.plusIv.visibility = View.GONE

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
//            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    private fun createEvent() {

        apiHitCounter = 4

        val builder = MultipartBody.Builder()
            .setType(MultipartBody.FORM)

        builder.addFormDataPart(
            "userID",
            Prefs.getInt(AppConstants.PreferenceConstants.USER_ID).toString()
        )
        builder.addFormDataPart(
            "accessToken",
            Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN).toString()
        )

        builder.addFormDataPart("eventName", binding.eventNameTv.text.toString())
        builder.addFormDataPart("location", binding.locationNameTv.text.toString())

        builder.addFormDataPart(
            "latitude",
            placeDetailResponse?.result?.geometry?.location?.lat.toString()
        )
        builder.addFormDataPart(
            "longitude",
            placeDetailResponse?.result?.geometry?.location?.lng.toString()
        )

        builder.addFormDataPart("startDate", binding.startDateTv.text.toString())
        builder.addFormDataPart("endDate", binding.endDateTv.text.toString())
        builder.addFormDataPart("startTime", binding.startTimeTv.text.toString())
        builder.addFormDataPart("endTime", binding.endTimeTv.text.toString())

        builder.addFormDataPart("eventDescription", binding.descriptionEt.text.toString())
        builder.addFormDataPart(
            "categoryID",
            categoryList.get(binding.eventCategorySpinner.selectedItemPosition - 1)?.Categoryid.toString()
        )
        builder.addFormDataPart("dealtext", "dummy")
        builder.addFormDataPart("percentage", "20")

        if (photoPath.isNotEmpty()) {
            val MEDIA_TYPE_PNG = MediaType.parse("image/*")
            val file = File(photoPath)
            val fileName = photoPath.substring(photoPath.lastIndexOf("/") + 1)
            val addFormDataPart = builder.addFormDataPart(
                "filepath",
                URLEncoder.encode(fileName, "utf-8"),
                RequestBody.create(MEDIA_TYPE_PNG, file)
            )
        }

        viewModel.createNewEvent(builder.build())
    }

    override fun onBackPressed() {
        CommonHelper.showConfirmationDialog(this,getString(R.string.go_back_alert_msg),object :CommonHelper.DialogCLick{
            override fun onNoClicked() {
            }

            override fun onYesClicked() {
                try {
                    finish()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
