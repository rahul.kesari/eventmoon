package com.music_event_app.ui.locationsearch;

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.music_event_app.R
import com.music_event_app.databinding.ActivityLocationSearchBinding
import com.music_event_app.domain.model.locationsearch.LocationDetailResponse
import com.music_event_app.domain.model.locationsearch.Prediction
import com.music_event_app.ui.adapters.LocationSuggestionAdapter
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.CommonHelper
import com.music_event_app.util.helper.DebouncingQueryTextListener
import com.music_event_app.util.helper.Prefs
import com.music_event_app.util.helper.SnackBarHelper
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class LocationSearchActivity :
    BaseActivity<LocationSearchViewModel, ActivityLocationSearchBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_location_search
    override val viewModel: LocationSearchViewModel by viewModel()

    private val TAG = LocationSearchActivity.javaClass.name

    private var PLACE_API = AppConstants.PLACE_API

    private var PLACE_DETAIL_API = AppConstants.PLACE_DETAIL_API

    private var selectedPlaceId: String? = ""

    private lateinit var locationSuggestionAdapter: LocationSuggestionAdapter
    private var locationList = ArrayList<Prediction?>()

    private var placeDetailResponse: LocationDetailResponse? = null
    private var stopSearch = true
    var apiHitCounter: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupInsets(binding.topLayout)
        initViews()
        setAddressDefault()
        initLocationRecycler()
        initSearchViews()
        observeViewModel()

    }



    private fun initViews() {

        binding.toolbarLayout.toolbar.titleTV.setText(getString(R.string.filter_string))

        binding.toolbarLayout.toolbar.setNavigationOnClickListener {
            finish()
        }

        binding.searchIV.setOnClickListener {
            handleAnimation()
        }

        binding.startDateTV.setOnClickListener {
            CommonHelper.openCalendarDialog(this, object : CommonHelper.DateListener {

                override fun onDateFetched(dayofMonth: String, monthOfyear: String, year: String) {
                    val chdate = "$dayofMonth, $monthOfyear/$year"
                    binding.startDateTV.text = chdate
                }
            })
        }

        binding.endDateTV.setOnClickListener {
            CommonHelper.openCalendarDialog(this, object : CommonHelper.DateListener {
                override fun onDateFetched(dayofMonth: String, monthOfyear: String, year: String) {
                    val chdate = "$dayofMonth, $monthOfyear/$year"
                    binding.endDateTV.text = chdate
                }
            })
        }
    }

    private fun setAddressDefault() {
        val address = CommonHelper.getAddressToUse()
        binding.searchLocationET.setText(address[2])
    }

    private fun initLocationRecycler() {
        binding.locationSearchRV.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        locationSuggestionAdapter = LocationSuggestionAdapter(locationList, object :
            RecyclerItemListener {
            override fun onItemSelected(item: Any, position: Int) {
                stopSearch = true
                selectedPlaceId = (item as Prediction).place_id
                binding.searchLocationET.setText((item as Prediction).description)
                binding.searchLocationET.setSelection((item as Prediction).description!!.length)
                locationDetail()
            }
        })

        binding.locationSearchRV.adapter = locationSuggestionAdapter
    }


    fun observeViewModel() {

        with(viewModel) {

            isProcessing.observe(this@LocationSearchActivity, Observer {
                if (it) binding.progressLayout.visibility =
                    View.VISIBLE else binding.progressLayout.visibility = View.GONE
            })

            locationSearchLiveData.observe(this@LocationSearchActivity, Observer {
                binding.locationSearchRV.visibility = View.VISIBLE
                locationList.clear()
                locationList.addAll(it.predictions)
                locationSuggestionAdapter.notifyDataSetChanged()
            })

            locationDetailLiveData.observe(this@LocationSearchActivity, Observer {
                placeDetailResponse = it

            })

            mSnackbarHandler.observe(this@LocationSearchActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@LocationSearchActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {

                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@LocationSearchActivity)
                }
            })
        }
    }

    private fun setResultsBack() {

        val intent = Intent()
        if (placeDetailResponse != null) {

            Prefs.putString(
                AppConstants.PreferenceConstants.SELECTED_ADDRESS,
                placeDetailResponse?.result?.name
            )
            Prefs.putString(
                AppConstants.PreferenceConstants.SELECTED_LATITUDE,
                "" + placeDetailResponse?.result?.geometry?.location?.lat
            )
            Prefs.putString(
                AppConstants.PreferenceConstants.SELECTED_LONGITUDE,
                "" + placeDetailResponse?.result?.geometry?.location?.lng
            )

        }
        setResult(RESULT_OK, intent)
        supportFinishAfterTransition()
        finish()
    }

    @ExperimentalCoroutinesApi
    private fun initSearchViews() {

        binding.searchLocationET.addTextChangedListener(DebouncingQueryTextListener(this@LocationSearchActivity) { newText ->
            newText?.let {

                if (stopSearch) {
                    stopSearch = false
                    return@let
                }
                Log.d("DebounceTest", "value: $it")
                searchLocation()
            }
        })
    }


    companion object {
        val REQ_CODE = 1001

        fun start(context: Context) {
            val intent = Intent(context, LocationSearchActivity::class.java)
            context.startActivity(intent)
        }

        fun startForResult(context: Activity) {
            val intent = Intent(context, LocationSearchActivity::class.java)
            context.startActivityForResult(intent, REQ_CODE)
        }
    }

    private fun searchLocation() {
        apiHitCounter = 1
        viewModel.searchLocation(PLACE_API + binding.searchLocationET.text.toString())
    }

    private fun locationDetail() {
        if (selectedPlaceId!!.isEmpty())
            return
        apiHitCounter = 2
        viewModel.locationDetail(PLACE_DETAIL_API + selectedPlaceId)
    }

    private fun handleAnimation() {
        val anim = AnimationUtils.loadAnimation(this@LocationSearchActivity, R.anim.scale)
        binding.searchIV.startAnimation(anim)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                setResultsBack()
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
    }

    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> searchLocation()
            2 -> locationDetail()
        }
    }
}