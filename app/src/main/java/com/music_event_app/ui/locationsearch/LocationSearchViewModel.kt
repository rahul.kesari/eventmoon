package com.music_event_app.ui.locationsearch

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.TermsConditionResponse
import com.music_event_app.domain.model.locationsearch.LocationDetailResponse
import com.music_event_app.domain.model.locationsearch.LocationSearchResponse
import com.music_event_app.domain.usecase.LocationSearchUseCase
import com.music_event_app.domain.usecase.TermsConditionUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

class LocationSearchViewModel constructor(private val locationSearchUseCase: LocationSearchUseCase) :
    BaseViewModel() {
    val TAG = LocationSearchViewModel::class.java.name
    val empty = MutableLiveData<Boolean>()
    val mSnackbarHandler = MutableLiveData<ErrorModel?>()
    val locationSearchLiveData = MutableLiveData<LocationSearchResponse>()
    val locationDetailLiveData = MutableLiveData<LocationDetailResponse>()
    val isProcessing = MutableLiveData<Boolean>()

    @ExperimentalCoroutinesApi
    fun searchLocation(url: String) {
        isProcessing.value = true
        locationSearchUseCase.searchLocation(
            scope,
            url,
            object : UseCaseResponse<LocationSearchResponse> {
                override fun onSuccess(result: LocationSearchResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    locationSearchLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarHandler.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun locationDetail(url: String) {
        isProcessing.value = true
        locationSearchUseCase.locationDetail(
            scope,
            url,
            object : UseCaseResponse<LocationDetailResponse> {
                override fun onSuccess(result: LocationDetailResponse) {
                    Log.i(TAG, "result: $result")
                    isProcessing.value = false
                    locationDetailLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarHandler.postValue(errorModel)
                }
            })
    }
}