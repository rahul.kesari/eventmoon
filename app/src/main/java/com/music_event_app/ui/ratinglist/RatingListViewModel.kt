package com.music_event_app.ui.ratinglist

import androidx.lifecycle.MutableLiveData
import com.music_event_app.data.source.remote.ErrorModel
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.usecase.RatingListUseCase
import com.music_event_app.domain.usecase.base.UseCaseResponse
import com.music_event_app.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

class RatingListViewModel constructor(private val ratingListUseCase: RatingListUseCase) : BaseViewModel() {

    val empty = MutableLiveData<Boolean>()
    val mSnackbarText = MutableLiveData<ErrorModel?>()

    val eventReviewLiveData = MutableLiveData<BaseResponse>()
    val eventRatingSubmitLiveData = MutableLiveData<BaseResponse>()

    val likeDislikeSubmitLiveData = MutableLiveData<BaseResponse>()

    val isProcessing = MutableLiveData<Boolean>()
    val mSnackbarHandler = MutableLiveData<ErrorModel?>()

    val TAG = RatingListViewModel::class.java.name


    @ExperimentalCoroutinesApi
    fun submitRatings(commonRequest: CommonRequest) {
        isProcessing.value = true
        ratingListUseCase.submitRatings(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventRatingSubmitLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun getEventReview(commonRequest: CommonRequest) {
        isProcessing.value = true
        ratingListUseCase.getEventReview(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    eventReviewLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }

    @ExperimentalCoroutinesApi
    fun submitLikeDislike(commonRequest: CommonRequest) {
        isProcessing.value = true
        ratingListUseCase.submitLikeDislike(
            scope,
            commonRequest,
            object : UseCaseResponse<BaseResponse> {
                override fun onSuccess(result: BaseResponse) {

                    isProcessing.value = false
                    likeDislikeSubmitLiveData.value = result
                }

                override fun onError(errorModel: ErrorModel?) {
                    isProcessing.postValue(false)
                    mSnackbarText.postValue(errorModel)
                }
            })
    }
}