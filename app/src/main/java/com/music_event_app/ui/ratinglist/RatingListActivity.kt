package com.music_event_app.ui.ratinglist

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.OvershootInterpolator
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.music_event_app.R
import com.music_event_app.databinding.ActivityRatingListBinding
import com.music_event_app.databinding.ActivityResetpasswordBinding
import com.music_event_app.domain.model.BaseResponse
import com.music_event_app.domain.model.LoginRequest
import com.music_event_app.domain.model.events.EventItem
import com.music_event_app.domain.model.request.CommonRequest
import com.music_event_app.domain.model.reviewsingle.RatingByVotesModel
import com.music_event_app.domain.model.reviewsingle.ReviewDescriptionWithModel
import com.music_event_app.domain.model.reviewsingle.SingleReview
import com.music_event_app.ui.adapters.AverageRatingAdapter
import com.music_event_app.ui.adapters.EventListAdapter
import com.music_event_app.ui.adapters.RatingListAdapter
import com.music_event_app.ui.base.BaseActivity
import com.music_event_app.ui.base.listeners.RecyclerItemListener
import com.music_event_app.ui.dialogs.RateFeedbackDialog
import com.music_event_app.ui.eventdetail.EventDetailActivity
import com.music_event_app.ui.home.HomeActivity
import com.music_event_app.util.AppConstants
import com.music_event_app.util.extensions.hideKeyboard
import com.music_event_app.util.helper.*
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.ScaleInAnimator
import kotlinx.android.synthetic.main.activity_home.view.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.android.synthetic.main.layout_toolbar.view.toolbar
import org.koin.android.viewmodel.ext.android.viewModel

class RatingListActivity : BaseActivity<RatingListViewModel, ActivityRatingListBinding>() {

    override var frameContainerId: Int = 0
    override val layoutId: Int = R.layout.activity_rating_list
    override val viewModel: RatingListViewModel by viewModel()

    private val TAG: String = RatingListActivity.javaClass.name

    private var eventId: Int? = null
    private var reviewText = ""
    private var reviewRatings = 0.0f
    private lateinit var averageRatingAdapter: AverageRatingAdapter
    private var averageRatingList = ArrayList<RatingByVotesModel?>()

    private lateinit var ratingListAdapter: RatingListAdapter
    private var ratingList = ArrayList<ReviewDescriptionWithModel?>()

    private var totalPage = 0
    private var currentPage = 1
    private var scrollListener: EndlessRecyclerViewScrollListener? = null

    var apiHitCounter: Int = 1
    var isLiked = false
    var ratingId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        eventId = intent.extras?.getInt(EVENT_ID)
        setupInsets(binding.topLayout)
        initViews()
        observeViewModel()
        initAverageRatingListRecycler()
        initRatingListRecycler()
        getEventReview()
    }

    private fun initViews() {
        binding.toolbarLayout.titleTV.text = "Review"
        binding.toolbarLayout.toolbar.setNavigationOnClickListener {
            finish()
        }

        binding.rateEventBtn.setOnClickListener {
            showRateDialog()
        }
    }


    private fun initAverageRatingListRecycler() {

        averageRatingList.add(RatingByVotesModel(Ratings = "5.00"))
        averageRatingList.add(RatingByVotesModel(Ratings = "4.00"))
        averageRatingList.add(RatingByVotesModel(Ratings = "3.00"))
        averageRatingList.add(RatingByVotesModel(Ratings = "2.00"))
        averageRatingList.add(RatingByVotesModel(Ratings = "1.00"))

        averageRatingAdapter =
            AverageRatingAdapter(averageRatingList, object : RecyclerItemListener {
                override fun onItemSelected(item: Any, position: Int) {

                }
            })
        binding.averageRatingRV.layoutManager = LinearLayoutManager(this)
        binding.averageRatingRV.adapter = averageRatingAdapter
    }

    private fun initRatingListRecycler() {

        binding.ratingListRV.itemAnimator = ScaleInAnimator(OvershootInterpolator(1f))

        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.ratingListRV.layoutManager = linearLayoutManager
        scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                if (currentPage < totalPage) {
                    currentPage += 1
                    binding.ratingListRV.post(Runnable {
                        ratingList.add(null)
                        ratingListAdapter?.notifyItemInserted(ratingListAdapter?.itemCount!!)
                        getEventReview()
                    })
                }
            }
        }

        binding.ratingListRV.addOnScrollListener(scrollListener as EndlessRecyclerViewScrollListener)
        ratingListAdapter =
            RatingListAdapter(ratingList, object : RatingListAdapter.RatingRecyclerItemListener {
                override fun onLikeClicked(item: Any, position: Int) {
                    ratingId = (item as ReviewDescriptionWithModel).RatingsID
                    isLiked = true
                    submitLikeDislike()
                }

                override fun onDislikeClicked(item: Any, position: Int) {
                    ratingId = (item as ReviewDescriptionWithModel).RatingsID
                    isLiked = false
                    submitLikeDislike()
                }
            })
        binding.ratingListRV.adapter = ScaleInAnimationAdapter(ratingListAdapter).apply {
            setFirstOnly(true)
            setDuration(500)
            setInterpolator(OvershootInterpolator(.5f))
        }
    }

    fun observeViewModel() {
        with(viewModel) {

            isProcessing.observe(this@RatingListActivity, Observer {
                if (it) binding.progressLayout.visibility =
                    View.VISIBLE else binding.progressLayout.visibility = View.GONE
            })

            eventRatingSubmitLiveData.observe(this@RatingListActivity, Observer {
                MessagesUtils.showToastSuccess(
                    this@RatingListActivity,
                    "Review Submitted Successfully"
                )
            })

            likeDislikeSubmitLiveData.observe(this@RatingListActivity, Observer {
                MessagesUtils.showToastSuccess(
                    this@RatingListActivity,
                    it.Results as String?
                )
            })

            eventReviewLiveData.observe(this@RatingListActivity, Observer {
                val eventDetail = ObjectConverter.getEventSingleReviewResponse(it.Results)
                renderEventReview(eventDetail, it)
            })

            mSnackbarHandler.observe(this@RatingListActivity, Observer {
                if (it!!.retryNeeded) {
                    SnackBarHelper.showRetry(it.message, this@RatingListActivity, object :
                        SnackBarHelper.RetryActionClicked {
                        override fun onRetryClicked() {
                            handleApiHits()
                        }
                    })
                } else {
                    SnackBarHelper.showError(it.message, this@RatingListActivity)
                }
            })
        }
    }


    private fun renderAverageEventReview(singleReview: SingleReview?) {


        binding.totalAverageRatingsTV.text =
            singleReview?.eventRatingDetailsModel?.ratingPercentage + " %"

        singleReview?.eventRatingDetailsModel?.AverageRating?.let {
            binding.totalStarRatingRB.rating = it.toFloat()
        }

        binding.totalVotesTV.text = singleReview?.eventRatingDetailsModel?.TotalVotes + " Vote(s)"

        averageRatingList.forEachIndexed { parentindex, parent ->
            singleReview?.ratingByVotesModel?.forEachIndexed { childindex, child ->
                if (parent?.Ratings.equals(child.Ratings)) {
                    averageRatingList[parentindex] = child
                }
            }
        }
        averageRatingAdapter.notifyDataSetChanged()
    }

    private fun renderEventReview(singleReview: SingleReview?, baseResponse: BaseResponse) {

        renderAverageEventReview(singleReview)

        if (currentPage > 1) {
            ratingList.removeAt(ratingList.size - 1)
            ratingListAdapter?.notifyItemRemoved(ratingList.size)
        } else {
            ratingList.clear()
            ratingListAdapter?.notifyDataSetChanged()
        }


        singleReview?.reviewDescriptionWithModel?.let {
            ratingList.addAll(it)
        }

        totalPage = baseResponse?.totalPageCount!!

        ratingListAdapter?.notifyDataSetChanged()
        scrollListener?.setLoading()
    }


    companion object {
        const val EVENT_ID = "EVENT_ID"

        fun start(context: Context, eventId: Int) {
            val intent = Intent(context, RatingListActivity::class.java)
            intent.putExtra(EVENT_ID, eventId)
            context.startActivity(intent)
        }
    }

    private fun showRateDialog() {
        val rateFeedbackDialog = RateFeedbackDialog(this, object : RateFeedbackDialog.RateFeedbackDialgListener {
                override fun onSubmit(review: String?, ratings: Float) {
                    reviewText = review!!
                    reviewRatings = ratings
                    submitReviewRatings()
                }
            })
        rateFeedbackDialog.show()
    }

    private fun getEventReview() {
        apiHitCounter = 1
        val commonRequest = CommonRequest(pageNumber = currentPage)
        commonRequest.eventId = eventId
        viewModel.getEventReview(commonRequest)
    }

    private fun submitReviewRatings() {
        apiHitCounter = 2

        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.ratings = reviewRatings
        commonRequest.reviewText = reviewText
        commonRequest.eventId = eventId

        viewModel.submitRatings(commonRequest)
    }

    private fun submitLikeDislike() {
        apiHitCounter = 3

        val commonRequest = CommonRequest(pageNumber = 0)
        commonRequest.userId = Prefs.getInt(AppConstants.PreferenceConstants.USER_ID)
        commonRequest.accessToken = Prefs.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN)
        commonRequest.isLike = isLiked
        commonRequest.ratingsID = ratingId
        viewModel.submitLikeDislike(commonRequest)
    }

    private fun handleApiHits() {
        when (apiHitCounter) {
            1 -> getEventReview()
            2 -> submitReviewRatings()
            3 -> submitLikeDislike()
        }
    }
}
