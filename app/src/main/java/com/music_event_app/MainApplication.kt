package com.music_event_app

import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import androidx.multidex.MultiDex
import com.facebook.FacebookSdk
import com.music_event_app.di.module.AppModule
import com.music_event_app.di.module.DatabaseModule
import com.music_event_app.di.module.NetworkModule
import com.music_event_app.util.AppConstants
import com.music_event_app.util.helper.LocaleHelper
import com.facebook.stetho.Stetho
import com.music_event_app.util.helper.Prefs
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApplication : Application() {

    private val TAG = MainApplication::class.java.name
    override fun onCreate() {
        super.onCreate()
        context = applicationContext

        FacebookSdk.sdkInitialize(applicationContext)

        MultiDex.install(this)
        Stetho.initializeWithDefaults(this)

        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(listOf(AppModule, NetworkModule, DatabaseModule))
        }
        initPrefs()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base, AppConstants.DEFAULT_LANGUAGE))
        MultiDex.install(this)
    }


    private fun initPrefs() {
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }

    companion object {
        lateinit var context: Context
    }
}