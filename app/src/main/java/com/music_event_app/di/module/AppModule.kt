package com.music_event_app.di.module

import com.music_event_app.data.repository.LocalRepository
import com.music_event_app.data.repository.RemoteRepository
import com.music_event_app.data.source.local.AppDatabase
import com.music_event_app.data.source.remote.APIService
import com.music_event_app.data.source.remote.ApiErrorHandle
import com.music_event_app.domain.usecase.*
import com.music_event_app.ui.buyticket.BuyTicketViewModel
import com.music_event_app.ui.createnewevent.CreateNewEventViewModel
import com.music_event_app.ui.editprofile.EditProfileViewModel
import com.music_event_app.ui.eventdetail.EventDetailViewModel
import com.music_event_app.ui.home.HomeActivityViewModel
import com.music_event_app.ui.home.fragments.drawer.DrawerViewModel
import com.music_event_app.ui.home.fragments.eventsearchfragment.EventSearchFragmentViewModel
import com.music_event_app.ui.home.fragments.homefragment.HomeFragmentViewModel
import com.music_event_app.ui.home.fragments.locationfragment.LocationNearbyFragmentViewModel
import com.music_event_app.ui.home.fragments.rewardsfragment.RewardsFragmentViewModel
import com.music_event_app.ui.locationsearch.LocationSearchViewModel
import com.music_event_app.ui.login.LoginViewModel
import com.music_event_app.ui.mapeventdetail.MapEventDetailViewModel
import com.music_event_app.ui.myevents.MyEventListViewModel
import com.music_event_app.ui.ratinglist.RatingListViewModel
import com.music_event_app.ui.resetpassword.ResetPasswordViewModel
import com.music_event_app.ui.signup.SignupViewModel
import com.music_event_app.ui.splash.SplashViewModel
import com.music_event_app.ui.termscondition.TermsConditionViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


/* for every usecase here entry will be done, & Viewmodel will be injected here */

val AppModule = module {

    viewModel { HomeActivityViewModel(get()) }
    single { createHomeActivityUseCase(get(), createApiErrorHandle()) }

    viewModel { SplashViewModel() }

    viewModel { LoginViewModel(get()) }
    single { createLoginSignupUseCase(get(), createApiErrorHandle()) }

    viewModel { SignupViewModel(get()) }

    viewModel { ResetPasswordViewModel(get()) }

    viewModel { TermsConditionViewModel(get()) }
    single { createTermsConditionUseCase(get(), createApiErrorHandle()) }

    viewModel { DrawerViewModel(get()) }

    viewModel { HomeFragmentViewModel(get()) }

    viewModel { RewardsFragmentViewModel(get()) }
    single { createHomeFragmentUseCase(get(), createApiErrorHandle()) }

    viewModel { EventDetailViewModel(get()) }
    single { createEventDetailUseCase(get(), createApiErrorHandle()) }

    viewModel { EventSearchFragmentViewModel(get()) }
    single { createEventSearchUseCase(get(), createApiErrorHandle()) }

    viewModel { LocationSearchViewModel(get()) }
    single { createLocationSearchUseCase(get(), createApiErrorHandle()) }

    viewModel { EditProfileViewModel(get()) }
    single { createEditProfileUseCase(get(), createApiErrorHandle()) }

    viewModel { RatingListViewModel(get()) }
    single { createRatingListUseCase(get(), createApiErrorHandle()) }

    viewModel { BuyTicketViewModel() }

    viewModel { CreateNewEventViewModel(get()) }
    single { createMyEventUseCase(get(), createApiErrorHandle()) }

    viewModel { LocationNearbyFragmentViewModel(get()) }
    single { createLocationNearbyUseCase(get(), createApiErrorHandle()) }

    viewModel { MyEventListViewModel(get()) }

    viewModel { MapEventDetailViewModel(get()) }
    single { createMapEventDetailUseCase(get(), createApiErrorHandle()) }

    single { createLocalRepository(get()) }

    single { createRemoteRepository(get()) }
}


fun createLocalRepository(database: AppDatabase): LocalRepository {
    return LocalRepository(database)
}

fun createRemoteRepository(apiService: APIService): RemoteRepository {
    return RemoteRepository(apiService)
}

fun createLoginSignupUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): LoginSignupUseCase {
    return LoginSignupUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createTermsConditionUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): TermsConditionUseCase {
    return TermsConditionUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createHomeFragmentUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): HomeFragmentUseCase {
    return HomeFragmentUseCase(homeRemoteRepository, apiErrorHandle)
}


fun createEventDetailUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): EventDetailUseCase {
    return EventDetailUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createEventSearchUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): EventSearchUseCase {
    return EventSearchUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createLocationSearchUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): LocationSearchUseCase {
    return LocationSearchUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createEditProfileUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): EditProfileUseCase {
    return EditProfileUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createRatingListUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): RatingListUseCase {
    return RatingListUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createHomeActivityUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): HomeActivityUseCase {
    return HomeActivityUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createMyEventUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): MyEventUseCase {
    return MyEventUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createLocationNearbyUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): LocationNearbyUseCase {
    return LocationNearbyUseCase(homeRemoteRepository, apiErrorHandle)
}

fun createMapEventDetailUseCase(
    homeRemoteRepository: RemoteRepository,
    apiErrorHandle: ApiErrorHandle
): MapEventDetailUseCase {
    return MapEventDetailUseCase(homeRemoteRepository, apiErrorHandle)
}