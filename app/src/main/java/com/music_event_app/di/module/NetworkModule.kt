package com.music_event_app.di.module

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.google.gson.GsonBuilder
import com.music_event_app.util.AppConstants
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.music_event_app.data.source.remote.APIService
import com.music_event_app.data.source.remote.ApiErrorHandle
import okhttp3.Headers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private val TIME_OUT = 60L
//val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK)
val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"

val NetworkModule = module {

    single { createService(get()) }

    single { createRetrofit(get()) }

    single { createOkHttpClient(get()) }

    single { createGsonConverterFactory() }

//    single { createMoshi() }

}


fun createRetrofit(
    okHttpClient: OkHttpClient
): Retrofit {

    val gson = GsonBuilder().serializeNulls().create()
    return Retrofit.Builder()
        .baseUrl(AppConstants.API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(okHttpClient)
        .build()
}


fun createOkHttpClient(context: Context): OkHttpClient {
    val client = OkHttpClient.Builder()
        //  .cache(cache)
        .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
        .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
        .readTimeout(TIME_OUT, TimeUnit.SECONDS)
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    client.addNetworkInterceptor(interceptor)
    client.addInterceptor { chain ->
        val request = chain.request().newBuilder()
            //  .headers(getJsonHeader(PrefUtils.getToken(context = context)))
            .build()
        chain?.proceed(request)
    }
        .build()
    return client.build()
}

private fun getJsonHeader(authToken: String?): Headers {
    val builder = Headers.Builder()
    builder.add("Content-Type", "application/json")
    builder.add("Accept", "application/json")
    builder.add("Accept-Language", "fa-ir")
    if (authToken != null && authToken.isNotEmpty()) {
        builder.add("Authorization", "JWT $authToken")
    }
    return builder.build()
}

//fun createMoshi(): Moshi {
//    return GsonBuilder.build()
//}

fun createGsonConverterFactory(): GsonConverterFactory {
    val gson = GsonBuilder().serializeNulls().create()
    return GsonConverterFactory.create(gson)
}

fun createApiErrorHandle(): ApiErrorHandle {
    return ApiErrorHandle()
}

fun createService(retrofit: Retrofit): APIService {
    return retrofit.create(APIService::class.java)
}
